<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		cek_login();
		$this->load->model('Barang_m');
		$this->load->model('Imei_barang_m');
		$this->load->model('Barangpermember_m', 'bpm');
	}
	public function index()
	{
		$data = array(
			'title'    => 'Barang',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'supplier' => $this->db->get('supplier')->result_array(),
			'member'   => $this->db->get('user')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'barang/item/index',
			'item'	   => $this->Barang_m->getAllData()
		);
		$this->load->view('templates/main', $data);
	}

	public function LoadData()
	{
		$json = array(
			"aaData"  => $this->Barang_m->getAllData()
		);
		echo json_encode($json);
	}

	public function inputbarang()
	{
		$imeis = $this->input->post('imei');
		$this->Barang_m->Save();
		$id_barang = $this->db->insert_id();
		$this->Imei_barang_m->saveBulk($id_barang, $imeis);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Barang berhasil disimpan.</div>');
		redirect('barang/index');
	}

	public function detilbarang($id = '')
	{
		$data = $this->Barang_m->Detail($id);
		echo json_encode($data);
	}
	public function hapusbarang($id = '')
	{
		$this->Barang_m->Delete($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Barang berhasil dihapus.</div>');
	}
	public function hapusSemuaBarang()
	{
		$this->Barang_m->DeleteAll();
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Semua Barang berhasil dihapus.</div>');
		redirect(base_url('barang'));
	}

	public function caribarang($key = '')
	{
		$data = $this->Barang_m->Search($key);
		echo json_encode($data);
	}

	public function edit($id)
	{
		$id = decrypt_url($id);
		$item = $this->Barang_m->Detail($id);
		$imeis = $this->Imei_barang_m->getImeiBarang($id);
		$data = array(
			'title'    => 'Edit Item',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'item'	   => $item,
			'imei'	   => $imeis,
			'member'   => $this->db->get('user')->result_array(),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'barang/item/edititem'
		);
		$this->load->view('templates/main', $data);
	}

	public function editbarang()
	{
		$id_barang = $this->input->post('iditem');
		$imeis = $this->input->post('imei');

		$this->Barang_m->Edit();
		foreach($imeis as $imei){
			$imei_exist = $this->Imei_barang_m->getImeiBarangByImei($imei);
			if(is_null($imei_exist)){
				$this->Imei_barang_m->save($id_barang, $imei);
			}
		}
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Barang berhasil diubah.</div>');
		redirect('barang/index');
	}

	public function updateStok($stok, $id)
	{
		$brg = $this->db->get_where('barang', ['id_barang' => $id])->row_array();
		if ($stok < 0) {
			$qty = abs($stok);
			$stokBrg = $brg['stok'] - $qty;
		} else {

			$stokBrg = $brg['stok'] + $stok;
		}
		$this->db->set(array('stok' => $stokBrg))->where('id_barang', $id)->update('barang');
	}

	public function member()
	{
		$data = array(
			'title'    => 'Barang per Member',
			'user'     => infoLogin(),
			'member'   => $this->bpm->get($_GET['id_cs'] ? ['id_customer' => $_GET['id_cs']] : []),
			'barang'   => $this->db->get('barang')->result(),
			'customer' => $this->db->get('customer')->result(),
			'content'  => 'barang/member/index',
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'item'	   => $this->Barang_m->getAllData()
		);
		$this->load->view('templates/main', $data);
	}

	public function member_input()
	{
		if ($this->bpm->create($this->input->post())) {
			$this->Func->notif('data berhasil disimpan', 'success');
		} else {
			$this->Func->notif('gagal menyimpan data', 'danger');
		}
		return redirect(base_url('barang/member'));
	}

	public function member_edit()
	{
		if ($this->bpm->update($this->input->post())) {
			$this->Func->notif('data berhasil diedit', 'success');
		} else {
			$this->Func->notif('gagal mengedit data', 'danger');
		}
		return redirect(base_url('barang/member'));
	}

	public function member_hapus($id)
	{
		if ($this->bpm->delete($id)) {
			$this->Func->notif('data berhasil dihapus', 'success');
		} else {
			$this->Func->notif('gagal mengedit data', 'danger');
		}
		return redirect(base_url('barang/member'));
	}

	public function edit_stok($id)
	{
		if ($this->db->get_where('barang', ['id_barang' => $id])->row()->stok !== $this->input->post('stok')) {
			$stok_awal = $this->db->get_where('barang', ['id_barang' => $id])->row()->stok;

			$this->db->update('barang', [
				'stok' => $this->input->post('stok')
			], ['id_barang' => $id]);

			$id_user = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row()->id_user;

			$this->db->insert('historyupdatestok', [
				'id_user' => $id_user,
				'id_barang' => $id,
				'stok_awal' => $stok_awal,
				'stok_akhir' => $this->input->post('stok')
			]);

			$this->Func->notif('stok berhasil diedit', 'success');
		} else {
			$this->Func->notif('stok tidak diedit', 'warning');
		}
		return redirect(base_url('barang'));
	}

	public function request($method, $id = null, $status = null)
	{
		if ($method == 'add') {
			$barang = $this->input->post('id_barang');
			for ($i = 0; $i < count($barang); $i++) {
				$this->db->insert('customer_request_barang', [
					'id_customer' => $this->input->post('id_customer'),
					'id_barang' => $barang[$i],
					'harga' => $this->input->post('harga')[$i],
					'status' => $this->session->userdata('tipe') == 'Sales' ? '0' : '1'
				]);
			}
			$this->Func->notif('request berhasil ditambah', 'success');
		}

		if ($method == 'change') {
			$this->db->update('customer_request_barang', [
				'status' => $status
			], ['id' => $id]);
			$this->Func->notif('status berhasil diubah', 'success');
		}

		if ($method == 'delete') {
			$this->db->delete('customer_request_barang', ['id' => $id]);
		}

		return redirect(base_url('customer'));
	}
}
