<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		cek_login();
		$this->load->model('Customer_m');
	}

	public function index()
	{
		$data = array(
			'title'    => 'Customer',
			'user'     => infoLogin(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'customer/index',
			'barang'   => $this->db->get('barang')->result(),
			'customer' => $this->db->get('customer')->result()
		);
		
		$id_cs = isset($_GET['id_cs']);

		$this->db->order_by('id', 'DESC');
		$data['customer_request_barang'] = $id_cs ? $this->db->get_where('customer_request_barang', ['id_customer' => $id_cs])->result() : $this->db->get('customer_request_barang')->result();
		if ($data['customer_request_barang']) {
			foreach ($data['customer_request_barang'] as $key => $item) {
				$data['customer_request_barang'][$key]->customer = $this->db->get_where('customer', ['id_cs' => $item->id_customer])->row();
				$data['customer_request_barang'][$key]->barang = $this->db->get_where('barang', ['id_barang' => $item->id_barang])->row();
			}
		}

		$this->load->view('templates/main', $data);
	}

	public function LoadData()
	{
		$json = array(
			"aaData"  => $this->Customer_m->getAllData()
		);
		echo json_encode($json);
	}

	public function inputcustomer()
	{
		$this->Customer_m->Save();
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Customer berhasil disimpan.</div>');
		redirect('customer/index');
	}

	public function detilcustomer($id = '')
	{
		$data = $this->Customer_m->Detail($id);
		echo json_encode($data);
	}

	public function editcustomer()
	{
		$this->Customer_m->Edit();
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Customer berhasil diubah.</div>');
		redirect('customer/index');
	}

	public function hapuscustomer($id = '')
	{
		$this->Customer_m->Delete($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Customer berhasil dihapus.</div>');
	}

	public function cek_delete($id = '')
	{
		$data = $this->Customer_m->cekDelete($id);
		echo json_encode($data);
	}

	public function poin()
	{
		$data = array(
			'title'    => 'Customer Poin',
			'user'     => infoLogin(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'customer/poin',
			'poin'	   => $this->Customer_m->getPoin()

		);
		$this->load->view('templates/main', $data);
	}

	public function riwayat_poin($kode)
	{
		$data = array(
			'title'    => 'Riwayat Poin',
			'user'     => infoLogin(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'customer/riwayat',
			'poin'	   => $this->Customer_m->getRiwayat($kode)

		);
		$this->load->view('templates/main', $data);
	}

	public function changestatus($id_cs, $status)
	{
		$this->db->update('customer', [
			'status' => $status
		], ['id_cs' => $id_cs]);
		return redirect(base_url('customer'));
	}
}
