<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dpenjualan extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    cek_login();
  }
  public function index()
  {
    $data = array(
      'title'    => 'Daftar Penjualan',
      'user'     => infoLogin(),
      'toko'     => $this->db->get('profil_perusahaan')->row(),
      'kasir'     => $this->db->where("tipe", "Kasir")->get('user')->result(),
      'content'  => 'daftarpenjualan/index',
    );
    $this->load->view('templates/main', $data);
  }
  public function LoadData()
  {

    $page_number = $this->input->get('page_number');
    $page_size = $this->input->get('page_size');
    $search = $this->input->get('search');
    $kasir = $this->input->get('kasir');
    $tanggal = $this->input->get('tanggal');
    $start = $page_number * $page_size;
    $search_filter = "";
    $kasir_filter = "";
    $tanggal_filter = "";
    if($search){
      $search_filter .= "AND (b.invoice LIKE '%$search%' ";
      $search_filter .= "OR d.nama_lengkap LIKE '%$search%' ";
      $search_filter .= "OR c.nama_cs LIKE '%$search%' ";
      $search_filter .= "OR b.method LIKE '%$search%' ";
      $search_filter .= "OR b.tgl LIKE '%$search%')";
    }
    if($kasir){
      $kasir_filter = "AND d.nama_lengkap = '$kasir' ";
    }
    if($tanggal){
      $tanggal_filter = "AND b.tgl BETWEEN '$tanggal 00:00:00' AND '$tanggal 23:59:59' ";
    }

    $sql = "SELECT b.id_jual, b.kode_jual, b.invoice, d.nama_lengkap, c.nama_cs, 
                    SUM(a.diskon) diskon, SUM(a.subtotal) as total, b.tgl, SUM(a.qty_jual) AS qty, b.method 
            FROM detil_penjualan a, penjualan b, customer c, user d 
            WHERE b.id_jual = a.id_jual AND 
                  c.id_cs = b.id_cs AND 
                  d.id_user = b.id_user AND 
                  b.is_active= 1 
                  $search_filter
                  $kasir_filter
                  $tanggal_filter
            GROUP BY a.id_jual
            ORDER BY b.tgl DESC
            LIMIT $start, $page_size";

    $json = array(
      "aaData"  => $this->model->General($sql)->result_array()
    );
    echo json_encode($json);
  }
  public function detilPenjualan($id = '')
  {
    $sql = "SELECT a.kode_detil_jual, c.barcode, c.nama_barang, a.harga_item, a.qty_jual, a.diskon, a.subtotal FROM detil_penjualan a, penjualan b, barang c WHERE b.id_jual = a.id_jual AND c.id_barang = a.id_barang AND  a.id_jual = '$id'";
    $data = $this->model->General($sql)->result_array();
    echo json_encode($data);
  }
  public function detilPenjualanServis($id = '')
  {
    $sql = "SELECT a.kode_detil_jual, a.harga_item, a.subtotal, d.nama_karyawan FROM detil_penjualan a, penjualan b, karyawan d WHERE b.id_jual = a.id_jual AND  a.id_jual = '$id' AND a.id_karyawan = d.id_karyawan";
    $data = $this->model->General($sql)->result_array();
    echo json_encode($data);
  }
}
