<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    cek_login();
    //cek_user();
    $this->load->model('Penjualan_m');
    $this->load->model('Imei_barang_m');
  }
  public function index()
  {
    $data = array(
      'title'    => 'Penjualan',
      'user'     => infoLogin(),
      'customer' => $this->db->get('customer')->result_array(),
      'toko'     => $this->db->get('profil_perusahaan')->row(),
      'content'  => 'penjualan/index',
      'pegawai'  => $this->db->get('karyawan')->result_array()
    );
    $this->load->view('templates/main', $data);
  }

  public function LoadData()
  {
    $detail_penjualan = $this->Penjualan_m->getDetilJual();
    foreach($detail_penjualan as &$item){
      $imeis = $this->Imei_barang_m->getImeiBarang($item['id_barang']);
      $item['imeiOption'] = $imeis;
    }
    $json = array(
      "aaData"  => $detail_penjualan
    );
    echo json_encode($json);
  }

  public function LoadDataService()
  {
    $json = array(
      "aaData"  => $this->Penjualan_m->getDetilService()
    );
    echo json_encode($json);
  }

  public function tambahbarang($id, $qty, $subtotal, $harga, $jenis, $pegawai)
  {

    $this->Penjualan_m->addItem($id, $qty, $subtotal, $harga, $jenis, $pegawai);
  }

  public function detilitemjual($id = '')
  {
    $this->Penjualan_m->detilItemJual($id);
  }

  public function detilservisjual($id = '')
  {
    $this->Penjualan_m->detilServisJual($id);
  }

  public function editdetiljual($id, $diskon, $qty, $hakhir)
  {
    $this->Penjualan_m->editDetailPenjualan($id, $diskon, $qty, $hakhir);
  }

  public function editservisjual($id, $harga, $subtotal)
  {
    $this->Penjualan_m->editServisJual($id, $harga, $subtotal);
  }

  public function hapusdetil($id = '')
  {
    $this->Penjualan_m->hapusDetail($id);
  }

  public function simpanpenjualan()
  {
    $detailPenjualan = $this->Penjualan_m->getDetilJual();
    foreach($detailPenjualan as $item){
      $imeis = $item['imei'];
      if($imeis){
        $arr = $imeis ? explode(",", $imeis) : [];
        for($i=0; $i<sizeof($arr); $i++){
          $imeis = $this->Imei_barang_m->useImei($arr[$i]);
        }
      }
    }
    $this->Penjualan_m->simpanPenjualan();
    redirect('report/struk_penjualan');
  }

  public function kodeinvoice()
  {
    date_default_timezone_set('Asia/Jakarta');
    $kodeinvoice = "POS" . date('YmdHis');
    echo json_encode($kodeinvoice);
  }

  public function hargatotal()
  {
    $sql = "SELECT SUM(subtotal) AS subtotal, SUM(diskon) as diskon FROM detil_penjualan WHERE id_jual IS NULL";
    $data = $this->model->General($sql)->row_array();
    echo json_encode($data);
  }

  public function detailJual($id = '')
  {
    $data = array(
      'title'    => 'Edit Penjualan',
      'user'     => infoLogin(),
      'customer' => $this->db->get('customer')->result_array(),
      'toko'     => $this->db->get('profil_perusahaan')->row(),
      'content'  => 'penjualan/edit'
    );
    $this->load->view('templates/main', $data);
  }

  public function editQtyDetilPenjualan($id)
  {
    $qty = $this->input->post('qty');
    $dataDetil = $this->db->get_where('detil_penjualan', ['id_detil_jual' => $id])->row();
    $dataBarang = $this->db->get_where('barang', ['id_barang' => $dataDetil->id_barang])->row();

    $stok = 0;

    if ($qty > $dataDetil->qty_jual) {
      $stok = $qty - $dataDetil->qty_jual;
      $this->db->where('id_barang', $dataDetil->id_barang);
      $this->db->update('barang', ['stok' => $dataBarang->stok - $stok]);
    }
    if ($dataDetil->qty_jual > $qty) {
      $stok = $dataDetil->qty_jual - $qty;
      $this->db->where('id_barang', $dataDetil->id_barang);
      $this->db->update('barang', ['stok' => $dataBarang->stok + $stok]);
    }

    $data = [
      'qty_jual' => $qty,
      'subtotal' => $qty * $dataDetil->harga_item
    ];

    $this->db->where('id_detil_jual', $id);
    $this->db->update('detil_penjualan', $data);
  }

  public function editImeiDetilPenjualan($id)
  {
    $imei = $this->input->post('imei');
    $imei_index = $this->input->post('imei_index');
    $detailPenjualan = $this->Penjualan_m->getDetailJualById($id);
    $imeiBarang = $this->Imei_barang_m->getImeiBarangByImei($imei);

    if(is_null($detailPenjualan) || is_null($imeiBarang)){
      return;
    }

    $qty = intval($detailPenjualan->qty_jual);
    $imeis = $detailPenjualan->imei;
    
    $arr = $imeis ? explode(",", $imeis) : [];
    if(is_null($arr)){
      for($i=0; $i<$qty; $i++){
        array_push($arr, "");
      }
    }
    $arr[$imei_index] = $imei;
    $new_imei = implode(",", $arr);

    $this->Penjualan_m->editImeiDetailPenjualan($id, $new_imei);
  }

  // public function dataEdit($id = '')
  // {
  //   $sql = "SELECT b.id_jual, a.id_detil_jual, d.barcode, d.nama_barang, d.harga_jual, a.qty_jual, a.diskon, 
  //   a.subtotal, c.nama_cs FROM detil_penjualan a, penjualan b, customer c, barang d
  //   WHERE b.id_jual = a.id_jual AND c.id_cs = b.id_cs AND d.id_barang = a.id_barang AND b.is_active = 1 AND b.id_jual = '$id'";

  //   $data = $this->model->General($sql)->result_array();
  //   $json = array(
  //     "aaData"  => $data
  //   );
  //   echo json_encode($json);
  // }
}
