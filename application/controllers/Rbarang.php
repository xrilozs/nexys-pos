<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rbarang extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		cek_login();
		$this->load->model('RBarang_m');
	}
	public function index()
	{
		$data = array(
			'title'    => 'Data Request Barang',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'rbarang/index',
			'item'	   => $this->RBarang_m->getSomeData()
		);
		$this->load->view('templates/main', $data);
	}
	
	public function historystok() {
		$data = array(
			'title'    => 'Update Stok History',
			'user'     => infoLogin(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'history'     => $this->db->get('historyupdatestok')->result_array(),
			'content'  => 'rbarang/historystok'
		);
		$this->load->view('templates/main', $data);
	}
	
	public function ready()
	{
		$data = array(
			'title'    => 'Request Barang',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'rbarang/gudang',
			'item'	   => $this->RBarang_m->getAllData()
		);
		$this->load->view('templates/main', $data);
	}

	public function editrb($id)
	{
		$id = decrypt_url($id);
		$data = array(
			'title'    => 'Edit Harga',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'item'	   => $this->RBarang_m->Detail($id),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'rbarang/editrb'
		);
		$this->load->view('templates/main', $data);
	}

	public function updatehrgcbg()
	{
		$this->RBarang_m->updatehrgcbg();
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Barang berhasil disimpan.</div>');
		redirect('rbarang/ready');	
	}

	public function showbarang()
	{
		$data = array(
			'title'    => 'Data Barang',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'rbarang/showbarang',
			'barang'   => $this->RBarang_m->DataBarang(),
		);
		$this->load->view('templates/main', $data);
	}
	public function getbarang($id)
	{
		$id = decrypt_url($id);
		$data = array(
			'title'    => 'Edit Item',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'item'	   => $this->RBarang_m->DetailBrg($id),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'rbarang/inputrequest'
		);
		$this->load->view('templates/main', $data);
	}
	


	public function tambahrbarang(){
		$user = infoLogin();
        $id_cabang = $user['id_user'];
		$id_barang  = $this->input->post('id_barang');
		$nama_barang  = $this->input->post('nama_barang');
		$stok  = $this->input->post('stok');

		$data = array(
			'id_barang' => $id_barang,
			'stok'		=> $stok,
			'status'	=> 'Pending',
			'id_cabang_request' => $id_cabang

		);

		$this->RBarang_m->tambah_rbarang($data,'request_barang');
		$this->RBarang_m->updatestok($id_barang,$stok);
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Barang berhasil disimpan.</div>');
		redirect('rbarang/index');

	}

    public function gudang()
	{
		$data = array(
			'title'    => 'Request Barang',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'rbarang/gudang',
			'item'	   => $this->RBarang_m->getAllData()
		);
		$this->load->view('templates/main', $data);
    }

    	public function edit($id)
	{
		$id = decrypt_url($id);
		$data = array(
			'title'    => 'Edit Item',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'item'	   => $this->RBarang_m->Detail($id),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'rbarang/editrequest'
		);
		$this->load->view('templates/main', $data);
	}

	public function updatestatus()
	{
		$this->RBarang_m->update();
		$this->RBarang_m->updatebarangstok();
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Barang berhasil diubah.</div>');
		redirect('rbarang/gudang');
	}

	public function reqharga()
	{
		$data = array(
			'title'    => 'Request Harga',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'rbarang/requestharga/index',
			'item'	   => $this->RBarang_m->showreqharga()
		);
		
		
		$this->load->view('templates/main', $data);
	}

	public function editreqh($id,$id_request) 
	{
		$id = decrypt_url($id);
		$id_request = decrypt_url($id_request);
		$data = array(
			'title'    => 'Edit Request Status',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'item'	   => $this->RBarang_m->Detailreqh($id),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'rbarang/requestharga/edit'
		);
		$this->load->view('templates/main', $data);
	}

	public function updatestatusharga(){
		$this->RBarang_m->updatestatusharga();
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Barang berhasil diubah.</div>');
		redirect('rbarang/reqharga');
	}
}