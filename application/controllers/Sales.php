<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		cek_login();
		$this->load->model('RBarang_m');
    }

    public function index(){
        $data = array(
			'title'    => 'Request Harga',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'sales/index',
			'item'	   => $this->RBarang_m->showbaranghrg()
		);
		
		
		$this->load->view('templates/main', $data);
	}
	
	public function ready(){
        $data = array(
			'title'    => 'Request Harga',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'sales/ready',
			'item'	   => $this->RBarang_m->showbaranghrgready()
		);
		
		
		$this->load->view('templates/main', $data);
	}
	
	public function addrequest(){
		$data = array(
			'title'    => 'Request Harga + Barang',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'sales/requestdata',
			'item'	   => $this->RBarang_m->detailrequestbrg()
		);
		
		
		$this->load->view('templates/main', $data);
	}

	public function getbarang($id)
	{
		$id = decrypt_url($id);
		$data = array(
			'title'    => 'Request Harga + Item',
			'user'     => infoLogin(),
			'kategori' => $this->db->get('kategori')->result_array(),
			'satuan'   => $this->db->get('satuan')->result_array(),
			'item'	   => $this->RBarang_m->req_get_item($id),
			'request'  => $this->RBarang_m->req_get_itemr($id),
			'supplier' => $this->db->get('supplier')->result_array(),
			'toko'     => $this->db->get('profil_perusahaan')->row(),
			'content'  => 'sales/inputrequest'
		);
		$this->load->view('templates/main', $data);
	}

	public function tambahrbarang(){
		
		$user = infoLogin();
		$id_user = $user['id_user'];
		$id_cabang = $user['id_ownercabang'];
		$harga_request  = htmlspecialchars($this->input->post('harga_request'),TRUE);
		$id_request  = htmlspecialchars($this->input->post('id_request'),TRUE);
		

		$data = array(
			'id_request' => $id_request,
			'harga_request'		=> $harga_request,
			'id_cabangrequest' => $id_cabang,
			'id_user'		=> $id_user,
			'status' => 'Pending'

		);

		$this->RBarang_m->tambah_rbarang($data,'hargabarang_request');
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Barang berhasil disimpan.</div>');
		redirect('sales/index');
	}
}