<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Servis extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        cek_login();
        $this->load->model('Servis_m');
    }
    public function index()
    {
        $data = array(
            'title'    => 'Data Servis',
            'user'     => infoLogin(),
            'toko'     => $this->db->get('profil_perusahaan')->row(),
            'content'  => 'servis/index',
            'servis'   => $this->Servis_m->all_data()

        );
        $this->load->view('templates/main', $data);
    }

    public function pagination()
    {
        $respObj = new ResponseApi();

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $kode_search = $this->input->get('kode_search');
        $no_hp_search = $this->input->get('no_hp_search');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');
        $draw = $this->input->get('draw');

        #get user
        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $servis = $this->Servis_m->get_servis_paginated($order, $limit, $kode_search, $no_hp_search);
        $output['data'] = $servis;
        $output['recordsTotal'] = $this->Servis_m->get_servis_total($kode_search, $no_hp_search);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
          $respObj->setResponse(200, "success", "Get servis is success", $output['data']);
          $resp = $respObj->getResponse();
          setOutput($resp);
          return;
        }else{
          $output['draw'] = $draw;
          $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
          $resp = $respObj->getResponseDatatable();
          setOutput($resp);
          return;
        } 

        // $data = array(
        //     'title'    => 'Data Servis',
        //     'user'     => infoLogin(),
        //     'toko'     => $this->db->get('profil_perusahaan')->row(),
        //     'content'  => 'servis/index',
        //     'servis'   => $this->Servis_m->all_data()

        // );
        // $this->load->view('templates/main', $data);
    }

    public function add()
    {
        $data = array(
            'title'    => 'Entry Data Servis',
            'user'     => infoLogin(),
            'toko'     => $this->db->get('profil_perusahaan')->row(),
            'content'  => 'servis/add',

        );
        $this->load->view('templates/main', $data);
    }

    public function LoadData()
    {
        $data = array(
            "aaData"    => $this->db->get_where('servis', ['status' => 'Aktif'])->result_array()
        );
        echo json_encode($data);
    }
    public function create()
    {
        $this->Servis_m->Save();
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Servis berhasil disimpan.</div>');
        redirect('servis');
    }

    public function detail($id = '')
    {
        $data = $this->Servis_m->Detail($id);
        echo json_encode($data);
    }

    public function update()
    {
        $this->Servis_m->Edit();
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Servis berhasil diubah.</div>');
        redirect('servis');
    }

    public function hapus($id = '')
    {
        $this->Servis_m->Delete($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button><b>Success!</b> Data Servis berhasil dihapus.</div>');
    }

    public function cek_delete($id = '')
    {
        $data = $this->Servis_m->cekDelete($id);
        echo json_encode($data);
    }
}
