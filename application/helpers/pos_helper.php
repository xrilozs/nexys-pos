<?php
function cek_login()
{

	$ci = get_instance();
	if (!$ci->session->userdata('username')) {

		redirect('auth');
	}
}
function cek_user()
{
	$ci = get_instance();
	$user = $ci->db->get_where('user', ['username' => $ci->session->userdata('username')])->row_array();
	if ($user['tipe'] != 'Administrator') {

		redirect('auth/blocked');
	}
}

function cek_user_aou()
{
	$ci = get_instance();
	$user = $ci->db->get_where('user', ['username' => $ci->session->userdata('username')])->row_array();
	
	if ($user['tipe'] != 'OwnerUtama' && $user['tipe'] != 'Administrator') {

		redirect('auth/blocked');
	}
}

function cek_user_edit_barang()
{
	$ci = get_instance();
	$user = $ci->db->get_where('user', ['username' => $ci->session->userdata('username')])->row_array();
	
	if ($user['tipe'] != 'OwnerUtama' && $user['tipe'] != 'Administrator' && $user['tipe'] != 'Gudang') {

		redirect('auth/blocked');
	}
}



function cek_user_aoug()
{
	$ci = get_instance();
	$user = $ci->db->get_where('user', ['username' => $ci->session->userdata('username')])->row_array();
	
	if ($user['tipe'] != 'OwnerUtama' && $user['tipe'] != 'Administrator' && $user['tipe'] != 'Gudang') {

		redirect('auth/blocked');
	}
}
function cek_user_aoc()
{
	$ci = get_instance();
	$user = $ci->db->get_where('user', ['username' => $ci->session->userdata('username')])->row_array();
	if ($user['tipe'] != 'Administrator' && $user['tipe'] != 'OwnerCabang') {

		redirect('auth/blocked');
	}
}

function cek_user_as()
{
	$ci = get_instance();
	$user = $ci->db->get_where('user', ['username' => $ci->session->userdata('username')])->row_array();
	if ($user['tipe'] != 'Administrator' && $user['tipe'] != 'Sales') {

		redirect('auth/blocked');
	}
}

function cek_user_ag()
{
	$ci = get_instance();
	$user = $ci->db->get_where('user', ['username' => $ci->session->userdata('username')])->row_array();
	if ($user['tipe'] != 'Administrator' && $user['tipe'] != 'Gudang') {

		redirect('auth/blocked');
	}
}

function cek_user_ak()
{
	$ci = get_instance();
	$user = $ci->db->get_where('user', ['username' => $ci->session->userdata('username')])->row_array();
	if ($user['tipe'] != 'Administrator' && $user['tipe'] != 'Kasir') {

		redirect('auth/blocked');
	}
}


function cek_user_oc()
{
	$ci = get_instance();
	$user = $ci->db->get_where('user', ['username' => $ci->session->userdata('username')])->row_array();
	if ( $user['tipe'] != 'OwnerCabang') {

		redirect('auth/blocked');
	}
}


function infoLogin()
{
	$ci = get_instance();
	return $ci->db->get_where('user', ['username' => $ci->session->userdata('username')])->row_array();
}

function setOutput($resp){
	$CI =& get_instance();
	$CI->output
			->set_header('Access-Control-Allow-Origin: *')
			->set_status_header($resp['code'])
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($resp));
}
