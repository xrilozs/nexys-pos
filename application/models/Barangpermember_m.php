<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Barangpermember_m extends CI_Model {

    private $table = 'barang_per_member';

    public function get($where = [])
    {
        if ($where) {
            $this->db->where($where);
        }

        $result = $this->db->get($this->table)->result();
        if ($result) {
            foreach ($result as $key => $item) {
                $result[$key]->member = $this->db->get_where('customer', ['id_cs' => $item->id_customer])->row();
                $result[$key]->barang = $this->db->get_where('barang', ['id_barang' => $item->id_barang])->row();
            }
        }

        return $result;
    }

    public function create($input)
    {
        return $this->db->insert($this->table, [
            'id_customer' => $input['id_customer'],
            'id_barang' => $input['id_barang'],
            'harga_diskon' => $input['harga_diskon']
        ]);
    }

    public function update($input)
    {
        $this->db->where(['id' => $input['id']]);
        return $this->db->update($this->table, [
            'id_customer' => $input['id_customer'],
            'id_barang' => $input['id_barang'],
            'harga_diskon' => $input['harga_diskon']
        ]);
    }

    public function delete($id)
    {
        return $this->db->delete($this->table, ['id' => $id]);
    }

}

/* End of file Barangpermember.php */
