<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Func_model extends CI_Model {

    public function notif($message, $type)
    {
        return $this->session->set_flashdata('message', '
        <div class="alert alert-' . $type . ' alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> 
            </button><b>' . $type . '!</b> ' . $message . '.
        </div>
        ');
    }

}

/* End of file Func_model.php */
