<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Imei_barang_m extends CI_Model
{
    protected $table = 'imei_barang';

    public function saveBulk($id_barang, $imeis){
        foreach($imeis as $imei){
            $data = array(
                "id_barang" => $id_barang,
                "nomor_imei" => $imei,
                "is_active" => 1
            );
            $this->db->insert($this->table, $data);
        }
    }

    public function save($id_barang, $imei){
        $data = array(
            "id_barang" => $id_barang,
            "nomor_imei" => $imei,
            "is_active" => 1
        );
        $this->db->insert($this->table, $data);
    }

    public function getImeiBarang($id_barang){
        $this->db->where("id_barang", $id_barang);
        $this->db->where("is_active", 1);
        $query = $this->db->get('imei_barang');
        return $query->result_array();
    }
    public function getImeiBarangByImei($imei){
        $this->db->where("nomor_imei", $imei);
        $this->db->where("is_active", 1);
        $query = $this->db->get('imei_barang');
        return $query->num_rows() > 0 ? $query->row() : null;
    }
    public function useImei($imei){
        $this->db->where(['nomor_imei' => $imei]);
        $this->db->update('imei_barang', ["is_active"=>0]);
    }
}