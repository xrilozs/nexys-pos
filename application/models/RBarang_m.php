<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RBarang_m extends CI_Model
{

    protected $table = 'request_barang';
    protected $primary = 'id_request';

    public function getSomeData()
    {
        //INI OWNER CABANG
        // return $this->db->get($this->table)->result_array();
        $user = infoLogin();
        $id = $user['id_user'];
        $sql = "SELECT  a.stok, a.status,b.barcode, b.kode_barang, b.nama_barang, c.satuan, d.kategori, 
        b.harga_beli, b.range_harga, b.harga_jual, b.id_barang 
        FROM request_barang a 
        LEFT JOIN barang b ON b.id_barang = a.id_barang 
        LEFT JOIN satuan c ON c.id_satuan = b.id_satuan 
        LEFT JOIN kategori d ON d.id_kategori = b.id_kategori 
        WHERE a.id_cabang_request = $id ";

        return $this->db->query($sql)->result_array();

    }

    
    public function DataBarang()
    {
        $sql = "SELECT a.id_barang, a.kode_barang, a.barcode ,a.nama_barang, b.satuan, c.kategori, a.harga_beli, 
        a.range_harga, a.harga_jual, a.stok FROM barang a LEFT JOIN satuan b ON b.id_satuan = a.id_satuan LEFT JOIN kategori c ON c.id_kategori = a.id_kategori WHERE a.is_active = 1";
        return $this->db->query($sql)->result_array();
    }

    public function tambah_rbarang($data,$table)
    {
  
        $this->db->insert($table,$data);
       
    }

    public function updatestok($id_barang,$stok){
        
        $data = $this->db->get_where('barang', ['id_barang' => $id_barang]);
        foreach ($data->result() as $d) {
            $stokb = $d->stok;
        }
        $hasilstok = $stokb-$stok;
        // $this->db->select('(SELECT stok FROM barang WHERE id_barang = $id_barang)');
        // $query = $this->db->get('barang');

        $updatebarang = array(
            'stok'  => $hasilstok
        );
        $this->db->set($updatebarang)->where('id_barang', $id_barang)->update('barang'); 
    }

    public function getAllData()
    {
        // return $this->db->get($this->table)->result_array();
        $user = infoLogin();
        $id = $user['id_user'];
        $sql = "SELECT a.id_request, a.stok, a.status,b.barcode, b.kode_barang, b.nama_barang, c.satuan, d.kategori, 
        b.harga_beli, b.range_harga, b.harga_jual , e.nama_lengkap 
        FROM request_barang a 
        LEFT JOIN barang b ON b.id_barang = a.id_barang 
        LEFT JOIN satuan c ON c.id_satuan = b.id_satuan 
        LEFT JOIN kategori d ON d.id_kategori = b.id_kategori
        LEFT JOIN user e ON e.id_user = a.id_cabang_request";

        return $this->db->query($sql)->result_array();

    }

    public function updatehrgcbg()
    {
        $id_req     = htmlspecialchars($this->input->post('id_req'), true);
        $data = array(
            'harga_cabang' => htmlspecialchars($this->input->post('harga_sekarang'), true)
        );

        return $this->db->set($data)->where($this->primary, $id_req)->update($this->table);
    }
    
    public function Detail($id)
    {
        $sql = "SELECT a.harga_cabang,a.id_request, a.status,b.barcode, b.kode_barang, b.nama_barang, c.satuan, d.kategori, 
        b.id_barang, b.harga_beli,a.stok, b.range_harga, b.harga_jual , e.nama_lengkap , e.alamat_user, e.telp_user
        FROM request_barang a 
        LEFT JOIN barang b ON b.id_barang = a.id_barang 
        LEFT JOIN satuan c ON c.id_satuan = b.id_satuan 
        LEFT JOIN kategori d ON d.id_kategori = b.id_kategori
        LEFT JOIN user e ON e.id_user = a.id_cabang_request
        WHERE a.id_request = $id";

        return $this->db->query($sql)->row_array();
    }

    public function DetailBrg($id)
    {
        $sql = "SELECT a.id_barang, a.nama_barang, a.stok, b.satuan FROM barang a LEFT JOIN satuan b 
        ON b.id_satuan = a.id_satuan WHERE a.id_barang = $id ";
        return $this->db->query($sql)->row_array();
        // return $this->db->get_where('barang', ['id_barang' => $id])->row_array();
    }

    public function update()
    {
        $id = $this->input->post('id_req');
        $id_barang = $this->input->post('id_barang');
        $status = $this->input->post('status');

            $data = array(
                'status'          => htmlspecialchars($this->input->post('status'), true)
            );
            return $this->db->set($data)->where($this->primary, $id)->update($this->table);
        
    }

    public function updatebarangstok()
    {
        $id = $this->input->post('id_req');
        $id_barang = $this->input->post('id_barang');
        $status = $this->input->post('status');

        $datab = $this->db->get_where('barang', ['id_barang' => $id_barang]);
        foreach ($datab->result() as $d) {
            $stokb = $d->stok;
        }

        $datar = $this->db->get_where('request_barang', ['id_request' => $id]);
        foreach ($datar->result() as $d) {
            $stokr = $d->stok;
        }
        
        if ($status == "Dibatalkan") {
            $datab = array(
                'stok'          => $stokb+$stokr
            );

            return $this->db->set($datab)->where('id_barang', $id_barang)->update('barang');
        }else{
            $datab = array(
            'stok'          => $stokb
        );

        return $this->db->set($datab)->where('id_barang', $id_barang)->update('barang');
    

        }
    }

    public function ready()
    {
        //INI OWNER CABANG
        // return $this->db->get($this->table)->result_array();
        $user = infoLogin();
        $id = $user['id_user'];
        $sql = "SELECT  a.harga_cabang, a.id_request, a.stok, a.status,b.barcode, b.kode_barang, b.nama_barang, c.satuan, d.kategori, 
        b.harga_beli, b.range_harga, b.harga_jual, b.id_barang 
        FROM request_barang a 
        LEFT JOIN barang b ON b.id_barang = a.id_barang 
        LEFT JOIN satuan c ON c.id_satuan = b.id_satuan 
        LEFT JOIN kategori d ON d.id_kategori = b.id_kategori 
        WHERE a.id_cabang_request = $id AND  a.status = 'Selesai' ";

        return $this->db->query($sql)->result_array();
    }

    public function showreqharga()
    {
        $user = infoLogin();
        $id_cabang = $user['id_user'];
        $sql = "SELECT a.id_hbr, a.id_request,a.harga_request,a.status, a.id_hbr,a.harga_request, b.harga_cabang, b.id_barang, 
        c.kode_barang, c.barcode, c.nama_barang, d.satuan, e.kategori, f.nama_lengkap, b.stok
        FROM hargabarang_request a 
        LEFT JOIN request_barang b ON b.id_request = a.id_request
        LEFT JOIN barang c ON c.id_barang = b.id_barang
        LEFT JOIN satuan d ON d.id_satuan = c.id_satuan
        LEFT JOIN kategori e ON e.id_kategori = c.id_kategori
        LEFT JOIN user f ON f.id_user = a.id_user
        WHERE a.id_cabangrequest = $id_cabang AND b.id_cabang_request = $id_cabang";

        return $this->db->query($sql)->result_array();
    }

    

    public function Detailreqh($id){
    
        return $this->db->get_where('hargabarang_request', ['id_hbr' => $id])->row_array();
    }

    public function updatestatusharga()
    {
        $id = $this->input->post('id_hbr');

            $data = array(
                'status'          => htmlspecialchars($this->input->post('status'), true)
            );
            return $this->db->set($data)->where('id_hbr', $id)->update('hargabarang_request');
        
    }

    public function showbaranghrg()
    {
        $user = infoLogin();
        $id_user = $user['id_user'];
        $id_cabang = $user['id_ownercabang'];
        $sql = "SELECT a.id_hbr, a.id_request ,a.status, a.id_hbr,a.harga_request, b.harga_cabang, b.id_barang, 
        c.kode_barang, c.barcode, c.nama_barang, d.satuan, e.kategori, f.nama_lengkap, b.stok
        FROM hargabarang_request a 
        LEFT JOIN request_barang b ON b.id_request = a.id_request
        LEFT JOIN barang c ON c.id_barang = b.id_barang
        LEFT JOIN satuan d ON d.id_satuan = c.id_satuan
        LEFT JOIN kategori e ON e.id_kategori = c.id_kategori
        LEFT JOIN user f ON f.id_user = a.id_user
        WHERE a.id_user = $id_user AND a.id_cabangrequest=$id_cabang ";

        return $this->db->query($sql)->result_array();
    }

    public function showbaranghrgready()
    {
        $user = infoLogin();
        $id_user = $user['id_user'];
        $id_cabang = $user['id_ownercabang'];
        $sql = "SELECT a.id_hbr, a.id_request ,a.status, a.id_hbr,a.harga_request, b.harga_cabang, b.id_barang, 
        c.kode_barang, c.barcode, c.nama_barang, d.satuan, e.kategori, f.nama_lengkap, b.stok
        FROM hargabarang_request a 
        LEFT JOIN request_barang b ON b.id_request = a.id_request
        LEFT JOIN barang c ON c.id_barang = b.id_barang
        LEFT JOIN satuan d ON d.id_satuan = c.id_satuan
        LEFT JOIN kategori e ON e.id_kategori = c.id_kategori
        LEFT JOIN user f ON f.id_user = a.id_user
        WHERE a.id_user = $id_user AND a.id_cabangrequest=$id_cabang AND a.status = 'Disetujui'";

        return $this->db->query($sql)->result_array();
    }


    public function detailrequestbrg(){
        $user = infoLogin();
        $id_user = $user['id_user'];
        $id_cabang = $user['id_ownercabang'];
        $sql1 = "SELECT a.id_request, a.id_barang, a.stok, a.status, a.harga_cabang, a.id_cabang_request, 
        b.nama_barang,b.kode_barang, b.barcode, b.id_satuan, b.id_kategori , c.kategori, d.satuan FROM request_barang a LEFT JOIN barang b ON
        b.id_barang = a.id_barang LEFT JOIN kategori c ON c.id_kategori = b.id_kategori LEFT JOIN 
        satuan d ON d.id_satuan = b.id_satuan WHERE a.status='Selesai' AND a.id_cabang_request=$id_cabang";
        return $this->db->query($sql1)->result_array();
    
    }

    public function req_get_item($id){
        
        $data = $this->db->get_where('request_barang', ['id_request' => $id]);
        foreach ($data->result() as $d) {
            $id_barang = $d->id_barang;
        }

        return $this->db->get_where('barang', ['id_barang' => $id_barang])->row_array();
        
        

    }

    public function req_get_itemr($id){
        

        return $this->db->get_where('request_barang', ['id_request' => $id])->row_array();
        
        

    }

    public function tambahrequestharga()
    {
        $this->db->insert($table,$data);
        
    }
    


}
