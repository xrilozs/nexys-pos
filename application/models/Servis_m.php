<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Servis_m extends CI_Model
{

    protected $table = 'servis';
    protected $primary = 'id_servis';

    public function all_data()
    {
        return $this->db->get($this->table)->result_array();
    }

    function get_servis_paginated($order=null, $limit=null, $kode_search=null, $no_hp_search=null){
        $this->db->from("servis");
        if($kode_search){
            $this->db->like("kode", $kode_search); 
        }
        if($no_hp_search){
            $this->db->like("no_hp", $no_hp_search); 
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_servis_total($kode_search=null, $no_hp_search=null){
        if($kode_search){
            $this->db->like("kode", $kode_search); 
        }
        if($no_hp_search){
            $this->db->like("no_hp", $no_hp_search); 
        }
        $this->db->from("servis");
        return $this->db->count_all_results();
    }

    public function Save()
    {
        $data = array(
            'kode'              => 'SV' . date('dmYHis'),
            'nama'              => htmlspecialchars($this->input->post('nama'), true),
            'jenis_hp'          => htmlspecialchars($this->input->post('jenis_hp'), true),
            'imei'              => htmlspecialchars($this->input->post('imei'), true),
            'deskripsi_masalah' => htmlspecialchars($this->input->post('deskripsi_masalah'), true),
            'harga'             => htmlspecialchars($this->input->post('harga'), true),
            'tanggal_service'   => htmlspecialchars($this->input->post('tanggal_service'), true),
            'tanggal_diambil'   => htmlspecialchars($this->input->post('tanggal_diambil'), true),
            'no_hp'             => htmlspecialchars($this->input->post('no_hp'), true),
            'diambil_oleh'      => htmlspecialchars($this->input->post('diambil_oleh'), true),
            'status_service'    => htmlspecialchars($this->input->post('status_service'), true),
            'status_pembayaran' => htmlspecialchars($this->input->post('status_pembayaran'), true),
        );
        return $this->db->insert($this->table, $data);
    }
    public function Edit()
    {
        $id = $this->input->post('id_servis');
        $data = array(
            'nama'              => htmlspecialchars($this->input->post('nama'), true),
            'jenis_hp'          => htmlspecialchars($this->input->post('jenis_hp'), true),
            'imei'              => htmlspecialchars($this->input->post('imei'), true),
            'deskripsi_masalah' => htmlspecialchars($this->input->post('deskripsi_masalah'), true),
            'harga'             => htmlspecialchars($this->input->post('harga'), true),
            'tanggal_service'   => htmlspecialchars($this->input->post('tanggal_service'), true),
            'tanggal_diambil'   => htmlspecialchars($this->input->post('tanggal_diambil'), true),
            'no_hp'             => htmlspecialchars($this->input->post('no_hp'), true),
            'diambil_oleh'      => htmlspecialchars($this->input->post('diambil_oleh'), true),
            'status_service'    => htmlspecialchars($this->input->post('status_service'), true),
            'status_pembayaran' => htmlspecialchars($this->input->post('status_pembayaran'), true),
        );
        return $this->db->set($data)->where($this->primary, $id)->update($this->table);
    }

    public function Detail($id)
    {
        return $this->db->get_where($this->table, [$this->primary => $id])->row_array();
    }

    public function Delete($id)
    {
        return $this->db->where($this->primary, $id)->delete($this->table);
    }

    public function cekDelete($id)
    {
        $sql = "SELECT b.id_detil_jual, a.id_servis FROM servis a, detil_penjualan b WHERE a.id_servis = '$id' GROUP BY a.id_servis";
        $result = $this->db->query($sql)->row_array();
        if ($result['id_detil_jual'] == NULL and $result['id_servis'] == NULL) {
            return array('num' => 0);
        } else {
            return array('num' => 1);
        }
    }
}
