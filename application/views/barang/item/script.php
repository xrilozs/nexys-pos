<script>
    var stockQtyBefore = 0;
    function tambahitem() {
        $('#inputDataBarang').modal('show');
    }

    function importExcel() {
        $('#importBarang').modal('show');
    }

    function hapusbarang(e) {
        Swal.fire({
            title: "Are you sure ?",
            text: "Deleted data can not be restored!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: base_url + "barang/hapusbarang/" + e,
                    type: "post",
                    success: function(data) {
                        window.location = base_url + "barang/index"
                    }
                })
            }
        })
    }

    $('#stok').change(function(){
        let currentStock = $(this).val()
        console.log("STOK: ", currentStock)
        let category = $('#kategori').find("option:selected").text()
        if(category.toLowerCase() == 'handphone'){
            stockQtyBefore = generate_imei_fields(stockQtyBefore, currentStock)
        }
    })

    $("body").delegate(".imei-field", "paste", function(e) {
        const id = $(this).data("id")
        const next_id = parseInt(id) + 1

        setTimeout(function () {
            $(`#imei-${next_id}`).focus()
        }, 0);
    })

    $('#kategori').change(function(){
        console.log("Change kategori")
        let val = $(this).find("option:selected").text();
        let currentStock = $('#stok').val()
        if(val.toLowerCase() == 'handphone'){
            $('.imei-section').show()
            stockQtyBefore = generate_imei_fields(stockQtyBefore, currentStock)
        }else{
            $('.imei-section').hide()
        }
    })

    function setQtyBefore(qty){
        stockQtyBefore = qty
    }

    function generate_imei_fields(stockQtyBefore, qty){
        console.log("BEFORE QTY: ", stockQtyBefore)
        console.log("CURRENT QTY: ", qty)

        let imeis = []
        if(stockQtyBefore > 0){
            for(let i=1; i<=stockQtyBefore; i++){
                imeis.push($(`#imei-${i}`).val())
            }
        }
        console.log("IMEIS: ", imeis)

        let imeiHtml = ''
        for(let i=1; i<=qty; i++){
            let itemHtml = `<input type="text" class="form-control imei-field" data-id="${i}" id="imei-${i}" name="imei[]">`
            if(imeis.length >= i){
                itemHtml = `<input type="text" class="form-control imei-field" data-id="${i}" sid="imei-${i}" name="imei[]" value="${imeis[i-1]}">`
            }
            imeiHtml += itemHtml
        }

        stockQtyBefore = qty
        $('.imei-group').html(imeiHtml)
        return stockQtyBefore
    }
        
</script>