<div class="modal fade" id="editDataBarangPerMember">
 	<div class="modal-dialog">
 		<div class="modal-content">
 			<div class="modal-header">
 				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
 				</button>
 				<h4 class="modal-title" id="editDataBarang">Edit Data Barang per Member</h4>
 			</div>
 			<div class="modal-body">
				 <form class="form-horizontal" method="post" id="editBarangPerMember" action="<?php echo base_url('barang/member_edit') ?>">
				 	<input type="hidden" name="id" id="id_edit">
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12">Member</label>
 						<div class="col-md-9 col-sm-9 col-xs-12">
 							<select id="id_customer_edit" name="id_customer" class="form-control select2" required>
 								<option value="">- Pilih -</option>
 								<?php foreach ($customer as $cst) : ?>
 									<option value="<?php echo $cst->id_cs ?>"><?php echo $cst->nama_cs ?></option>
 								<?php endforeach; ?>
 							</select>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12">Barang</label>
 						<div class="col-md-9 col-sm-9 col-xs-12">
 							<select id="id_barang_edit" name="id_barang" class="form-control select2" required onchange="getHargaJual()">
 								<option value="">- Pilih -</option>
 								<?php foreach ($barang as $brg) : ?>
 									<option data-harga_jual="<?= $brg->harga_jual ?>" value="<?php echo $brg->id_barang ?>"><?php echo $brg->nama_barang ?></option>
 								<?php endforeach; ?>
 							</select>
 						</div>
                     </div>
					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12">Harga Jual</label>
 						<div class="col-md-9 col-sm-9 col-xs-12">
 							<input type="teks" class="form-control" id="harga_jual_edit" name="harga_jual" required autocomplete="off" disabled="true">
 						</div>
 					</div>
					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12">Harga Diskon</label>
 						<div class="col-md-9 col-sm-9 col-xs-12">
 							<input type="number" class="form-control" id="harga_diskon_edit" name="harga_diskon" required autocomplete="off">
 						</div>
 					</div>
 					<div class="modal-footer">
 						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 						<button type="submit" class="btn btn-primary">Save changes</button>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 </div>