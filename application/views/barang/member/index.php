<style>
    .select2-container {
        z-index: 1051;
    }
</style>
<?php cek_user_aoug() ?>
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?php echo $title ?></h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <button type="button" class="btn btn-sm btn-primary" title="Tambah Data" data-toggle="modal" data-target="#inputDataBarangPerMember"><i class="fa fa-plus"></i> Tambah Data</button>
                        <!-- <button type="button" class="btn btn-sm btn-default" title="Import Data" id="importExcel"><i class="fa fa-upload"></i> Import Excel</button> -->
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-4"></div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <select name="id_customer_search" class="form-control">
                                    <option value="">Semua</option>
                                    <?php foreach ($customer as $item): ?>
                                        <option value="<?= $item->id_cs ?>" <?= $_GET['id_cs'] == $item->id_cs ? 'selected="true"' : '' ?>><?= $item->nama_cs ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <script>
                                document.querySelector('[name=id_customer_search]').addEventListener('change', function() {
                                    window.location = window.location.href.replace(window.location.search, '') + '?id_cs=' + this.value;
                                })
                            </script>
                        </div>
                        <?php echo $this->session->flashdata('message'); ?>
                        <table width="100%" class="table table-striped table-bordered datatable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Member</th>
                                    <th>Barang</th>
                                    <th>Harga Asli</th>
                                    <th>Harga Diskon</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($member as $key => $item) : ?>
                                    <tr>
                                        <td><?= $key + 1 ?></td>
                                        <td><?= $item->member->nama_cs ?></td>
                                        <td><?= $item->barang->nama_barang ?></td>
                                        <td>Rp.<?= number_format($item->barang->harga_jual, 0, '.', '.') ?></td>
                                        <td>Rp.<?= number_format($item->harga_diskon, 0, '.', '.') ?></td>
                                        <td class="text-center">
                                            <a href="#" class="btn btn-primary btn-xs" title="Hapus Data" data-toggle="modal" data-target="#editDataBarangPerMember" onclick="editBpm('<?php echo $item->id ?>', {
                                                id_customer: '<?= $item->id_customer ?>',
                                                id_barang: '<?= $item->id_barang ?>',
                                                harga_jual: '<?= $item->barang->harga_jual ?>',
                                                harga_diskon: '<?= $item->harga_diskon ?>'
                                            })"><i class="fa fa-edit "></i></a>
                                            <a href="#" class="btn btn-danger btn-xs" title="Hapus Data" onclick="hapusBpm('<?php echo $item->id ?>')"><i class="fa fa-trash "></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
        include 'input.php';
        include 'edit.php';
    ?>
</div>
<script>
    function getHargaJual() {
        const select_barang = document.querySelector('#id_barang');
        const harga_jual = select_barang.options[select_barang.options.selectedIndex].dataset.harga_jual;
        document.getElementById('harga_jual').value = 'Rp.' + parseInt(harga_jual).toLocaleString('id-ID');
    }

    function editBpm(id, obj) {
        document.getElementById('id_edit').value = id;
        document.getElementById('id_customer_edit').value = obj.id_customer;
        document.getElementById('id_barang_edit').value = obj.id_barang;
        document.getElementById('harga_jual_edit').value = obj.harga_jual;
        document.getElementById('harga_diskon_edit').value = obj.harga_diskon;
    }

    function hapusBpm(id) {
        Swal.fire({
            title: "Are you sure ?",
            text: "Deleted data can not be restored!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then((result) => {
            if (result.value) {
                window.location = base_url + 'barang/member_hapus/' + id;
            }
        })
    }
</script>