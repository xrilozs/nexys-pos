		 <?php //cek_user() 
			?>
		 <div class="right_col" role="main">
		 	<div class="">
		 		<div class="page-title">
		 			<div class="title_left">
		 				<h3><?php echo $title ?></h3>
		 			</div>
		 		</div>
		 		<div class="clearfix"></div>
		 		<div class="row">
		 			<div class="col-md-12 col-sm-12 col-xs-12">
		 				<div class="x_panel">
		 					<div class="x_title">
		 						<?php include 'inputcs.php' ?>
		 						<button type="button" class="btn btn-sm btn-primary" onclick="tambahcustomer()" title="Tambah Data" id="tambahkaryawan"><i class="fa fa-plus"></i> Tambah Data</button>
		 						<!-- <a href="<?php //echo base_url('customer/poin') 
												?>" class="btn btn-sm btn-primary" title="Customer Poin" id="customerPoin"><i class="fa fa-heart"></i> Customer Poin</a> -->
		 						<?php if ($this->session->userdata('tipe') !== 'Sales') : ?>
		 							<a href="<?php echo base_url('report/customer') ?>" class="btn btn-sm btn-primary" title="Export Data" target="_blank"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
		 						<?php endif ?>
		 						<button type="button" class="btn btn-sm btn-default" onclick="importCus()" title="Import Data" id="importCus"><i class="fa fa-upload"></i> Import Data</button>
		 						<ul class="nav navbar-right panel_toolbox">
		 							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		 							</li>
		 							<li><a class="close-link"><i class="fa fa-close"></i></a>
		 							</li>
		 						</ul>
		 						<div class="clearfix"></div>
		 					</div>
		 					<div class="x_content">
		 						<script>
		 							var user_tipe = '<?= $this->session->userdata('tipe'); ?>'
		 						</script>
		 						<?php echo $this->session->flashdata('message'); ?>
		 						<table id="datacustomer" width="100%" class="table table-striped table-bordered">
		 							<thead>
		 								<tr>
		 									<th>Kode</th>
		 									<th>Nama Customer</th>
		 									<th>Telp</th>
		 									<th>Email</th>
		 									<th>Alamat</th>
		 									<th>Jenis</th>
											<th>Keterangan</th>
		 									<?php if ($this->session->userdata('tipe') == 'Sales') : ?>
		 										<th>Status</th>
		 									<?php else : ?>
		 										<th>Opsi</th>
		 									<?php endif ?>
		 								</tr>
		 							</thead>
		 						</table>
		 					</div>
		 				</div>
		 				<div class="x_panel">
		 					<div class="x_title">
		 						<div class="row">
		 							<div class="col-sm-6">
		 								Request Harga Customer
		 							</div>
		 							<div class="col-sm-6 text-right">
		 								<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#tambahrequest">
		 									<i class="fa fa-plus"></i>
		 									Tambah Request
		 								</button>
		 							</div>
		 						</div>
		 					</div>
		 					<section id="request" class="x_content">
		 						<table class="table table-striped table-bordered datatable">
		 							<thead>
		 								<tr>
		 									<th>No</th>
		 									<th>Nama Customer</th>
		 									<th>Nama Barang</th>
		 									<th>Harga Jual</th>
		 									<th>Harga Request</th>
											<th>Status</th>
											<?php if ($this->session->userdata('tipe') == 'Administrator'): ?>
												<th>Opsi</th>
											<?php endif ?>
		 								</tr>
		 							</thead>
		 							<tbody>
		 								<?php foreach ($customer_request_barang as $key => $item) : ?>
		 									<tr>
		 										<td><?= $key + 1 ?></td>
		 										<td><?= $item->customer->nama_cs ?></td>
		 										<td><?= $item->barang->nama_barang ?></td>
		 										<td>Rp.<?= number_format($item->barang->harga_jual) ?></td>
		 										<td>Rp.<?= number_format($item->harga) ?></td>
												<td><span class="badge badge-<?= $item->status == '1' ? 'success' : 'warning' ?>"><?= $item->status == '1' ? 'Disetujui' : 'Ditolak' ?></span></td>
		 										<?php if ($this->session->userdata('tipe') == 'Administrator') : ?>
													<td>
														<?php if ($item->status == '1') : ?>
															<a href="<?= base_url('barang/request/change/' . $item->id . '/0') ?>" class="btn btn-warning btn-xs">Tolak</a>
														<?php else : ?>
															<a href="<?= base_url('barang/request/change/' . $item->id . '/1') ?>" class="btn btn-success btn-xs">Setujui</a>
														<?php endif ?>
														<a href="<?= base_url('barang/request/delete/' . $item->id) ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
													</td>
		 										<?php endif ?>
		 									</tr>
		 								<?php endforeach ?>
		 							</tbody>
		 						</table>
		 					</section>
		 				</div>
		 			</div>
		 		</div>
		 	</div>
		 </div>
		 <div class="modal fade" id="tambahrequest">
			 <div class="modal-dialog">
				<form class="modal-content" method="post" action="<?= base_url('barang/request/add') ?>">
					 <div class="modal-header">
						 <div class="modal-title">Tambah Request</div>
					 </div>
					 <div class="modal-body">
						 <div class="form-horizontal">
							<div class="form-group">
								<style>
									.select2-container {
										z-index: 1200;
									}
								</style>
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Customer</label>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<select id="id_customer" name="id_customer" class="form-control select2" required>
										<option value="">- Pilih -</option>
										<?php foreach ($customer as $key => $item): ?>
											<option value="<?= $item->id_cs ?>"><?= $item->nama_cs ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Barang</label>
								<div id="barangRequest" class="col-md-9 col-sm-9 col-xs-12 row">
								    <div class="col-xs-6 mb-2">
    									<select name="id_barang[]" class="form-control id_barang" required>
    										<option value="">- Pilih -</option>
    										<?php foreach ($barang as $key => $item): ?>
    											<option value="<?= $item->id_barang ?>"><?= $item->nama_barang ?> (Rp.<?= number_format($item->harga_jual) ?>)</option>
    										<?php endforeach ?>
    									</select>
    								</div>
									<div class="col-xs-6 mb-2">
									    <input type="text" class="form-control harga" name="harga[]" autocomplete="off" placeholder="Req. Harga">
									</div>
								</div>
							</div>
						</div>
					 </div>
					 <div class="modal-footer">
					     <button class="btn btn-warning text-white" id="tambahRequest" type="button">Tambah Request</button>
						 <button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>
						 <button type="submit" class="btn btn-primary">Simpan</button>
					 </div>
				</form>
			 </div>
		 </div>
		 <?php include 'editcs.php' ?>
		 <?php include 'import.php' ?>
		 <?php include 'script.php' ?>
		 <script>
		     document.getElementById('tambahRequest').addEventListener('click', function() {
		         const requestBarang = document.getElementById('barangRequest');
		         const html = `
		         <div class="col-xs-6 mb-2">
					<select name="id_barang[]" class="form-control id_barang" required>
						<option value="">- Pilih -</option>
						<?php foreach ($barang as $key => $item): ?>
							<option value="<?= $item->id_barang ?>"><?= $item->nama_barang ?> (Rp.<?= number_format($item->harga_jual) ?>)</option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-xs-6 mb-2">
				    <input type="text" class="form-control harga" name="harga[]" autocomplete="off" placeholder="Req. Harga">
				</div>
		         `;
		         
		         const valueIdBarang = [];
		         document.querySelectorAll('.id_barang').forEach(element => {
		             valueIdBarang.push(element.value);
		         })
		         const valueHarga = [];
		         document.querySelectorAll('.harga').forEach(element => {
		             valueHarga.push(element.value);
		         })
		         
		         requestBarang.innerHTML += html;
		         
		         document.querySelectorAll('.id_barang').forEach((element, i) => {
		             element.value = valueIdBarang[i] ? valueIdBarang[i] : "";
		         });
		         document.querySelectorAll('.harga').forEach((element, i) => {
		             element.value = valueHarga[i] ? valueHarga[i] : "";
		         });
		     })
		 </script>