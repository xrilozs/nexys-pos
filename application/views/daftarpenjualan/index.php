		 <div class="right_col" role="main">
		 	<div class="">
		 		<div class="page-title">
		 			<div class="title_left">
		 				<h3><?php echo $title ?></h3>
		 			</div>
		 		</div>
		 		<div class="clearfix"></div>
		 		<div class="row">
		 			<div class="col-md-12 col-sm-12 col-xs-12">
		 				<div class="x_panel">
		 					<div class="x_title">
		 						<ul class="nav navbar-right panel_toolbox">
		 							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		 							</li>
		 							<li><a class="close-link"><i class="fa fa-close"></i></a>
		 							</li>
		 						</ul>
		 						<div class="clearfix"></div>
		 					</div>
		 					<div class="x_content">
								<div class="row" style="margin-bottom:10px;">
									<div class="col-lg-6 col-md-6">
										<label for="">Filter Kasir</label>
										<select name="kasir-option" id="kasir-option" class="form-control">
											<option value="">--Pilih Kasir--</option>
											<?php foreach ($kasir as $item) : ?>
												<option value="<?=$item->nama_lengkap?>"><?=$item->nama_lengkap?></option>
											<?php endforeach ?>
										</select>
									</div>
									<div class="col-lg-6 col-md-6">
										<label for="">Filter Tanggal</label>
										<input type="date" id="date-option" class="form-control datepicker" name="date-option" />
									</div>
								</div>
		 						<?php echo $this->session->flashdata('message'); ?>
		 						<table id="daftarjual" width="100%" class="table table-striped table-bordered">
		 							<thead>
		 								<tr>
		 									<th>Invoice</th>
		 									<th>Kasir</th>
		 									<th>Customer</th>
		 									<th>Diskon</th>
		 									<th>Total</th>
		 									<th>Payment Method</th>
		 									<th>Qty</th>
		 									<th>Waktu</th>
		 									<th>Opsi</th>
		 								</tr>
		 							</thead>
		 						</table>
		 					</div>
		 				</div>
		 			</div>
		 		</div>
		 	</div>
		 </div>
		 <?php include 'detildaftar.php' ?>
		 <?php include 'script.php' ?>