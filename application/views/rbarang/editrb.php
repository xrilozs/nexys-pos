<?php cek_user_aoc() ?>
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3><?php echo $title ?></h3>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
                            <form class="form-horizontal" method="post" action="<?php echo base_url('rbarang/updatehrgcbg') ?>">

                                <input type="text" value="<?php echo $item['id_request']?>" name="id_req" hidden>
                                
                                <input type="text" value="<?php echo $item['id_barang']?>" name="id_barang" hidden>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Barang</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" readonly class="form-control" id="namabarang" name="namabarang" value="<?php echo $item['nama_barang']?>" autocomplete="off">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Kode Item</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input  readonly type="text" class="form-control" id="barcodeitem" name="kode_item" autocomplete="off" value="<?php echo $item['kode_barang']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" readonly class="form-control" id="barcodeitem" name="kategori" autocomplete="off" value="<?php echo $item['kategori']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Stok</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" readonly class="form-control" id="barcodeitem" name="stok" autocomplete="off" value="<?php echo $item['stok']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Harga Pusat</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" readonly class="form-control" id="barcodeitem" name="harga_pusat" autocomplete="off" value="<?php echo $item['harga_beli']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Range Harga</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="number" readonly class="form-control" id="beli" name="range_harga" value="<?php echo $item['range_harga']?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Harga Sekarang</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="number"  class="form-control" id="beli" name="harga_sekarang" value="<?php echo $item['harga_cabang']?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <?php if($item['status']=="Dibatalkan"){?>
                                    <a href="<?php echo base_url('rbarang/gudang')?>" class="btn btn-primary">Kembali</a>
                                    <?php } else{?>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    <?php }?>
                                </div>
                            </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>