<?php cek_user_ag() ?>
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3><?php echo $title ?></h3>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
                            <form class="form-horizontal" method="post" action="<?php echo base_url('rbarang/updatestatus') ?>">

                                <input type="text" value="<?php echo $item['id_request']?>" name="id_req" hidden>
                                
                                <input type="text" value="<?php echo $item['id_barang']?>" name="id_barang" hidden>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Cabang</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input  readonly type="text" class="form-control" id="barcodeitem" name="nama_cabang" autocomplete="off" value="<?php echo $item['nama_lengkap']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Cabang</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" readonly class="form-control" id="barcodeitem" name="alamat_cabang" autocomplete="off" value="<?php echo $item['alamat_user']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Telepon Cabang</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" readonly class="form-control" id="barcodeitem" name="telp_cabang" autocomplete="off" value="<?php echo $item['telp_user']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Barang</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" readonly class="form-control" id="namabarang" name="namabarang" value="<?php echo $item['nama_barang']?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Stok</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="number" readonly class="form-control" id="beli" name="beli" value="<?php echo $item['stok']?>" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <select id="kategori" name="status" class="form-control" required>
                                        <?php if ($item['status']=="Pending") {?>    
                                            <option value="<?php echo $item['status']?>"><?php echo $item['status']?></option>
                                            <option value="Dibatalkan">Dibatalkan</option>
                                            <option value="Diproses">Diproses</option>
                                            <option value="Dikirim">Dikirim</option>
                                            <option value="Selesai">Selesai</option>
                                        <?php } elseif ($item['status']=="Dibatalkan") {?>
                                            <option value="<?php echo $item['status']?>"><?php echo $item['status']?></option>
                                        
                                        <?php } elseif ($item['status']=="Diproses") {?>
                                            <option value="<?php echo $item['status']?>"><?php echo $item['status']?></option>
                                            <option value="Dikirim">Dikirim</option>
                                            <option value="Selesai">Selesai</option>
                                        
                                        <?php } elseif ($item['status']=="Dikirim") {?>
                                            <option value="<?php echo $item['status']?>"><?php echo $item['status']?></option>
                                            <option value="Selesai">Selesai</option>
                                        
                                        <?php } elseif ($item['status']=="Selesai") {?>
                                            <option value="<?php echo $item['status']?>"><?php echo $item['status']?></option>
                                        <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <?php if($item['status']=="Dibatalkan"){?>
                                    <a href="<?php echo base_url('rbarang/gudang')?>" class="btn btn-primary">Kembali</a>
                                    <?php } else{?>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    <?php }?>
                                </div>
                            </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>