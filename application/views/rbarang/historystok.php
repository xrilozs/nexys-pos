<?php cek_user_ag() ?>
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3><?php echo $title ?></h3>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<table width="100%" class="table table-striped table-bordered datatable">
									<thead>
										<tr>
											<th>No</th>
											<th>User</th>
											<th>Barang</th>
											<th>Stok Awal</th>
											<th>Stok Akhir</th>
                                            <th>Created At</th>
										</tr>
									</thead>
									<tbody>
										<!-- <?php foreach ($history as $key => $item) { ?> -->
											<tr>
												<td><?php echo $key + 1 ?></td>
												<td><?php echo $this->db->get_where('user', ['id_user' => $item['id_user']])->row()->username ?></td>
												<td><?php echo $this->db->get_where('barang', ['id_barang' => $item['id_barang']])->row()->nama_barang ?></td>
												<td><?php echo $item['stok_awal'] ?></td>
												<td><?php echo $item['stok_akhir'] ?></td>
                                                <td><?php echo $item['created_at']?></td>
											</tr>
										<!-- <?php } ?> -->
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>