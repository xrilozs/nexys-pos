<?php cek_user_aoc() ?>
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3><?php echo $title ?></h3>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
						<?php echo $this->session->flashdata('message'); ?>
							<div class="x_title">
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<form class="form-horizontal" method="post" action="<?php echo base_url('rbarang/tambahrbarang') ?>">
									<div class="form-group">
										<input type="hidden" class="form-control" id="iditem" name="id_barang" value="<?php echo $item['id_barang'] ?>">
										<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
											<label for="">Nama Barang</label>
											<input type="text" class="form-control" name="nama_barang" value="<?php echo $item['nama_barang'] ?>" autocomplete="off" required />
										</div>
										<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
											<label for="">Stok(Maksimal <?php echo $item['stok'] ." ". $item['satuan']?>)</label>
											<input type="number" id="namabarang" class="form-control"  name="stok" min="1" max="<?php echo $item['stok']?>" autocomplete="off" required />
										</div>
									</div>
									
									<div class="form-group">
										<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Edit</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>