<?php cek_user_oc() ?>
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3><?php echo $title ?></h3>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
                            <form class="form-horizontal" method="post" action="<?php echo base_url('rbarang/updatestatusharga') ?>">

                                <input type="text" value="<?php echo $item['id_hbr']?>" name="id_hbr" hidden>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <select id="kategori" name="status" class="form-control" required>
                                        <?php if ($item['status']=="Pending") {?>    
                                            <option value="<?php echo $item['status']?>"><?php echo $item['status']?></option>
                                            <option value="Dibatalkan">Dibatalkan</option>
                                            <option value="Disetujui">Disetujui</option>
                                        <?php } elseif ($item['status']=="Dibatalkan") {?>
                                            <option value="<?php echo $item['status']?>"><?php echo $item['status']?></option>
                                        <?php } elseif ($item['status']=="Disetujui") {?>
                                            <option value="<?php echo $item['status']?>"><?php echo $item['status']?></option>
                                        <?php }?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                        <a href="<?php echo base_url('rbarang/reqharga')?>" class="btn btn-warning">Kembali</a>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>