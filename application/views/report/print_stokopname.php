<?php
/* Change to the correct path if you copy this example! */
// require _DIR_ . '/../../autoload.php';
require APPPATH.'views/autoload.php';

use Mike42\Escpos\EscposImage;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
      $connector = new WindowsPrintConnector("Generic");
       $printer = new Printer($connector);
      try {
        $sql = "SELECT a.id_stok_opname, b.barcode, b.nama_barang, a.stok, a.stok_nyata, a.selisih, a.keterangan, 
         a.nilai FROM stok_opname a, barang b WHERE a.id_barang = b.id_barang AND SUBSTRING(a.tanggal, 1, 10) BETWEEN '$awal' AND '$akhir'";
        $data = $this->model->General($sql)->result_array();
    $img =  EscposImage::load("./assets/img/profil/logo112.png");
    $printer -> text("           ".$profil['nama_toko']."           ");
    $printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer->bitImage($img);
    $printer -> setJustification(Printer::JUSTIFY_LEFT);
    $printer -> text("\n Jl. Krakatau No.60, Pd. Waluh, \n     Kencong , Kec. Kencong     \n");
    $printer -> text('          '.$profil['telp_toko']."         \n\n");
    $printer -> text('Tgl. Print Stok:'.date('d-m-Y', strtotime($awal))."- ".date('d-m-Y H:i:s', strtotime($akhir))."\n\n");

    $printer -> text("\n====== Laporan stokopname ======\n");
    foreach ($data as $key => $item) {
        $printer->text('Kode Barang : '.$item['barcode']."\n"); 
        $printer->text('Nama Barang : '.$item['nama_barang']."\n"); 
        $printer->text('Stok Nyata  : '.$item['stok_nyata']."\n"); 
        $printer -> text("--------------------------------\n");                
    } 

    $printer -> text("\n--------------------------------\n");
    $printer -> text("Terima kasih telah berbelanja di\n");
    $printer -> text("           ".$profil['nama_toko']."           \n");
    $printer -> text("--------------------------------\n");    
    $printer -> text(" \n\n\n");                



    $printer -> pulse();
    $printer -> cut();
    
    /* Close printer */
    $printer -> close();
    $printer -> pulse();
    redirect('laporan/stokopname');
    
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
}


     
       
/**
 * Install the printer using USB printing support, and the "Generic / Text Only" driver,
 * then share it (you can use a firewall so that it can only be seen locally).
 *
 * Use a WindowsPrintConnector with the share name to print.
 *
 * Troubleshooting: Fire up a command prompt, and ensure that (if your printer is shared as
 * "Receipt Printer), the following commands work:
 *
 *  echo "Hello World" > testfile
 *  copy testfile "\\%H61-PC%\Generic"
 *  del testfile
 */