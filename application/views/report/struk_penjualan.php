<?php
/* Change to the correct path if you copy this example! */
// require _DIR_ . '/../../autoload.php';
require APPPATH.'views/autoload.php';
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
      $connector = new WindowsPrintConnector("Generic");
       $printer = new Printer($connector);
      try {

        $max_id = "SELECT MAX(id_jual) AS id_jual FROM penjualan";
        $id = implode($this->db->query($max_id)->row_array());
        $penjualan = $this->db->get_where('penjualan', ['id_jual' => $id])->row();
    
        if ($id_resi == null) {
            $sql_general = "SELECT a.invoice, a.bayar, a.kembali, a.ppn, a.tgl, b.username, b.id_user, a.id_cs, a.method FROM penjualan a, user b WHERE a.id_user = b.id_user AND a.id_jual = '$id'";
            $sql_detail = "SELECT b.id_barang, b.kode_barang, b.nama_barang, b.harga_jual, a.qty_jual, a.diskon, a.subtotal FROM detil_penjualan a,  barang b
            WHERE a.id_barang = b.id_barang AND a.id_jual = '$id'";
            $sql_total = "SELECT SUM(subtotal) AS total, SUM(diskon) AS diskon FROM detil_penjualan WHERE id_jual = '$id'";
            $sql_servis = "SELECT a.id_detil_jual, a.harga_item, a.qty_jual, a.subtotal 
            FROM detil_penjualan a WHERE a.id_jual= '$id'";
        } else {
            $sql_general = "SELECT a.invoice, a.bayar, a.kembali, a.ppn, a.tgl, b.username, b.id_user, a.id_cs, a.method FROM penjualan a, user b WHERE a.id_user = b.id_user AND a.id_jual = '$id_resi'";
            $sql_detail = "SELECT b.id_barang, b.kode_barang, b.nama_barang, b.harga_jual, a.qty_jual, a.diskon, a.subtotal FROM detil_penjualan a,  barang b
            WHERE a.id_barang = b.id_barang AND a.id_jual = '$id_resi'";
            $sql_total = "SELECT SUM(subtotal) AS total, SUM(diskon) AS diskon FROM detil_penjualan WHERE id_jual = '$id_resi'";
            $sql_servis = "SELECT a.id_detil_jual, a.harga_item, a.qty_jual, a.subtotal 
            FROM detil_penjualan a WHERE a.id_jual= '$id_resi'";
        }
        $general = $this->db->query($sql_general)->row_array();
        $detail = $this->db->query($sql_detail)->result_array();
        $total = $this->db->query($sql_total)->row_array();
        $servis = $this->db->query($sql_servis)->result_array();
    
        $harga_total = $total['total'];
        $detil_penjualan = $this->db->get_where('detil_penjualan', ['id_jual' => $id ? $id : $id_resi])->result();
        foreach ($detil_penjualan as $key => $item) {
            $harga = $item->harga_item;
            $where = [
                'id_barang' => $item->id_barang,
                'id_customer' => $penjualan->id_cs
            ];
            if ($barang_per_member = $this->db->get_where('barang_per_member', $where)->row()) {
                $harga = $barang_per_member->harga_diskon;
            }
        }
    
        foreach ($detail as $key => $item) {
            $where = [
                'id_barang' => $item['id_barang'],
                'id_customer' => $penjualan->id_cs
            ];
    
            if ($barang_per_member = $this->db->get_where('barang_per_member', $where)->row()) {
                $detail[$key]['diskon'] = $barang_per_member->harga_diskon;
                $detail[$key]['subtotal'] = $item['qty_jual'] * $detail[$key]['diskon'];
                $harga_total += $detail[$key]['subtotal'];
            }
    
            if ($request = $this->db->get_where('customer_request_barang', array_merge($where, ['status' => '1']))->row()) {
                $detail[$key]['diskon'] = $request->harga;
                $detail[$key]['subtotal'] = $item['qty_jual'] * $detail[$key]['diskon'];
                $harga_total += $detail[$key]['subtotal'];
            }
    
        }
    
    $customer = $this->db->get_where('customer', ['id_cs' => $penjualan->id_cs])->row();
    $dataDetailX = $this->db->get_where('detil_penjualan', ['id_jual' => $id])->result();
    $totalX = 0;
    foreach ($dataDetailX as $val) {
        $totalX += $val->harga_item * $val->qty_jual;
    }
    $img =  EscposImage::load("./assets/img/profil/logo112.png");
    $printer -> text("           ".$profil['nama_toko']."           ");
    $printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer->bitImage($img);
    $printer -> setJustification(Printer::JUSTIFY_LEFT);
    $printer -> text("\n Jl. Krakatau No.60, Pd. Waluh, \n     Kencong , Kec. Kencong     \n");
    $printer -> text('          '.$profil['telp_toko']."         \n\n");
    $printer -> text("customer :".strtoupper($customer->nama_cs)."\n");
    $printer -> text("Nota     :".$general['invoice']."\n");
    $printer -> text('             '.date('d-m-Y H:i:s', strtotime($general['tgl'])));

    $printer -> text("\n================================\n");
    foreach ($detail as $key => $item) {
        $printer->text($item['nama_barang']."\n");
        $printer->text('Rp.'.number_format($item['harga_jual'], 0, '.', '.')." X(");
        $printer->text($item['qty_jual']." pcs) = "); 
        $printer->text('Rp.'.number_format($item['harga_jual'] * $item['qty_jual'], 0, '.', '.')."\n");
        $printer -> text("--------------------------------\n");                
    }

    // $ppnPersen = $totalX / $penjualan->ppn * 100;
    $printer->text('   Subtotal    : Rp.'.number_format($totalX, 0, '.', '.')."\n");
    $printer->text('   Diskon      : Rp.'.number_format($penjualan->diskon, 0, '.', '.')."\n");
    $printer->text('   PPN         : Rp.'.number_format($penjualan->ppn, 0, '.', '.')."\n");
    $printer->text('   Total       : Rp.'.number_format($totalX + $general['ppn'] - $penjualan->diskon, 0, '.', '.')."\n");
    $printer->text('   Bayar       : Rp.'.number_format($penjualan->bayar, 0, '.', '.')."\n");
    $printer->text('   Kembalian   : Rp.'.number_format($penjualan->kembali, 0, '.', '.')."\n");

    $printer -> text("\n--------------------------------\n");
    $printer -> text("Terima kasih telah berbelanja di\n");
    $printer -> text("           ".$profil['nama_toko']."           \n");
    $printer -> text("--------------------------------\n");    
    $printer -> text(" \n\n\n");                



    $printer -> pulse();
    $printer -> cut();
    
    /* Close printer */
    $printer -> close();
    $printer -> pulse();
    redirect('penjualan');
    
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
}


     
       
/**
 * Install the printer using USB printing support, and the "Generic / Text Only" driver,
 * then share it (you can use a firewall so that it can only be seen locally).
 *
 * Use a WindowsPrintConnector with the share name to print.
 *
 * Troubleshooting: Fire up a command prompt, and ensure that (if your printer is shared as
 * "Receipt Printer), the following commands work:
 *
 *  echo "Hello World" > testfile
 *  copy testfile "\\%H61-PC%\Generic"
 *  del testfile
 */