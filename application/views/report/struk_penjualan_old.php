<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Report Struk Penjualan</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
    </style>
</head>

<body>
    <?php
    require _DIR_ . '/../../autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
      $connector = new WindowsPrintConnector("Generic");
       $printer = new Printer($connector);
       try {
    $max_id = "SELECT MAX(id_jual) AS id_jual FROM penjualan";
    $id = implode($this->db->query($max_id)->row_array());
    $penjualan = $this->db->get_where('penjualan', ['id_jual' => $id])->row();

    if ($id_resi == null) {
        $sql_general = "SELECT a.invoice, a.bayar, a.kembali, a.ppn, a.tgl, b.username, b.id_user, a.id_cs, a.method FROM penjualan a, user b WHERE a.id_user = b.id_user AND a.id_jual = '$id'";
        $sql_detail = "SELECT b.id_barang, b.kode_barang, b.nama_barang, a.qty_jual, a.diskon, a.subtotal FROM detil_penjualan a,  barang b
        WHERE a.id_barang = b.id_barang AND a.id_jual = '$id'";
        $sql_total = "SELECT SUM(subtotal) AS total, SUM(diskon) AS diskon FROM detil_penjualan WHERE id_jual = '$id'";
        $sql_servis = "SELECT a.id_detil_jual, a.harga_item, a.qty_jual, a.subtotal 
        FROM detil_penjualan a WHERE a.id_jual= '$id'";
    } else {
        $sql_general = "SELECT a.invoice, a.bayar, a.kembali, a.ppn, a.tgl, b.username, b.id_user, a.id_cs, a.method FROM penjualan a, user b WHERE a.id_user = b.id_user AND a.id_jual = '$id_resi'";
        $sql_detail = "SELECT b.id_barang, b.kode_barang, b.nama_barang, a.qty_jual, a.diskon, a.subtotal FROM detil_penjualan a,  barang b
        WHERE a.id_barang = b.id_barang AND a.id_jual = '$id_resi'";
        $sql_total = "SELECT SUM(subtotal) AS total, SUM(diskon) AS diskon FROM detil_penjualan WHERE id_jual = '$id_resi'";
        $sql_servis = "SELECT a.id_detil_jual, a.harga_item, a.qty_jual, a.subtotal 
        FROM detil_penjualan a WHERE a.id_jual= '$id_resi'";
    }
    $general = $this->db->query($sql_general)->row_array();
    $detail = $this->db->query($sql_detail)->result_array();
    $total = $this->db->query($sql_total)->row_array();
    $servis = $this->db->query($sql_servis)->result_array();

    $harga_total = 0;
    $detil_penjualan = $this->db->get_where('detil_penjualan', ['id_jual' => $id ? $id : $id_resi])->result();
    foreach ($detil_penjualan as $key => $item) {
        $harga = $item->harga_item;
        $where = [
            'id_barang' => $item->id_barang,
            'id_customer' => $penjualan->id_cs
        ];
        if ($barang_per_member = $this->db->get_where('barang_per_member', $where)->row()) {
            $harga = $barang_per_member->harga_diskon;
        }
    }

    foreach ($detail as $key => $item) {
        $where = [
            'id_barang' => $item['id_barang'],
            'id_customer' => $penjualan->id_cs
        ];

        if ($barang_per_member = $this->db->get_where('barang_per_member', $where)->row()) {
            $detail[$key]['diskon'] = $barang_per_member->harga_diskon;
            $detail[$key]['subtotal'] = $item['qty_jual'] * $detail[$key]['diskon'];
        }

        if ($request = $this->db->get_where('customer_request_barang', array_merge($where, ['status' => '1']))->row()) {
            $detail[$key]['diskon'] = $request->harga;
            $detail[$key]['subtotal'] = $item['qty_jual'] * $detail[$key]['diskon'];
        }

        $harga_total += $detail[$key]['subtotal'];
    }

    $customer = $this->db->get_where('customer', ['id_cs' => $penjualan->id_cs])->row();
    ?>
    <style>
        .container {
            padding: 2px;
            max-width: 219px;
            width: 219px;
        }
    </style>
    <div class="container">
        <div align="center">
            <h3>
                <?= $profil['nama_toko'] ?>
                <br>
                <small>
                    <?= $profil['alamat_toko'] ?>
                    <br>
                    Telp: <?= $profil['telp_toko'] ?> / Fax: <?= $profil['fax_toko'] ?>
                </small>
            </h3>
        </div>
        <br>
        <div style="text-align: left;">
            <?= date('Y-m-d') ?>
            <br />
            <div>Kepada Customer: </div>
            <b><?= strtoupper($customer->nama_cs) ?></b>
        </div>
        <div style="display: flex; margin-top: 15px;">
            <div style="width: 50%; text-align: left;">No. Faktur: <br> <?= $general['invoice'] ?></div>
            <div style="width: 50%; text-align: right;">Tgl. Transaksi: <br><?= $general['tgl'] ?></div>
        </div>
        <table style="width: 100%; border-collapse: collapse; text-align: left; margin-top: 4px;" cellpadding="2">
            <thead>
                <tr>
                    <th>Nama Barang</th>
                    <th>Harga</th>
                    <th>Qty</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($detail as $key => $item) : ?>
                    <tr class="body">
                        <td><?= $item['nama_barang'] ?></td>
                        <td align="right"><?= number_format($item['subtotal'] / $item['qty_jual'], 0, '.', '.') ?></td>
                        <td align="center"><?= $item['qty_jual'] ?></td>
                        <td align="right"><?= number_format($item['subtotal'], 0, '.', '.') ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <table style="width: 100%; margin-top: 10px;">
            <tr>
                <th align="right">
                    Subtotal
                </th>
                <td>:</td>
                <td align="right">
                    Rp. <?= number_format($harga_total + $general['ppn'], 0, '.', '.') ?>
                </td>
            </tr>
            <tr>
                <th align="right">
                    Diskon
                </th>
                <td>:</td>
                <td align="right">
                    Rp. <?= number_format($penjualan->diskon, 0, '.', '.') ?>
                </td>
            </tr>
            <tr>
                <th align="right">
                    Total
                </th>
                <td>:</td>
                <td align="right">
                    Rp. <?= number_format($harga_total + $general['ppn'] - $penjualan->diskon, 0, '.', '.') ?>
                </td>
            </tr>
            <tr>
                <th align="right">
                    Jumlah Bayar
                </th>
                <td>:</td>
                <td align="right">
                    Rp. <?= number_format($penjualan->bayar, 0, '.', '.') ?>
                </td>
            </tr>
            <tr>
                <th align="right">
                    Kembalian
                </th>
                <td>:</td>
                <td align="right">
                    Rp. <?= number_format($penjualan->kembali, 0, '.', '.') ?>
                </td>
            </tr>
        </table>
        <!-- <br>
        <hr style="border-style: dotted">
        <div style="margin-top: 15px;">
            <div style="display: flex">
                <div style="width: 60%;" align="right">GRAND TOTAL:</div>
                <div style="width: 40%;" align="right">Rp. <?= number_format($harga_total, 0, '.', '.') ?></div>
            </div>
            <div style="margin-top: 7px; display: flex">
                <div style="width: 60%" align="right">PPN:</div>
                <div style="width: 40%" align="right">Rp. <?= number_format($general['ppn'], 0, '.', '.') ?></div>
            </div>
            <div style="margin-top: 7px; display: flex">
                <div style="width: 60%" align="right">DISKON:</div>
                <div style="width: 40%" align="right">Rp. <?= number_format($total['diskon'], 0, '.', '.') ?></div>
            </div>
            <div style="margin-top: 7px; display: flex">
                <div style="width: 60%" align="right">TOTAL:</div>
                <div style="width: 40%" align="right">Rp. <?= number_format($harga_total + $general['ppn'], 0, '.', '.') ?></div>
            </div>
            <div style="margin-top: 7px; display: flex">
                <div style="width: 60%" align="right">TUNAI:</div>
                <div style="width: 40%" align="right">Rp. <?= number_format($general['bayar'], 0, '.', '.') ?></div>
            </div>
            <div style="margin-top: 7px; display: flex">
                <div style="width: 60%" align="right">KEMBALI:</div>
                <div style="width: 40%" align="right">Rp. <?= number_format($general['kembali'], 0, '.', '.') ?></div>
            </div>
        </div> -->
        <br> <br>
        <div align="center">
            <strong>
                TERIMA KASIH SUDAH BERBELANJA
                <br>
            </strong>
            Barang yang sudah dibeli tidak boleh dikembalikan
            <p>
                <img src="<?php echo base_url('assets/img/profil/') . $toko->logo_toko ?>" width="40">
            </p>
        </div>
    </div>
</body>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        window.print();
        setTimeout(() => {
            window.location = '<?= base_url('penjualan') ?>'
        }, 3000);
    });
</script>

</html>