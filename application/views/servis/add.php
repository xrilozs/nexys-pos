<?php cek_user_ak() ?>
<div class="right_col" role="main">
    <div>
        <div class="page-title">
            <div class="title_left">
                <h3><?php echo $title ?></h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('servis/create') ?>">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" aria-describedby="nama" required>
                        </div>
                        <div class="form-group">
                            <label for="jenis_hp">Jenis HP</label>
                            <input type="text" class="form-control" id="jenis_hp" name="jenis_hp" aria-describedby="jenis_hp" required>
                        </div>
                        <div class="form-group">
                            <label for="imei">IMEI</label>
                            <input type="text" class="form-control" id="imei" name="imei" aria-describedby="imei" required>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi_masalah">Deskripsi Masalah</label>
                            <textarea class="form-control" id="deskripsi_masalah" name="deskripsi_masalah" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="harga">Harga</label>
                            <input type="number" class="form-control" id="harga" name="harga" aria-describedby="harga" required>
                        </div>
                        <div class="form-group">
                            <label for="tanggal_service">Tanggal Servis</label>
                            <input type="date" id="tanggal_service" class="form-control datepicker" name="tanggal_service" required />
                        </div>
                        <div class="form-group">
                            <label for="tanggal_diambil">Tanggal Diambil (Optional)</label>
                            <input type="date" id="tanggal_diambil" class="form-control datepicker" name="tanggal_diambil" />
                        </div>
                        <div class="form-group">
                            <label for="no_hp">No HP</label>
                            <input type="text" class="form-control" id="no_hp" name="no_hp" aria-describedby="no_hp" required>
                        </div>
                        <div class="form-group">
                            <label for="diambil_oleh">Diambil Oleh (Optional)</label>
                            <input type="text" class="form-control" id="diambil_oleh" name="diambil_oleh" aria-describedby="diambil_oleh">
                        </div>
                        <div class="form-group">
                            <label for="status_service">Status Servis</label>
                            <select id="status_service" name="status_service" class="form-control" required>
 								<option value="SERVICE">SERVICE</option>
                                <option value="SELESAI">SELESAI</option>
                                <option value="DIAMBIL">DIAMBIL</option>
 							</select>
                        </div>
                        <div class="form-group">
                            <label for="status_pembayaran">Status Pembayaran</label>
                            <select id="status_pembayaran" name="status_pembayaran" class="form-control" required>
 								<option value="BELUM LUNAS">BELUM LUNAS</option>
                                <option value="LUNAS">LUNAS</option>
 							</select>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>