<div id="detail-servis-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Detail Data</h4>
        </div>
        <div class="modal-body">
          <form id="detail-servis-form">
          <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" id="nama" name="nama" aria-describedby="nama" readonly>
          </div>
          <div class="form-group">
              <label for="jenis_hp">Jenis HP</label>
              <input type="text" class="form-control" id="jenis_hp" name="jenis_hp" aria-describedby="jenis_hp" readonly>
          </div>
          <div class="form-group">
              <label for="imei">IMEI</label>
              <input type="text" class="form-control" id="imei" name="imei" aria-describedby="imei" readonly>
          </div>
          <div class="form-group">
              <label for="deskripsi_masalah">Deskripsi Masalah</label>
              <textarea class="form-control" id="deskripsi_masalah" name="deskripsi_masalah" readonly></textarea>
          </div>
          <div class="form-group">
              <label for="harga">Harga</label>
              <input type="text" class="form-control" id="harga" name="harga" aria-describedby="harga" readonly>
          </div>
          <div class="form-group">
              <label for="tanggal_service">Tanggal Servis</label>
              <input type="text" id="tanggal_service" class="form-control datepicker" name="tanggal_service" readonly>
          </div>
          <div class="form-group">
              <label for="tanggal_diambil">Tanggal Diambil (Optional)</label>
              <input type="text" id="tanggal_diambil" class="form-control datepicker" name="tanggal_diambil" readonly>
          </div>
          <div class="form-group">
              <label for="no_hp">No HP</label>
              <input type="text" class="form-control" id="no_hp" name="no_hp" aria-describedby="no_hp" readonly>
          </div>
          <div class="form-group">
              <label for="diambil_oleh">Diambil Oleh (Optional)</label>
              <input type="text" class="form-control" id="diambil_oleh" name="diambil_oleh" aria-describedby="diambil_oleh" readonly>
          </div>
          <div class="form-group">
              <label for="status_service">Status Servis</label>
              <input type="text" class="form-control" id="status_service" name="status_service" aria-describedby="status_service" readonly>
          </div>
          <div class="form-group">
              <label for="status_pembayaran">Status Pembayaran</label>
              <input type="text" class="form-control" id="status_pembayaran" name="status_pembayaran" aria-describedby="status_pembayaran" readonly>
          </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>