<?php cek_user_ak() ?>
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3><?php echo $title ?></h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row" style="margin-bottom:20px;">
            <div class="col-md-12">
                <form class="form-inline" id="search-form">
                    <div class="form-group" style="margin-right:10px;">
                        <label for="no_hp_search">No. HP</label>
                        <input type="text" class="form-control" id="no_hp_search" name="no_hp_search" aria-describedby="no_hp_search">
                    </div>
                    <div class="form-group">
                        <label for="kode_search">No. Invoice</label>
                        <input type="text" class="form-control" id="kode_search" name="kode_search" aria-describedby="kode_search">
                    </div>
                    <button type="submit" class="btn btn-primary" style="margin:0px;">
                        <i class="fa fa-search"></i> Cari
                    </button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <!-- <button type="button" class="btn btn-sm btn-primary" onclick="tambahServis()" title="Tambah Data" id="tambahServis"><i class="fa fa-plus"></i> Tambah Data</button> -->
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php echo $this->session->flashdata('message'); ?>
                        <table width="100%" class="table table-striped table-bordered" id="servis-table">
                            <thead>
                                <tr>
                                    <th>No. Invoice</th>
                                    <th>Nama</th>
                                    <th>IMEI</th>
                                    <th>Harga</th>
                                    <th>No. HP</th>
                                    <th>Tanggal Servis</th>
                                    <th>Status Servis</th>
                                    <th>Status Pembayaran</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'script.php' ?>
<?php include 'edit.php' ?>
<?php include 'detail.php' ?>