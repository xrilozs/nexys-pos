<script src="<?php echo base_url('assets/') ?>vendors/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>DataTables/datatables.min.js"></script>

<script>
    let no_hp_search = ""
    let kode_search = ""

    let servis_table = $('#servis-table').DataTable( {
        processing: true,
        serverSide: true,
        searching: false,
        ajax: {
          async: true,
          url: 'servis/pagination',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            // console.log("D itu:", d)
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            // let status = $('#status_spl').val()
            // let date = $('#filter_tanggal').val()
            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            newObj.draw = d.draw
            newObj.kode_search = kode_search
            newObj.no_hp_search = no_hp_search
            // newObj.sort = d.order[0].dir
            // if(status != 'ALL') newObj.status = status 
            switch (column) {
                case 0:
                    newObj.order_by = 'kode'
                    break
                case 1:
                    newObj.order_by = 'nama'
                    break
                case 2:
                    newObj.order_by = 'imei'
                    break
                case 3:
                    newObj.order_by = 'harga'
                    break
                case 4:
                    newObj.order_by = 'no_hp'
                    break
                case 5:
                    newObj.order_by = 'tanggal_service'
                    break
                case 6:
                    newObj.order_by = 'status_service'
                    break
                case 7:
                    newObj.order_by = 'status_pembayaran'
                    break
                default:
                    newObj.order_by = 'kode'
                    break
            }

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "kode",
                orderable: true
            },
            { 
                data: "nama",
                orderable: true
            },
            { 
                data: "imei",
                orderable: true
            },
            { 
                data: "harga",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data, true)
                }
            },
            { 
                data: "no_hp",
                orderable: true
            },
            { 
                data: "tanggal_service",
                orderable: true
            },
            { 
                data: "status_service",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'SELESAI'){
                        label = `<span class="label label-success">${data}</span>`
                    }else if(data == 'DIAMBIL'){
                        label = `<span class="label label-primary">${data}</span>`
                    }else{
                        label = `<span class="label label-default">${data}</span>`
                    }
                    return label
                }
            },
            { 
                data: "status_pembayaran",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'LUNAS'){
                        label = `<span class="label label-success">${data}</span>`
                    }else{
                        label = `<span class="label label-default">${data}</span>`
                    }
                    return label
                }
            },
            {
                data: "id_servis",
                className: "dt-body-left",
                render: function (data, type, row, meta) {
                    let button = `
                    <button onclick="detailServis(${data})" title="Detail Data" class="btn btn-default btn-xs" data-toggle="modal" data-target="#detail-servis-modal"><i class="fa fa-search"></i></button>
                    <button onclick="editServis(${data})" title="Edit Data" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#edit-servis-modal"><i class="fa fa-edit"></i></a>`
                    
                    return button
                },
                orderable: false
            }
        ]
    });

    $('#search-form').submit(function(e){
        e.preventDefault()
        no_hp_search = $('#no_hp_search').val()
        kode_search = $('#kode_search').val()
        servis_table.draw()
    })

    function detailServis(id){
        console.log("DATA ", id)
        $.ajax({
            url: base_url + "servis/detail/" + id,
            type: "get",
            success: function(data) {
                const obj = JSON.parse(data);
                renderForm("detail", obj)
            }
        })
    }

    function editServis(e) {
        $.ajax({
            url: base_url + "servis/detail/" + e,
            type: "get",
            success: function(data) {
                const obj = JSON.parse(data);
                renderForm("edit", obj)
            }
        })
    }

    function hapusServis(e) {
        $.ajax({
            url: base_url + "servis/cek_delete/" + e,
            type: "post",
            success: function(data) {
                var obj = JSON.parse(data);
                if (obj.num == 1) {
                    Swal.fire({
                        title: "Cannot Delete This Data!",
                        text: "Please check your data relation!",
                        icon: "error",
                    });
                } else {
                    Swal.fire({
                        title: "Are you sure ?",
                        text: "Deleted data can not be restored!",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Yes, delete it!"
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                url: base_url + "servis/hapus/" + e,
                                type: "post",
                                success: function(data) {
                                    window.location = base_url + "servis"
                                }
                            })
                        }
                    })
                }
            }
        });
    }

    function formatRupiah(angka, prefix){
        let number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    function renderForm(action, obj){
        let form = $(`#${action}-servis-form`)
        form.find( "input[name='kode']" ).val(obj.kode)
        form.find( "input[name='nama']" ).val(obj.nama)
        form.find( "input[name='jenis_hp']" ).val(obj.jenis_hp)
        form.find( "input[name='imei']" ).val(obj.imei)
        form.find( "textarea[name='deskripsi_masalah']" ).val(obj.deskripsi_masalah)
        form.find( "input[name='harga']" ).val(obj.harga)
        form.find( "input[name='tanggal_service']" ).val(obj.tanggal_service)
        form.find( "input[name='tanggal_diambil']" ).val(obj.tanggal_diambil)
        form.find( "input[name='no_hp']" ).val(obj.no_hp)
        form.find( "input[name='diambil_oleh']" ).val(obj.diambil_oleh)
        if(action == 'edit'){
            form.find( "input[name='id_servis']" ).val(obj.id_servis)
            $('#status_service').val(obj.status_service).change()
            $('#status_pembayaran').val(obj.status_pembayaran).change()
        }else{
            form.find( "input[name='status_service']" ).val(obj.status_service)
            form.find( "input[name='status_pembayaran']" ).val(obj.status_pembayaran)
        }
    }
</script>