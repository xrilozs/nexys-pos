        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fa fa-shopping-cart"></i> <span>PRIMA MART</span></a>
            </div>
            <div class="clearfix"></div>
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url('assets/production/') ?>images/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $user['nama_lengkap'] ?></h2>
              </div>
            </div>
            <br />
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <?php if ($user['tipe'] == "Administrator") { ?>
                    <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('dashboard') ?>">Dashboard</a></li>
                      </ul>
                    </li>
                  <?php } ?>
                  <?php if($user['tipe'] == "Administrator" || $user['tipe'] == "Kasir"){?>
                  <li><a><i class="fa fa-shopping-cart"></i> Transaksi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <?php if($user['tipe'] == "Kasir"){?>
                      <li><a href="<?php echo base_url('penjualan') ?>">Entry Penjualan</a></li>
                      <li><a href="<?php echo base_url('servis/add') ?>">Entry Servis</a></li>
                      <?php }?>
                      <li><a href="<?php echo base_url('dpenjualan') ?>">Daftar Penjualan</a></li>
                      <li><a href="<?php echo base_url('servis') ?>">Data Servis</a></li>
                      <?php if($user['tipe'] == "Administrator"){?>
                      <li><a href="<?php echo base_url('pembelian') ?>">Entry Pembelian</a></li>
                      <li><a href="<?php echo base_url('dpembelian') ?>">Daftar Pembelian</a></li>
                      <li><a href="<?php echo base_url('hutang') ?>">Hutang</a></li>
                      <li><a href="<?php echo base_url('piutang') ?>">Piutang</a></li>
                      <?php }?>
                    </ul>
                  </li>
                  <?php }?>
                  <?php if ($user['tipe'] == "Administrator") { ?>
                    <li><a><i class="fa fa-desktop"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a>Data Barang<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="<?php echo base_url('barang') ?>">Data Item</a>
                            </li>
                            <li><a href="<?php echo base_url('kategori') ?>">Kategori</a>
                            </li>
                            <li><a href="<?php echo base_url('satuan') ?>">Satuan</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="<?php echo base_url('supplier') ?>">Data Supplier</a></li>
                        <li><a href="<?php echo base_url('customer') ?>">Data Customer</a></li>
                        <li><a href="<?php echo base_url('karyawan') ?>">Data Karyawan</a></li>
                        <!-- <li><a href="<?php echo base_url('servis') ?>">Data Servis</a></li> -->
                        <li><a href="<?php echo base_url('barang/member') ?>">Data Barang per Member</a></li>
                        <!-- <li><a href="<?php //echo base_url('mutasi')
                                          ?>">Mutasi Barang</a></li> -->
                        <li><a href="<?php echo base_url('stokopname') ?>">Stok Opname</a></li>
                        <li><a href="<?php echo base_url('stok') ?>">Stok In/Out</a></li>
                      </ul>
                    </li>
                    <li><a><i class="fa fa-money"></i> Keuangan <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('kas') ?>">Kas</a></li>
                        <li><a href="<?php echo base_url('ppn') ?>">PPN</a></li>
                        <li><a href="<?php echo base_url('bank') ?>">Bank</a></li>
                      </ul>
                    </li>

                    <li><a><i class="fa fa-file-text-o"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('laporan/barang') ?>">Laporan Barang</a></li>
                        <li><a href="<?php echo base_url('laporan/penjualan') ?>">Laporan Penjualan</a></li>
                        <li><a href="<?php echo base_url('laporan/pembelian') ?>">Laporan Pembelian</a></li>
                        <!-- <li><a href="<?php //echo base_url('laporan/mutasi')
                                          ?>">Laporan Mutasi Barang</a></li> -->
                        <li><a href="<?php echo base_url('laporan/stokopname') ?>">Laporan Stok Opname</a></li>
                        <li><a href="<?php echo base_url('laporan/laba_rugi') ?>">Laporan Laba Rugi</a></li>
                        <li><a href="<?php echo base_url('laporan/kas') ?>">Laporan Kas</a></li>
                        <li><a href="<?php echo base_url('laporan/kas_bank') ?>">Laporan Kas Bank</a></li>
                        <li><a href="<?php echo base_url('laporan/stok') ?>">Laporan Stok In/Out</a></li>
                        <li><a href="<?php echo base_url('laporan/hutang') ?>">Laporan Hutang</a></li>
                        <li><a href="<?php echo base_url('laporan/piutang') ?>">Laporan Piutang</a></li>
                      </ul>
                    </li>
                    <li><a><i class="fa fa-user"></i> Management User <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('userlog') ?>">User Log</a></li>
                      </ul>
                    </li>

                    <li><a><i class="fa fa-bar-chart-o"></i> Grafik <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('grafik') ?>">Grafik</a></li>
                      </ul>
                    </li>

                    <li><a><i class="fa fa-magic"></i> Tools <span class="fa fa-chevron-down"></span></a>

                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('barcode') ?>">Generate Barcode</a></li>
                        <li><a href="<?php echo base_url('backup') ?>">Backup Data</a></li>
                        <li><a href="<?php echo base_url('applog') ?>">Application Log</a></li>
                      </ul>
                    </li>

                    <li><a><i class="fa fa-gift"></i> Prestasi <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('prestasi') ?>">Index Prestasi</a></li>
                      </ul>
                    </li>

                    <li><a><i class="fa fa-gears"></i> Setting <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('profil') ?>">Profil Toko</a></li>
                        <!-- <li><a href="<?php //echo base_url('promo')
                                          ?>">Setting Promo</a></li> -->
                      </ul>
                    </li>
                  <?php } ?>
                  <?php if ($user['tipe'] == "Gudang") {?>
                    <li><a><i class="fa fa-desktop"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('barang') ?>">Data Barang<span class="fa"></span></a></li>
                        
                      </ul>
                    </li>

                    <li><a><i class="fa fa-money"></i> Histori  <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('rbarang/gudang') ?>">Histori Request</a></li>
                        <li><a href="<?php echo base_url('rbarang/historystok') ?>">Histori Update Stok</a></li>
                      </ul>
                    </li>
                  <?php }?>
                  <?php if ($user['tipe'] == "OwnerUtama"){ ?>
                    <li><a><i class="fa fa-desktop"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a>Data Barang<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="<?php echo base_url('barang') ?>">Data Item</a>
                            </li>
                            <li><a href="<?php echo base_url('kategori') ?>">Kategori</a>
                            </li>
                            <li><a href="<?php echo base_url('satuan') ?>">Satuan</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                  <?php }?>
                  <?php if ($user['tipe'] == "OwnerCabang"){ ?>
                    <li><a><i class="fa fa-desktop"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a>Data Barang<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="<?php echo base_url('rbarang') ?>">Data Request</a>
                            <li class="sub_menu"><a href="<?php echo base_url('rbarang/ready') ?>">Data Barang</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </li>

                    <li><a><i class="fa fa-user"></i> Management Sales <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('user/salescabang') ?>">Data Sales</a></li>
                      </ul>
                    </li>

                    <li><a><i class="fa fa-money"></i> Histori  <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('rbarang/reqharga') ?>">Histori Request Harga</a></li>
                      </ul>
                    </li>
                  <?php }?>
                  <?php if ($user['tipe'] == "Sales"){ ?>
                    <li><a><i class="fa fa-desktop"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url('customer') ?>">Data Customer</a></li>
                        <li><a href="<?php echo base_url('barang') ?>">Data Barang</a></li>
                        <!-- <li><a>Data Barang<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="<?php echo base_url('sales/index') ?>">Data Request</a>
                            <li class="sub_menu"><a href="<?php echo base_url('sales/ready') ?>">Data Barang</a>
                            </li>
                          </ul>
                        </li> -->
                      </ul>
                    </li>

                  <?php }?>
                </ul>
              </div>
            </div>
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip">
                -
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen" onclick="document.body.requestFullscreen()">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?= base_url('auth/logout') ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip">
                -
              </a>
            </div>
          </div>
        </div>