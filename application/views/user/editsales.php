<?php cek_user_oc() ?>
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3><?php echo $title ?></h3>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">        
                                <form class="form-horizontal" method="post" action="<?php echo base_url('user/updatesales')?>">
                                    <div class="form-group">
                                        <input type="hidden" class="form-control has-feedback-left" id="iduser" name="iduser" value="<?php echo $item['id_user']?>">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" id="editusername" name="editusername" value="<?php echo $item['username']?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" id="editnama" name="editnama" value="<?php echo $item['nama_lengkap']?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">No. Telp</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" id="edittelp" value="<?php echo $item['telp_user']?>" name="edittelp"autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" id="editemail" value="<?php echo $item['email_user']?>" name="editemail"autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input col="7" rows="2" class="form-control" name="editalamat" value="<?php echo $item['alamat_user']?>" id="editalamat"></input>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>