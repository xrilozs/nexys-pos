<?php cek_user_ag() ?>
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3><?php echo $title ?></h3>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<?php echo $this->session->flashdata('message'); ?>
								<table width="100%" class="table table-striped table-bordered datatable">
									<thead>
										<tr>
                                            <th>Username</th>
                                            <th>Tipe User</th>
                                            <th>Nama Lengkap</th>
                                            <th>Alamat</th>
                                            <th>Telp</th>
                                            <th>Email</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($item as $d) { ?>
											<tr>
												<td><?php echo $d['username'] ?></td>
												<td><?php echo $d['tipe'] ?></td>
												<td><?php echo $d['nama_lengkap'] ?></td>
												<td><?php echo $d['alamat_user'] ?></td>
												<td><?php echo $d['telp_user'] ?></td>
												<td><?php echo $d['email_user'] ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>