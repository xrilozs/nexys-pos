<?php cek_user_oc() ?>
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3><?php echo $title ?></h3>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
                                <?php include 'inputsales.php' ?>
                                <button type="button" class="btn btn-sm btn-primary" onclick="tambahsales()" title="Tambah Data" id="tambahsales"><i class="fa fa-plus"></i> Tambah Sales</button>
								<ul class="nav navbar-right panel_toolbox">
									<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
									</li>
									<li><a class="close-link"><i class="fa fa-close"></i></a>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<?php echo $this->session->flashdata('message'); ?>
								<table width="100%" class="table table-striped table-bordered datatable">
									<thead>
										<tr>
											<th>Username</th>
											<th>Nama Lengkap</th>
                                            <th>Alamat</th>
                                            <th>Telepon</th>
                                            <th>Email</th>
                                            <th>Opsi</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($item as $i) { ?>
											<tr>
												<td><?php echo $i['username'] ?></td>
												<td><?php echo $i['nama_lengkap'] ?></td>
                                                <td><?php echo $i['alamat_user']?></td>
                                                <td><?php echo $i['telp_user']?></td>
                                                <td><?php echo $i['email_user']?></td>
                                                <td>
                                                    <a href="<?php echo base_url('user/editsales/'). encrypt_url($i['id_user'])?>" class="btn btn-primary btn-xs"> <i class="fa fa-edit"></i></a>
                                                </td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        <?php include 'script.php' ?>