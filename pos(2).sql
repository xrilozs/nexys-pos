-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2022 at 07:59 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id_bank` bigint(20) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `keterangan` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id_bank`, `id_user`, `tanggal`, `jenis`, `nominal`, `keterangan`) VALUES
(1, 1, '2021-03-04 13:10:17', 'Pemasukan', 90000, 'Mutasi Dari Kas'),
(2, 1, '2021-06-29 08:45:25', 'Pemasukan', 0, 'Mutasi Dari Kas');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `kode_barang` varchar(20) DEFAULT NULL,
  `barcode` varchar(20) DEFAULT NULL,
  `nama_barang` varchar(50) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_satuan` int(11) DEFAULT NULL,
  `id_supplier` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT 1,
  `harga_beli` varchar(30) DEFAULT NULL,
  `range_harga` varchar(30) DEFAULT NULL,
  `harga_jual` varchar(30) DEFAULT NULL,
  `harga_pelanggan` int(11) DEFAULT NULL,
  `harga_sales` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `kode_barang`, `barcode`, `nama_barang`, `id_kategori`, `id_satuan`, `id_supplier`, `id_user`, `harga_beli`, `range_harga`, `harga_jual`, `harga_pelanggan`, `harga_sales`, `stok`, `is_active`) VALUES
(69, 'BRG-00001', '8998866201957', 'SABRINA MINYAK GORENG 1L', 15, 19, 39, 1, '20000', '2000', '22000', 0, 0, 1000, 1),
(70, 'BRG-00002', '8993365170032', 'MADU TJ 15ML', 16, 21, 40, 1, '9000', '1000', '10000', 0, 0, 1000, 1),
(71, 'BRG-00003', '8998866608305', 'SABUN GIV 80GR', 18, 19, 42, 1, '2200', '200', '2400', 0, 0, 1000, 1),
(72, 'BRG-00004', '8999999706081', 'PASTA GIGI PEPSODENT 75GR', 19, 19, 42, 1, '4500', '500', '5000', 0, 0, 1000, 1),
(73, 'BRG-00005', '8997021870540', 'HOT IN CREAM 60GR', 17, 20, 40, 1, '13000', '1000', '14000', 0, 0, 1000, 1),
(74, 'BRG-00006', '8998866200301', 'MIE SEDAP GORENG 85GR', 15, 19, 38, 1, '2500', '100', '2600', 0, 0, 1000, 1),
(75, 'BRG-00007', '8998866604963', 'SABUN DAIA FRESH LEMON 265GR', 18, 19, 42, 1, '4500', '500', '5000', 0, 0, 1000, 1),
(76, 'BRG-00008', '20000924', 'AY CLEAN SABUN CUCI PIRING 500ML', 18, 20, 41, 1, '9000', '1000', '10000', 0, 0, 1000, 1),
(77, 'BRG-00009', '20000924', 'SOFTENER PELEMBUT PAKAIAN 500ML', 18, 20, 41, 1, '9000', '1000', '10000', 0, 0, 1000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `barang_per_member`
--

CREATE TABLE `barang_per_member` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `harga_diskon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Stand-in structure for view `coba`
-- (See below for the actual view)
--
CREATE TABLE `coba` (
`id_barang` int(11)
,`nama_barang` varchar(50)
,`barcode` varchar(20)
,`harga_pelanggan` int(11)
,`harga_sales` int(11)
,`satuan` varchar(50)
,`kategori` varchar(50)
,`harga_beli` varchar(30)
,`harga_jual` varchar(30)
,`stok` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_cs` int(11) NOT NULL,
  `kode_cs` varchar(20) DEFAULT NULL,
  `nama_cs` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `telp` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `jenis_cs` varchar(50) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 = nonaktif, 1 = aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_cs`, `kode_cs`, `nama_cs`, `jenis_kelamin`, `telp`, `email`, `alamat`, `jenis_cs`, `status`) VALUES
(1, 'CS-000001', 'UMUM', 'Laki-Laki', '98765678', 'Umum@gmail.com', 'hiihihi', 'Umum', '1');

-- --------------------------------------------------------

--
-- Table structure for table `customer_request_barang`
--

CREATE TABLE `customer_request_barang` (
  `id` int(11) NOT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '0' COMMENT '0 = tidak disetujui, 1 = disetujui'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `detil_hutang`
--

CREATE TABLE `detil_hutang` (
  `id_detil_hutang` bigint(20) NOT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_hutang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `detil_pembelian`
--

CREATE TABLE `detil_pembelian` (
  `id_detil_beli` bigint(20) NOT NULL,
  `id_beli` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `kode_detil_beli` varchar(20) DEFAULT NULL,
  `hrg_beli` int(11) DEFAULT NULL,
  `hrg_jual` int(11) DEFAULT NULL,
  `qty_beli` int(11) DEFAULT NULL,
  `subtotal` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `detil_penjualan`
--

CREATE TABLE `detil_penjualan` (
  `id_detil_jual` bigint(20) NOT NULL,
  `id_jual` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_servis` int(11) DEFAULT NULL,
  `id_karyawan` int(11) DEFAULT NULL,
  `kode_detil_jual` varchar(20) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `harga_item` int(11) DEFAULT NULL,
  `qty_jual` int(11) DEFAULT NULL,
  `subtotal` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `detil_penjualan`
--

INSERT INTO `detil_penjualan` (`id_detil_jual`, `id_jual`, `id_barang`, `id_servis`, `id_karyawan`, `kode_detil_jual`, `diskon`, `harga_item`, `qty_jual`, `subtotal`) VALUES
(1, 317, 5, NULL, NULL, 'DJ-0000001', 0, 123123123, 1, '123123123'),
(3, 318, 8, NULL, NULL, 'DJ-0000002', 0, 80000, 6, '480000'),
(4, 319, 9, NULL, NULL, 'DJ-0000008', 0, 6000, 7, '6000'),
(5, 319, 10, NULL, NULL, 'DJ-0000010', 0, 60000, 2, '60000'),
(6, 320, 9, NULL, NULL, 'DJ-0000012', 0, 6000, 50, '300000'),
(7, 321, 9, NULL, NULL, 'DJ-0000013', 0, 6000, 50, '300000'),
(8, 322, 12, NULL, NULL, 'DJ-0000014', 0, 8000, 30, '240000'),
(9, 322, 11, NULL, NULL, 'DJ-0000015', 0, 15000, 5, '75000'),
(10, 323, 14, NULL, NULL, 'DJ-0000016', 0, 12000, 12, '144000'),
(11, 324, 17, NULL, NULL, 'DJ-0000017', 0, 7000, 1, '7000'),
(12, 324, 16, NULL, NULL, 'DJ-0000018', 0, 15, 1, '15'),
(13, 324, 15, NULL, NULL, 'DJ-0000019', 0, 11, 1, '11'),
(14, 324, 18, NULL, NULL, 'DJ-0000020', 0, 12000, 40, '480000'),
(15, 324, 19, NULL, NULL, 'DJ-0000021', 0, 23000, 1, '23000'),
(16, 325, 18, NULL, NULL, 'DJ-0000022', 0, 12000, 1, '12000'),
(17, 325, 19, NULL, NULL, 'DJ-0000023', 0, 23000, 1, '23000'),
(18, 326, 20, NULL, NULL, 'DJ-0000024', 0, 6000, 3, '18000'),
(19, 327, 21, NULL, NULL, 'DJ-0000025', 0, 15000, 5, '75000'),
(20, 328, 21, NULL, NULL, 'DJ-0000026', 0, 15000, 20, '300000'),
(21, 329, 21, NULL, NULL, 'DJ-0000027', 0, 15000, 30, '450000'),
(22, 330, 22, NULL, NULL, 'DJ-0000028', 0, 8000, 20, '160000'),
(23, 331, 23, NULL, NULL, 'DJ-0000029', 0, 6000, 44, '264000'),
(24, 332, 24, NULL, NULL, 'DJ-0000030', 0, 6000, 50, '300000'),
(25, 332, 25, NULL, NULL, 'DJ-0000031', 0, 8000, 12, '96000'),
(26, 333, 26, NULL, NULL, 'DJ-0000032', 0, 6000, 1, '6000'),
(27, 333, 27, NULL, NULL, 'DJ-0000033', 0, 8000, 1, '8000'),
(28, 334, 27, NULL, NULL, 'DJ-0000034', 0, 8000, 15, '120000'),
(29, 334, 26, NULL, NULL, 'DJ-0000035', 0, 6000, 18, '108000'),
(30, 335, 26, NULL, NULL, 'DJ-0000036', 0, 6000, 90, '540000'),
(31, 335, 27, NULL, NULL, 'DJ-0000037', 0, 8000, 1, '8000'),
(32, 336, 26, NULL, NULL, 'DJ-0000038', 0, 6000, 60, '360000'),
(33, 336, 27, NULL, NULL, 'DJ-0000039', 0, 8000, 50, '400000'),
(34, 337, 28, NULL, NULL, 'DJ-0000042', 0, 7000, 5, '7000'),
(35, 337, 29, NULL, NULL, 'DJ-0000043', 0, 5000, 100, '500000'),
(37, 338, 28, NULL, NULL, 'DJ-0000044', 0, 7000, 1, '7000'),
(39, 339, 31, NULL, NULL, 'DJ-0000046', 0, 10000, 6, '50000'),
(40, 339, 32, NULL, NULL, 'DJ-0000047', 0, 7000, 1, '7000'),
(41, 340, 31, NULL, NULL, 'DJ-0000048', 0, 10000, 8, '80000'),
(42, 340, 32, NULL, NULL, 'DJ-0000049', 0, 7000, 1, '7000'),
(43, 341, 33, NULL, NULL, 'DJ-0000051', 0, 10000, 2, '10000'),
(44, 341, 34, NULL, NULL, 'DJ-0000054', 0, 6000, 3, '6000'),
(45, 342, 35, NULL, NULL, 'DJ-0000056', 0, 5000, 2, '5000'),
(46, 342, 36, NULL, NULL, 'DJ-0000058', 0, 7000, 2, '7000'),
(47, 343, 37, NULL, NULL, 'DJ-0000059', 0, 7000, 5, '35000'),
(48, 343, 38, NULL, NULL, 'DJ-0000060', 0, 5000, 5, '25000'),
(49, 344, 39, NULL, NULL, 'DJ-0000061', 0, 6000, 10, '60000'),
(50, 344, 40, NULL, NULL, 'DJ-0000062', 0, 5000, 10, '50000'),
(51, 344, 41, NULL, NULL, 'DJ-0000063', 0, 7000, 10, '70000'),
(52, 345, 42, NULL, NULL, 'DJ-0000065', 0, 7000, 2, '7000'),
(53, 345, 43, NULL, NULL, 'DJ-0000066', 0, 10000, 1, '10000'),
(54, 346, 45, NULL, NULL, 'DJ-0000067', 0, 7000, 30, '210000'),
(55, 346, 44, NULL, NULL, 'DJ-0000068', 0, 7000, 10, '70000'),
(56, 347, 46, NULL, NULL, 'DJ-0000070', 0, 5000, 10, '50000'),
(57, 347, 47, NULL, NULL, 'DJ-0000071', 0, 6000, 1, '6000'),
(58, 348, 46, NULL, NULL, 'DJ-0000072', 0, 5000, 5, '25000'),
(59, 348, 47, NULL, NULL, 'DJ-0000073', 0, 6000, 2, '12000'),
(60, 349, 48, NULL, NULL, 'DJ-0000077', 0, 10000, 4, '10000'),
(61, 349, 49, NULL, NULL, 'DJ-0000080', 0, 7000, 3, '7000'),
(62, 350, 49, NULL, NULL, 'DJ-0000081', 0, 7000, 1, '7000'),
(63, 350, 50, NULL, NULL, 'DJ-0000082', 0, 7000, 1, '7000'),
(64, 350, 51, NULL, NULL, 'DJ-0000084', 0, 6000, 2, '6000'),
(65, 351, 53, NULL, NULL, 'DJ-0000085', 0, 10000, 1, '10000'),
(66, 351, 52, NULL, NULL, 'DJ-0000087', 0, 7000, 2, '7000'),
(67, 352, 55, NULL, NULL, 'DJ-0000090', 0, 9000, 3, '9000'),
(68, 352, 54, NULL, NULL, 'DJ-0000095', 0, 13000, 5, '13000'),
(69, 353, 56, NULL, NULL, 'DJ-0000096', 0, 10000, 5, '50000'),
(70, 353, 58, NULL, NULL, 'DJ-0000097', 0, 10000, 15, '150000'),
(72, 354, 60, NULL, NULL, 'DJ-0000098', 0, 7000, 5, '35000'),
(73, 354, 59, NULL, NULL, 'DJ-0000099', 0, 8000, 12, '96000'),
(74, 355, 61, NULL, NULL, 'DJ-0000100', 0, 10000, 5, '50000'),
(75, 355, 62, NULL, NULL, 'DJ-0000101', 0, 7000, 5, '35000'),
(76, 356, 61, NULL, NULL, 'DJ-0000102', 0, 10000, 10, '100000'),
(77, 357, 64, NULL, NULL, 'DJ-0000107', 0, 21000, 5, '21000'),
(78, 358, 67, NULL, NULL, 'DJ-0000113', 0, 8000, 6, '8000'),
(79, 358, 68, NULL, NULL, 'DJ-0000114', 0, 2000, 10, '20000'),
(80, 358, 65, NULL, NULL, 'DJ-0000115', 0, 50000, 3, '150000');

-- --------------------------------------------------------

--
-- Table structure for table `detil_piutang`
--

CREATE TABLE `detil_piutang` (
  `id_detil_piutang` bigint(20) NOT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_piutang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `hargabarang_request`
--

CREATE TABLE `hargabarang_request` (
  `id_hbr` int(11) NOT NULL,
  `id_request` int(11) DEFAULT NULL,
  `harga_request` int(11) DEFAULT NULL,
  `status` varchar(55) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_cabangrequest` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `hargabarang_request`
--

INSERT INTO `hargabarang_request` (`id_hbr`, `id_request`, `harga_request`, `status`, `id_user`, `id_cabangrequest`) VALUES
(1, 21, 15000, 'Dibatalkan', 14, 13),
(2, 22, 12000, 'Disetujui', 14, 13),
(3, 22, 12000, 'Disetujui', 14, 13);

-- --------------------------------------------------------

--
-- Table structure for table `historyupdatestok`
--

CREATE TABLE `historyupdatestok` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `stok_awal` int(11) NOT NULL,
  `stok_akhir` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `historyupdatestok`
--

INSERT INTO `historyupdatestok` (`id`, `id_user`, `id_barang`, `stok_awal`, `stok_akhir`, `created_at`) VALUES
(14, 15, 163, 0, 10, '2022-05-07 05:29:42'),
(15, 15, 164, 0, 8, '2022-05-07 05:33:40'),
(16, 15, 165, 0, 9, '2022-05-07 05:33:43'),
(17, 15, 163, 5, 6, '2022-05-07 21:48:13');

-- --------------------------------------------------------

--
-- Table structure for table `hutang`
--

CREATE TABLE `hutang` (
  `id_hutang` bigint(20) NOT NULL,
  `id_beli` int(11) DEFAULT NULL,
  `tgl_hutang` datetime DEFAULT NULL,
  `jml_hutang` int(11) DEFAULT NULL,
  `bayar` int(11) DEFAULT NULL,
  `sisa` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `jatuh_tempo` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `kode_karyawan` varchar(20) DEFAULT NULL,
  `nama_karyawan` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `telp_karyawan` varchar(15) DEFAULT NULL,
  `email_karyawan` varchar(50) DEFAULT NULL,
  `status_karyawan` varchar(20) DEFAULT NULL,
  `tmpt_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(20) DEFAULT NULL,
  `tgl_masuk` varchar(20) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `kode_karyawan`, `nama_karyawan`, `jenis_kelamin`, `telp_karyawan`, `email_karyawan`, `status_karyawan`, `tmpt_lahir`, `tgl_lahir`, `tgl_masuk`, `alamat`) VALUES
(3, 'K-00002', 'Ciko Ciki Tita', 'Laki-Laki', '082236578566', 'cikocik@gmail.com', 'Tetap', 'Banyuwangi', '04 Oktober 1998', '11/10/2019', 'Songgon'),
(16, 'K-00003', 'Rio Febrianto', 'Laki-Laki', '081092570948', 'riofebrianto@gmail.com', 'Tetap', 'Banyuwangi', '03 Maret 1998', '18/01/2020', 'Banyuwangi'),
(25, 'K-00004', 'Lina Fitriyani', 'Perempuan', '087898728987', 'lina@gmail.com', 'Tetap', 'Banyuwangi', '3 Desember 1991', '10/07/2020', 'Jl. Kepiting'),
(26, 'K-00005', 'Dinda Nur Azahra', 'Perempuan', '087893338990', 'dinda@gmail.com', 'Tetap', 'Malang', '04 Maret 1981', '10/07/2020', 'Jl. Udang');

-- --------------------------------------------------------

--
-- Table structure for table `kas`
--

CREATE TABLE `kas` (
  `id_kas` bigint(20) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `kode_kas` varchar(20) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jenis` varchar(20) DEFAULT NULL,
  `nominal` varchar(50) DEFAULT NULL,
  `keterangan` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `kas`
--

INSERT INTO `kas` (`id_kas`, `id_user`, `kode_kas`, `tanggal`, `jenis`, `nominal`, `keterangan`) VALUES
(1, 3, 'KS-0000001', '2022-05-10 09:27:38', 'Pemasukan', '80000', 'Penjualan'),
(2, 3, 'KS-0000002', '2022-05-10 09:29:52', 'Pemasukan', '100000', 'Penjualan'),
(3, 3, 'KS-0000003', '2022-05-10 09:30:29', 'Pemasukan', '20000', 'Penjualan'),
(4, 3, 'KS-0000004', '2022-05-10 09:33:14', 'Pemasukan', '32500', 'Penjualan'),
(5, 3, 'KS-0000005', '2022-05-10 09:40:15', 'Pemasukan', '13000', 'Penjualan'),
(6, 3, 'KS-0000006', '2022-05-10 09:40:42', 'Pemasukan', '31500', 'Penjualan'),
(7, 3, 'KS-0000007', '2022-05-10 09:41:37', 'Pemasukan', '60000', 'Penjualan'),
(8, 3, 'KS-0000008', '2022-05-10 11:43:54', 'Pemasukan', '447500', 'Penjualan'),
(9, 3, 'KS-0000009', '2022-05-12 08:36:58', 'Pemasukan', '584000', 'Penjualan'),
(10, 3, 'KS-0000010', '2022-05-12 08:51:22', 'Pemasukan', '300000', 'Penjualan'),
(11, 3, 'KS-0000011', '2022-05-12 08:54:39', 'Pemasukan', '30000', 'Penjualan'),
(12, 3, 'KS-0000012', '2022-05-12 08:56:03', 'Pemasukan', '33000', 'Penjualan'),
(13, 3, 'KS-0000013', '2022-05-12 08:58:08', 'Pemasukan', '1392000', 'Penjualan'),
(14, 3, 'KS-0000014', '2022-05-12 09:02:07', 'Pemasukan', '140000', 'Penjualan'),
(15, 3, 'KS-0000015', '2022-05-12 09:03:00', 'Pemasukan', '100000', 'Penjualan'),
(16, 3, 'KS-0000016', '2022-05-12 09:03:31', 'Pemasukan', '200000', 'Penjualan'),
(17, 3, 'KS-0000017', '2022-05-12 09:03:49', 'Pemasukan', '300000', 'Penjualan'),
(18, 3, 'KS-0000018', '2022-05-12 09:06:10', 'Pemasukan', '11000', 'Penjualan'),
(19, 3, 'KS-0000019', '2022-05-12 09:06:23', 'Pemasukan', '5500', 'Penjualan'),
(20, 3, 'KS-0000020', '2022-05-12 23:53:00', 'Pemasukan', '123123123', 'Penjualan'),
(21, 3, 'KS-0000021', '2022-05-13 00:58:48', 'Pemasukan', '400000', 'Penjualan'),
(22, 3, 'KS-0000022', '2022-05-13 08:23:14', 'Pemasukan', '162000', 'Penjualan'),
(23, 3, 'KS-0000023', '2022-05-13 08:25:13', 'Pemasukan', '300000', 'Penjualan'),
(24, 3, 'KS-0000024', '2022-05-13 08:25:47', 'Pemasukan', '300000', 'Penjualan'),
(25, 3, 'KS-0000025', '2022-05-13 08:35:37', 'Pemasukan', '315000', 'Penjualan'),
(26, 3, 'KS-0000026', '2022-05-13 08:40:28', 'Pemasukan', '144000', 'Penjualan'),
(27, 3, 'KS-0000027', '2022-05-13 09:08:58', 'Pemasukan', '503000', 'Penjualan'),
(28, 3, 'KS-0000028', '2022-05-13 09:10:06', 'Pemasukan', '35000', 'Penjualan'),
(29, 3, 'KS-0000029', '2022-05-13 09:15:01', 'Pemasukan', '18000', 'Penjualan'),
(30, 3, 'KS-0000030', '2022-05-13 09:20:36', 'Pemasukan', '75000', 'Penjualan'),
(31, 3, 'KS-0000031', '2022-05-13 09:24:43', 'Pemasukan', '300000', 'Penjualan'),
(32, 3, 'KS-0000032', '2022-05-13 09:26:00', 'Pemasukan', '450000', 'Penjualan'),
(33, 3, 'KS-0000033', '2022-05-13 09:30:01', 'Pemasukan', '160000', 'Penjualan'),
(34, 3, 'KS-0000034', '2022-05-13 09:33:05', 'Pemasukan', '264000', 'Penjualan'),
(35, 3, 'KS-0000035', '2022-05-13 09:37:05', 'Pemasukan', '396000', 'Penjualan'),
(36, 3, 'KS-0000036', '2022-05-13 09:41:48', 'Pemasukan', '14000', 'Penjualan'),
(37, 3, 'KS-0000037', '2022-05-13 09:43:30', 'Pemasukan', '228000', 'Penjualan'),
(38, 3, 'KS-0000038', '2022-05-13 09:44:58', 'Pemasukan', '548000', 'Penjualan'),
(39, 3, 'KS-0000039', '2022-05-13 09:46:00', 'Pemasukan', '760000', 'Penjualan'),
(40, 3, 'KS-0000040', '2022-05-13 09:59:11', 'Pemasukan', '535000', 'Penjualan'),
(41, 3, 'KS-0000041', '2022-05-13 10:02:52', 'Pemasukan', '5000', 'Penjualan'),
(42, 3, 'KS-0000042', '2022-05-13 10:13:46', 'Pemasukan', '67000', 'Penjualan'),
(43, 3, 'KS-0000043', '2022-05-13 10:15:18', 'Pemasukan', '87000', 'Penjualan'),
(44, 3, 'KS-0000044', '2022-05-13 10:24:36', 'Pemasukan', '38000', 'Penjualan'),
(45, 3, 'KS-0000045', '2022-05-13 10:28:59', 'Pemasukan', '24000', 'Penjualan'),
(46, 3, 'KS-0000046', '2022-05-13 10:32:42', 'Pemasukan', '60000', 'Penjualan'),
(47, 3, 'KS-0000047', '2022-05-13 10:36:54', 'Pemasukan', '180000', 'Penjualan'),
(48, 3, 'KS-0000048', '2022-05-13 10:40:20', 'Pemasukan', '24000', 'Penjualan'),
(49, 3, 'KS-0000049', '2022-05-13 10:43:47', 'Pemasukan', '280000', 'Penjualan'),
(50, 3, 'KS-0000050', '2022-05-13 10:49:24', 'Pemasukan', '56000', 'Penjualan'),
(51, 3, 'KS-0000051', '2022-05-13 10:50:38', 'Pemasukan', '37000', 'Penjualan'),
(52, 3, 'KS-0000052', '2022-05-13 10:53:47', 'Pemasukan', '61000', 'Penjualan'),
(53, 3, 'KS-0000053', '2022-05-13 10:59:32', 'Pemasukan', '19000', 'Penjualan'),
(54, 3, 'KS-0000054', '2022-05-13 11:03:19', 'Pemasukan', '24000', 'Penjualan'),
(55, 3, 'KS-0000055', '2022-05-13 11:09:49', 'Pemasukan', '92000', 'Penjualan'),
(56, 3, 'KS-0000056', '2022-05-13 11:16:38', 'Pemasukan', '200000', 'Penjualan'),
(57, 3, 'KS-0000057', '2022-05-13 11:24:11', 'Pemasukan', '130000', 'Penjualan'),
(58, 3, 'KS-0000058', '2022-05-13 11:34:29', 'Pemasukan', '85000', 'Penjualan'),
(59, 3, 'KS-0000059', '2022-05-13 11:39:46', 'Pemasukan', '50000', 'Penjualan'),
(60, 3, 'KS-0000060', '2022-05-13 11:43:42', 'Pemasukan', '100000', 'Penjualan'),
(61, 3, 'KS-0000061', '2022-05-13 11:52:05', 'Pemasukan', '200000', 'Penjualan');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `kategori` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`) VALUES
(15, 'Makanan'),
(16, 'Minuman'),
(17, 'Obat'),
(18, 'Sabun'),
(19, 'Pasta Gigi');

-- --------------------------------------------------------

--
-- Table structure for table `pajak_ppn`
--

CREATE TABLE `pajak_ppn` (
  `id_pajak` bigint(20) NOT NULL,
  `kode_pajak` varchar(20) DEFAULT NULL,
  `jenis` varchar(70) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `pajak_ppn`
--

INSERT INTO `pajak_ppn` (`id_pajak`, `kode_pajak`, `jenis`, `nominal`, `tanggal`, `keterangan`, `id_user`) VALUES
(31, 'PPN-0000001', 'PPN Keluaran', 4900, '2020-07-27 20:55:38', 'Penjualan', 1),
(37, 'PPN-0000002', 'PPN Keluaran', 26900, '2020-07-27 21:56:52', 'Penjualan', 1),
(38, 'PPN-0000003', 'PPN Disetorkan', 11111, '2021-03-05 11:34:58', '', 1),
(39, 'PPN-0000004', 'PPN Keluaran', 178200, '2021-06-13 20:51:35', 'Penjualan', 3),
(40, 'PPN-0000005', 'PPN Keluaran', 70500, '2021-06-16 10:29:38', 'Penjualan', 3),
(41, 'PPN-0000006', 'PPN Keluaran', 70500, '2021-06-16 10:42:27', 'Penjualan', 3),
(42, 'PPN-0000007', 'PPN Keluaran', 1200, '2022-04-25 09:03:19', 'Penjualan', 3),
(43, 'PPN-0000008', 'PPN Keluaran', 900, '2022-04-25 09:22:34', 'Penjualan', 3),
(44, 'PPN-0000009', 'PPN Keluaran', 71700, '2022-05-07 18:45:31', 'Penjualan', 3),
(45, 'PPN-0000010', 'PPN Keluaran', 85000, '2022-05-10 02:02:55', 'Penjualan', 3),
(46, 'PPN-0000011', 'PPN Keluaran', 10500, '2022-05-10 02:09:39', 'Penjualan', 3),
(47, 'PPN-0000012', 'PPN Keluaran', 44000, '2022-05-10 02:13:29', 'Penjualan', 3),
(48, 'PPN-0000013', 'PPN Keluaran', 55500, '2022-05-10 02:22:13', 'Penjualan', 3),
(49, 'PPN-0000014', 'PPN Keluaran', 38400, '2022-05-10 02:27:12', 'Penjualan', 3),
(50, 'PPN-0000015', 'PPN Keluaran', 17100, '2022-05-10 02:35:10', 'Penjualan', 3),
(51, 'PPN-0000016', 'PPN Keluaran', 47250, '2022-05-10 02:40:50', 'Penjualan', 3),
(52, 'PPN-0000017', 'PPN Keluaran', 23500, '2022-05-10 03:03:53', 'Penjualan', 3),
(53, 'PPN-0000018', 'PPN Keluaran', 7000, '2022-05-10 03:10:04', 'Penjualan', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id_beli` int(11) NOT NULL,
  `id_supplier` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `kode_beli` varchar(20) DEFAULT NULL,
  `tgl_faktur` varchar(20) DEFAULT NULL,
  `faktur_beli` varchar(20) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `method` varchar(30) DEFAULT NULL,
  `total` varchar(30) DEFAULT NULL,
  `bayar` int(11) DEFAULT NULL,
  `kembali` int(11) DEFAULT NULL,
  `tgl` datetime NOT NULL,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id_beli`, `id_supplier`, `id_user`, `kode_beli`, `tgl_faktur`, `faktur_beli`, `diskon`, `method`, `total`, `bayar`, `kembali`, `tgl`, `is_active`) VALUES
(33, 33, 1, 'PB-0000001', '2020-07-28', 'FJP-000001', 0, 'Kredit', '85000', 80000, 0, '2020-07-28 15:33:15', 1),
(34, 4, 1, 'PB-0000002', '2020-07-28', 'FJP-000002', 0, 'Cash', '31000', 50000, 19000, '2020-07-28 15:35:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id_jual` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_cs` int(11) DEFAULT NULL,
  `kode_jual` varchar(20) DEFAULT NULL,
  `invoice` varchar(50) DEFAULT NULL,
  `method` varchar(30) DEFAULT NULL,
  `bayar` int(11) DEFAULT NULL,
  `kembali` int(11) DEFAULT NULL,
  `ppn` int(11) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id_jual`, `id_user`, `id_cs`, `kode_jual`, `invoice`, `method`, `bayar`, `kembali`, `ppn`, `tgl`, `is_active`, `diskon`) VALUES
(298, 3, 1, 'KJ-0000001', 'POS20220510092738', 'Cash', 80000, 0, 0, '2022-05-10 09:27:38', 1, 0),
(299, 3, 1, 'KJ-0000002', 'POS20220510092952', 'Cash', 100000, 0, 0, '2022-05-10 09:29:52', 1, 20000),
(300, 3, 1, 'KJ-0000003', 'POS20220510093028', 'Cash', 50000, 30000, 0, '2022-05-10 09:30:28', 1, 0),
(301, 3, 1, 'KJ-0000004', 'POS20220510093314', 'Cash', 40000, 7500, 0, '2022-05-10 09:33:14', 1, 0),
(302, 3, 1, 'KJ-0000005', 'POS20220510094015', 'Cash', 15000, 2000, 0, '2022-05-10 09:40:15', 1, 0),
(303, 3, 1, 'KJ-0000006', 'POS20220510094041', 'Cash', 35000, 3500, 0, '2022-05-10 09:40:41', 1, 0),
(304, 3, 1, 'KJ-0000007', 'POS20220510094137', 'Cash', 80000, 20000, 0, '2022-05-10 09:41:37', 1, 0),
(305, 3, 1, 'KJ-0000008', 'POS20220510114354', 'Cash', 450000, 2500, 0, '2022-05-10 11:43:54', 1, 0),
(306, 3, 1, 'KJ-0000009', 'POS20220512083657', 'Cash', 590000, 6000, 0, '2022-05-12 08:36:58', 1, 0),
(307, 3, 1, 'KJ-0000010', 'POS20220512085122', 'Cash', 300000, 0, 0, '2022-05-12 08:51:22', 1, 0),
(308, 3, 1, 'KJ-0000011', 'POS20220512085438', 'Cash', 30000, 0, 0, '2022-05-12 08:54:38', 1, 0),
(309, 3, 1, 'KJ-0000012', 'POS20220512085603', 'Cash', 40000, 7000, 0, '2022-05-12 08:56:03', 1, 0),
(310, 3, 1, 'KJ-0000013', 'POS20220512085808', 'Cash', 1400000, 8000, 0, '2022-05-12 08:58:08', 1, 0),
(311, 3, 1, 'KJ-0000014', 'POS20220512090207', 'Cash', 150000, 10000, 0, '2022-05-12 09:02:07', 1, 0),
(312, 3, 1, 'KJ-0000015', 'POS20220512090300', 'Cash', 100000, 0, 0, '2022-05-12 09:03:00', 1, 0),
(313, 3, 1, 'KJ-0000016', 'POS20220512090331', 'Cash', 200000, 0, 0, '2022-05-12 09:03:31', 1, 0),
(314, 3, 1, 'KJ-0000017', 'POS20220512090349', 'Cash', 300000, 0, 0, '2022-05-12 09:03:49', 1, 0),
(315, 3, 1, 'KJ-0000018', 'POS20220512090609', 'Cash', 12000, 1000, 0, '2022-05-12 09:06:10', 1, 0),
(316, 3, 1, 'KJ-0000019', 'POS20220512090623', 'Cash', 10000, 4500, 0, '2022-05-12 09:06:23', 1, 0),
(317, 3, 1, 'KJ-0000020', 'POS20220512235300', 'Cash', 200000000, 76876877, 0, '2022-05-12 23:53:00', 1, 0),
(318, 3, 1, 'KJ-0000021', 'POS20220513005848', 'Cash', 400000, 0, 0, '2022-05-13 00:58:48', 1, 80000),
(319, 3, 1, 'KJ-0000022', 'POS20220513082314', 'Cash', 170000, 8000, 0, '2022-05-13 08:23:14', 1, 0),
(320, 3, 1, 'KJ-0000023', 'POS20220513082513', 'Cash', 300000, 0, 0, '2022-05-13 08:25:13', 1, 0),
(321, 3, 1, 'KJ-0000024', 'POS20220513082547', 'Cash', 300000, 0, 0, '2022-05-13 08:25:47', 1, 0),
(322, 3, 1, 'KJ-0000025', 'POS20220513083537', 'Cash', 350000, 35000, 0, '2022-05-13 08:35:37', 1, 0),
(323, 3, 1, 'KJ-0000026', 'POS20220513084028', 'Cash', 150000, 6000, 0, '2022-05-13 08:40:28', 1, 0),
(324, 3, 1, 'KJ-0000027', 'POS20220513090858', 'Cash', 510000, 7000, 0, '2022-05-13 09:08:58', 1, 0),
(325, 3, 1, 'KJ-0000028', 'POS20220513091006', 'Cash', 40000, 5000, 0, '2022-05-13 09:10:06', 1, 0),
(326, 3, 1, 'KJ-0000029', 'POS20220513091500', 'Cash', 20000, 2000, 0, '2022-05-13 09:15:00', 1, 0),
(327, 3, 1, 'KJ-0000030', 'POS20220513092036', 'Cash', 80000, 5000, 0, '2022-05-13 09:20:36', 1, 0),
(328, 3, 1, 'KJ-0000031', 'POS20220513092443', 'Cash', 500000, 200000, 0, '2022-05-13 09:24:43', 1, 0),
(329, 3, 1, 'KJ-0000032', 'POS20220513092559', 'Cash', 500000, 50000, 0, '2022-05-13 09:25:59', 1, 0),
(330, 3, 1, 'KJ-0000033', 'POS20220513093001', 'Cash', 200000, 40000, 0, '2022-05-13 09:30:01', 1, 0),
(331, 3, 1, 'KJ-0000034', 'POS20220513093305', 'Cash', 300000, 36000, 0, '2022-05-13 09:33:05', 1, 0),
(332, 3, 1, 'KJ-0000035', 'POS20220513093704', 'Cash', 400000, 4000, 0, '2022-05-13 09:37:04', 1, 0),
(333, 3, 1, 'KJ-0000036', 'POS20220513094147', 'Cash', 20000, 6000, 0, '2022-05-13 09:41:47', 1, 0),
(334, 3, 1, 'KJ-0000037', 'POS20220513094329', 'Cash', 300000, 72000, 0, '2022-05-13 09:43:29', 1, 0),
(335, 3, 1, 'KJ-0000038', 'POS20220513094458', 'Cash', 600000, 52000, 0, '2022-05-13 09:44:58', 1, 0),
(336, 3, 1, 'KJ-0000039', 'POS20220513094600', 'Cash', 800000, 40000, 0, '2022-05-13 09:46:00', 1, 0),
(337, 3, 1, 'KJ-0000040', 'POS20220513095911', 'Cash', 600000, 65000, 0, '2022-05-13 09:59:11', 1, 0),
(338, 3, 1, 'KJ-0000041', 'POS20220513100252', 'Cash', 5000, 0, 0, '2022-05-13 10:02:52', 1, 2000),
(339, 3, 1, 'KJ-0000042', 'POS20220513101346', 'Cash', 70000, 3000, 0, '2022-05-13 10:13:46', 1, 0),
(340, 3, 1, 'KJ-0000043', 'POS20220513101518', 'Cash', 90000, 3000, 0, '2022-05-13 10:15:18', 1, 0),
(341, 3, 1, 'KJ-0000044', 'POS20220513102436', 'Cash', 40000, 2000, 0, '2022-05-13 10:24:36', 1, 0),
(342, 3, 1, 'KJ-0000045', 'POS20220513102858', 'Cash', 25000, 1000, 0, '2022-05-13 10:28:58', 1, 0),
(343, 3, 1, 'KJ-0000046', 'POS20220513103241', 'Cash', 100000, 40000, 0, '2022-05-13 10:32:41', 1, 0),
(344, 3, 1, 'KJ-0000047', 'POS20220513103653', 'Cash', 200000, 20000, 0, '2022-05-13 10:36:53', 1, 0),
(345, 3, 1, 'KJ-0000048', 'POS20220513104020', 'Cash', 100000, 76000, 0, '2022-05-13 10:40:20', 1, 0),
(346, 3, 1, 'KJ-0000049', 'POS20220513104347', 'Cash', 300000, 20000, 0, '2022-05-13 10:43:47', 1, 0),
(347, 3, 1, 'KJ-0000050', 'POS20220513104924', 'Cash', 500000, 444000, 0, '2022-05-13 10:49:24', 1, 0),
(348, 3, 1, 'KJ-0000051', 'POS20220513105038', 'Cash', 40000, 3000, 0, '2022-05-13 10:50:38', 1, 0),
(349, 3, 1, 'KJ-0000052', 'POS20220513105346', 'Cash', 70000, 9000, 0, '2022-05-13 10:53:46', 1, 0),
(350, 3, 1, 'KJ-0000053', 'POS20220513105932', 'Cash', 20000, 1000, 0, '2022-05-13 10:59:32', 1, 0),
(351, 3, 1, 'KJ-0000054', 'POS20220513110319', 'Cash', 50000, 26000, 0, '2022-05-13 11:03:19', 1, 0),
(352, 3, 1, 'KJ-0000055', 'POS20220513110949', 'Cash', 100000, 8000, 0, '2022-05-13 11:09:49', 1, 0),
(353, 3, 1, 'KJ-0000056', 'POS20220513111638', 'Cash', 200000, 0, 0, '2022-05-13 11:16:38', 1, 0),
(354, 3, 1, 'KJ-0000057', 'POS20220513112411', 'Cash', 150000, 20000, 0, '2022-05-13 11:24:11', 1, 1000),
(355, 3, 1, 'KJ-0000058', 'POS20220513113429', 'Cash', 90000, 5000, 0, '2022-05-13 11:34:29', 1, 0),
(356, 3, 1, 'KJ-0000059', 'POS20220513113946', 'Cash', 100000, 50000, 0, '2022-05-13 11:39:46', 1, 50000),
(357, 3, 1, 'KJ-0000060', 'POS20220513114342', 'Cash', 176000, 76000, 0, '2022-05-13 11:43:42', 1, 5000),
(358, 3, 1, 'KJ-0000061', 'POS20220513115205', 'Cash', 300000, 100000, 0, '2022-05-13 11:52:05', 1, 18000);

-- --------------------------------------------------------

--
-- Table structure for table `piutang`
--

CREATE TABLE `piutang` (
  `id_piutang` bigint(20) NOT NULL,
  `id_jual` int(11) DEFAULT NULL,
  `tgl_piutang` datetime DEFAULT NULL,
  `jml_piutang` int(11) DEFAULT NULL,
  `bayar` int(11) DEFAULT NULL,
  `sisa` int(11) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `jatuh_tempo` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `piutang`
--

INSERT INTO `piutang` (`id_piutang`, `id_jual`, `tgl_piutang`, `jml_piutang`, `bayar`, `sisa`, `status`, `jatuh_tempo`) VALUES
(16, 240, '2022-05-10 00:04:09', 84910, 0, 84910, 'Belum Lunas', '');

-- --------------------------------------------------------

--
-- Table structure for table `profil_perusahaan`
--

CREATE TABLE `profil_perusahaan` (
  `id_toko` int(11) NOT NULL,
  `nama_toko` varchar(100) DEFAULT NULL,
  `alamat_toko` varchar(100) DEFAULT NULL,
  `telp_toko` varchar(15) DEFAULT NULL,
  `fax_toko` varchar(15) DEFAULT NULL,
  `email_toko` varchar(50) DEFAULT NULL,
  `website_toko` varchar(50) DEFAULT NULL,
  `logo_toko` varchar(50) DEFAULT NULL,
  `IG` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `profil_perusahaan`
--

INSERT INTO `profil_perusahaan` (`id_toko`, `nama_toko`, `alamat_toko`, `telp_toko`, `fax_toko`, `email_toko`, `website_toko`, `logo_toko`, `IG`) VALUES
(1, 'PRIMA MART', 'Jl. Krakatau No.60, Pd. Waluh, Kencong, Kec. Kencong', '(0336) 321378', '(0336) 321378', 'smkprima@ymail.com', 'https://esemkaprima.com/', 'logo_SMK.png', 'brusedbykarin');

-- --------------------------------------------------------

--
-- Table structure for table `request_barang`
--

CREATE TABLE `request_barang` (
  `id_request` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `status` varchar(55) NOT NULL,
  `harga_cabang` int(11) DEFAULT NULL,
  `id_cabang_request` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id_satuan` int(11) NOT NULL,
  `satuan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `satuan`) VALUES
(19, 'PCS'),
(20, 'BOTOL'),
(21, 'KOTAK');

-- --------------------------------------------------------

--
-- Table structure for table `servis`
--

CREATE TABLE `servis` (
  `id_servis` bigint(20) NOT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama_servis` varchar(200) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `servis`
--

INSERT INTO `servis` (`id_servis`, `kode`, `nama_servis`, `harga`, `keterangan`, `status`) VALUES
(6, 'SV022141', 'servis satu', 10000, 'Ini servis satu', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE `stok` (
  `id_stok` bigint(20) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jml` int(11) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jenis` varchar(50) DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `stok_opname`
--

CREATE TABLE `stok_opname` (
  `id_stok_opname` bigint(20) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `stok` varchar(10) DEFAULT NULL,
  `stok_nyata` varchar(10) DEFAULT NULL,
  `selisih` varchar(10) DEFAULT NULL,
  `nilai` varchar(50) DEFAULT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `kode_supplier` varchar(20) DEFAULT NULL,
  `nama_supplier` varchar(100) DEFAULT NULL,
  `alamat_supplier` varchar(100) DEFAULT NULL,
  `telp_supplier` varchar(15) DEFAULT NULL,
  `fax_supplier` varchar(15) DEFAULT NULL,
  `email_supplier` char(50) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `rekening` varchar(30) DEFAULT NULL,
  `atas_nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `kode_supplier`, `nama_supplier`, `alamat_supplier`, `telp_supplier`, `fax_supplier`, `email_supplier`, `bank`, `rekening`, `atas_nama`) VALUES
(38, 'SP-00001', 'PT. Wingsfoods', 'Jalan Raya\r\n', '087654321123', '087654321123', 'Wingsfoods@gmail.com', 'BCA', '14233567865', 'PT. Wingsfoods'),
(39, 'SP-00002', 'CV. Sabrina', 'Jalan Apa Saja\r\n', '081234567890', '081234567890', 'sabrina@gmail.com', 'BCA', '142567876543', 'CV. Sabrina'),
(40, 'SP-00003', 'PT. Ultrasakti', 'Jalani Saja Dulu', '082134567543', '082134567543', 'Ultrasakti@gmail.com', 'BCA', '142564321123', 'PT. Ultrasakti'),
(41, 'SP-00004', 'CV. Zayya Nusantara', 'Jalan Jalan Saja', '085432123456', '085432123456', 'Zayyanusantara@gmail.com', 'Mandiri', '145678654321', 'CV. Zayya Nusantara'),
(42, 'SP-00005', 'PT. Wings', 'Jalan Apa ya...', '087654321123', '087654321123', 'Wingsnew@gmail.com', 'BRI', '123456543212', 'PT. Wings');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `password` varchar(225) DEFAULT NULL,
  `tipe` varchar(30) DEFAULT NULL,
  `alamat_user` varchar(100) DEFAULT NULL,
  `telp_user` varchar(15) DEFAULT NULL,
  `email_user` varchar(50) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `id_ownercabang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `nama_lengkap`, `password`, `tipe`, `alamat_user`, `telp_user`, `email_user`, `is_active`, `id_ownercabang`) VALUES
(1, 'admin', 'Administrator', '$2y$10$VefjwtFGQZ.l0PIgLOuQFuu6NlHQyBxBkQPO72xnNJ9uVEDbuGyze', 'Administrator', 'Banyuwagi', '085647382748', 'admin@gmail.com', 1, NULL),
(3, 'kasir', 'Kasir', '$2y$10$0F.nhADYbfDopu6Bo85xoenYsxS/uC99NgipDtYDaKNcSlIQe2QnC', 'Kasir', 'Banyuwangi', '082236578566', 'kasir@gmail.com', 1, NULL),
(12, 'ownerutama', 'Owner Utama', '$2y$10$Br82KnjSwSxT6CvrdKaGKub7Q6mja5K3IWJ8gWpRDFWeJZzbdx9R2', 'OwnerUtama', 'Indonesia', '085467648576', 'ownerutama@gmail.com', 1, NULL),
(13, 'ownercabang', 'owner Cabang', '$2y$10$uzFw/qOGIowJftn0kly9SehK8Cdgp0PFnY8puNyZSJqsH5.L4TWlu', 'OwnerCabang', 'Indonesia', '0895745672', 'ownercabang@gmail.com', 1, NULL),
(14, 'sales', 'Sales', '$2y$10$oPVbuthjtrjgPJbSooDIWO.6u0LsqVAf7aFlwEbhTFgynW/FFx4um', 'Sales', 'Pasuruan', '08785465767', 'sales@gmail.com', 1, 13),
(15, 'gudang', 'Gudang', '$2y$10$vQ9pSrln2oGGlMNYsmkk1uSnxcn86wbEmdrr.CmMXY9KGeQUlRveW', 'Gudang', 'Indonesia', '08754553455', 'gudang@gmail.com', 1, NULL),
(16, 'ownercabang2', 'Owner Cabang 2', '$2y$10$T/R2YWjBFhFd/1PHClpXruQdqR8kLTBSuQOZVw4oaXtH.3Jw4k61K', 'OwnerCabang', 'Indonesia\r\n', '083546546575', 'owner2@mail.com', 1, NULL),
(17, 'salescabangkedua', 'sales cabang kedua', '$2y$10$mGFAl/M7UNT.PZnaJUmkVO6UG3Td6dUax/lpGXR8ueNb5pDwcYOJO', 'Sales', 'Bekasi', '083477346235', 'salescabangkedua@gmail.com', 1, 13),
(18, 'salescabangkedua1', 'salescabangkedua1', '$2y$10$U2aEA/BPCJhzmAQN45V3dORWViCTwaszqjyyL2mhdl7EtKzM00L5K', 'Sales', 'Jakarta', '064567845645', 'salescabangkeduaas@gmail.com', 0, 13),
(19, 'salescabang2', 'Sales Cabang 2', '$2y$10$cZEp7SesInM1UtHCU8l7OOk46aYu8QYpl5br/FnP2Rm5K1u3ZH4La', 'Sales', 'Kediri', '084565732355', 'salescabang2@gmail.com', 1, 16),
(20, 'tes', 'tes', '$2y$10$LubXQrJz7JkCD/lJqGMrLOKROeRg5sf7ftW0OjDgofPxl17bybqxO', 'Sales', 'Jember', 'tes', 'tes@gmail.com', 0, NULL),
(21, 'superadmin', 'Superadmin', '$2y$10$eQ8MzGUIRBFbn4b6LarHTOQZUWu0837yIlakKat5U2/sb1kQQu8oW', 'Administrator', 'Mmmmz', '039392', 'superadmin@gmail.com', 1, NULL),
(22, 'kasirku', 'kasirkasir', '$2y$10$p98F3B.v/ywektqvrx/H5eJ2x66CiJKHvpOhel5V2MiUbwaM7Wb5S', 'Kasir', 'fhfh', '098765', 'kasirkasir@gmail.com', 1, NULL),
(23, 'wefwefwe', 'fwefw', '$2y$10$iT3iBr4PsZb1DXoC5U6ibOQHOQFe4KSANDJIsWqVwojl3.k7NCT0e', 'Administrator', 'wefwefwef', 'wfe', 'wefwefw', 0, NULL),
(24, 'input1', 'Input1', '$2y$10$f81SjkbyjL7Hbv.r9a2SWeW0qoUlV7gsftaDuRHQ.7xYfzy/nE0RO', 'Gudang', 'kencong', '09876543', 'input1@gmail.com', 1, NULL),
(25, 'input2', 'input2', '$2y$10$H83QQpYa4CNomVZJlGDgY.ZXQatkAiJyF02mX3CPNSl5o/KvUUi6W', 'Gudang', 'Kencong', '8765432123456', 'input2@gmail.com', 1, NULL),
(26, 'input3', 'input3', '$2y$10$TuaYfs4te/xvBXX4jHo9OeJla9QDNBbCzrxnvTiMlM1p48tgOvzJO', 'Gudang', 'Kencong', '87654345678987', 'input3@gmail.com', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE `user_log` (
  `id_log` bigint(20) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `login` datetime DEFAULT NULL,
  `logout` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id_log`, `id_user`, `login`, `logout`) VALUES
(17, 1, '2020-07-25 22:58:04', '2020-07-25 18:08:55'),
(18, 1, '2020-07-25 23:09:00', '2020-07-25 18:16:40'),
(27, 1, '2020-07-30 14:31:23', '2020-07-30 13:03:25'),
(28, 1, '2020-07-31 20:00:43', '2020-07-31 15:24:45'),
(29, 3, '2020-07-31 20:24:51', '2020-07-31 15:27:10'),
(30, 1, '2020-07-31 20:27:14', '2020-12-09 14:59:59'),
(31, 1, '2020-12-09 20:48:33', '2020-12-09 14:59:59'),
(32, 3, '2020-12-09 21:00:05', '2020-12-09 15:01:04'),
(33, 1, '2020-12-09 21:01:16', '2020-12-09 15:10:43'),
(34, 3, '2020-12-09 21:10:47', '2020-12-09 15:46:20'),
(35, 1, '2020-12-09 21:46:25', '2020-12-09 15:46:47'),
(36, 3, '2020-12-09 21:46:53', '2020-12-09 15:47:23'),
(37, 1, '2020-12-09 21:47:15', '2020-12-09 15:47:23'),
(38, 1, '2020-12-09 21:47:32', '2020-12-09 15:56:15'),
(39, 3, '2020-12-09 21:56:20', '2020-12-09 16:02:41'),
(40, 3, '2020-12-09 22:02:45', '2020-12-09 16:02:49'),
(41, 1, '2020-12-09 22:02:53', '2020-12-09 16:12:28'),
(42, 12, '2020-12-09 22:15:14', '2020-12-09 16:31:07'),
(43, 12, '2020-12-09 22:20:56', '2020-12-09 16:31:07'),
(44, 1, '2020-12-09 22:31:13', '2020-12-09 16:37:30'),
(45, 12, '2020-12-09 22:37:36', '2020-12-09 16:55:46'),
(46, 12, '2020-12-09 22:55:49', '2020-12-09 17:18:20'),
(47, 12, '2020-12-09 22:56:05', '2020-12-09 17:18:20'),
(48, 12, '2020-12-09 22:57:37', '2020-12-09 17:18:20'),
(49, 12, '2020-12-09 23:03:53', '2020-12-09 17:18:20'),
(50, 12, '2020-12-09 23:04:51', '2020-12-09 17:18:20'),
(51, 12, '2020-12-09 23:07:46', '2020-12-09 17:18:20'),
(52, 12, '2020-12-09 23:08:07', '2020-12-09 17:18:20'),
(53, 12, '2020-12-09 23:18:24', '2020-12-09 17:18:40'),
(54, 1, '2020-12-09 23:18:47', '2020-12-09 17:34:16'),
(55, 12, '2020-12-09 23:34:18', '2020-12-09 22:33:32'),
(56, 12, '2020-12-09 23:35:09', '2020-12-09 22:33:32'),
(57, 12, '2020-12-09 23:35:47', '2020-12-09 22:33:32'),
(58, 12, '2020-12-10 04:26:48', '2020-12-09 22:33:32'),
(59, 1, '2020-12-10 04:33:37', '2020-12-10 03:39:47'),
(60, 1, '2020-12-10 06:47:48', '2020-12-10 03:39:47'),
(61, 12, '2020-12-10 09:39:50', '2020-12-10 09:04:11'),
(62, 12, '2020-12-10 14:48:26', '2020-12-10 09:04:11'),
(63, 1, '2020-12-10 15:04:21', '2020-12-10 09:04:35'),
(64, 15, '2020-12-10 15:04:42', '2020-12-10 09:21:01'),
(65, 1, '2020-12-10 15:21:09', '2020-12-10 09:23:24'),
(66, 15, '2020-12-10 15:23:35', '2020-12-10 11:45:52'),
(67, 15, '2020-12-10 15:26:33', '2020-12-10 11:45:52'),
(68, 15, '2020-12-10 17:29:29', '2020-12-10 11:45:52'),
(69, 12, '2020-12-10 17:45:56', '2020-12-10 11:46:11'),
(70, 1, '2020-12-10 17:46:15', '2020-12-10 11:48:20'),
(71, 15, '2020-12-10 17:48:26', '2020-12-10 12:11:10'),
(72, 15, '2020-12-10 18:09:28', '2020-12-10 12:11:10'),
(73, 15, '2020-12-10 18:10:45', '2020-12-10 12:11:10'),
(74, 1, '2020-12-10 18:11:15', '2020-12-10 12:11:34'),
(75, 15, '2020-12-10 18:11:47', '2020-12-10 12:21:22'),
(76, 1, '2020-12-10 18:21:27', '2020-12-10 12:24:20'),
(77, 15, '2020-12-10 18:24:25', '2020-12-10 19:21:15'),
(78, 1, '2020-12-11 01:21:02', '2020-12-10 19:21:15'),
(79, 12, '2020-12-11 01:21:20', '2020-12-10 19:21:38'),
(80, 15, '2020-12-11 01:21:44', '2020-12-10 19:37:58'),
(81, 13, '2020-12-11 02:10:23', '2020-12-10 20:55:01'),
(82, 13, '2020-12-11 02:11:04', '2020-12-10 20:55:01'),
(83, 13, '2020-12-11 02:14:24', '2020-12-10 20:55:01'),
(84, 13, '2020-12-11 02:19:40', '2020-12-10 20:55:01'),
(85, 13, '2020-12-11 02:46:05', '2020-12-10 20:55:01'),
(86, 15, '2020-12-11 02:55:08', '2020-12-10 21:01:10'),
(87, 15, '2020-12-11 03:00:07', '2020-12-10 21:01:10'),
(88, 13, '2020-12-11 03:01:20', '2020-12-10 21:01:25'),
(89, 15, '2020-12-11 03:01:34', '2020-12-10 21:04:12'),
(90, 1, '2020-12-11 03:04:17', '2020-12-10 21:08:25'),
(91, 15, '2020-12-11 03:08:38', '2020-12-10 21:53:45'),
(92, 15, '2020-12-11 03:40:44', '2020-12-10 21:53:45'),
(93, 15, '2020-12-11 03:46:32', '2020-12-10 21:53:45'),
(94, 13, '2020-12-11 03:53:54', '2020-12-10 21:54:52'),
(95, 15, '2020-12-11 03:54:57', '2020-12-10 23:01:04'),
(96, 15, '2020-12-11 05:01:11', '2020-12-10 23:02:15'),
(97, 13, '2020-12-11 05:02:25', '2020-12-10 23:07:32'),
(98, 3, '2020-12-11 05:07:42', '2020-12-10 23:09:38'),
(99, 13, '2020-12-11 05:10:59', '2020-12-10 23:13:06'),
(100, 15, '2020-12-11 05:13:16', '2020-12-10 23:13:36'),
(101, 15, '2020-12-11 05:13:42', '2020-12-10 23:13:48'),
(102, 13, '2020-12-11 05:14:02', '2020-12-10 23:39:01'),
(103, 13, '2020-12-11 05:37:24', '2020-12-10 23:39:01'),
(104, 12, '2020-12-11 05:38:55', '2020-12-10 23:39:01'),
(105, 13, '2020-12-11 05:39:21', '2020-12-11 00:04:19'),
(106, 13, '2020-12-11 05:41:31', '2020-12-11 00:04:19'),
(107, 13, '2020-12-11 05:41:59', '2020-12-11 00:04:19'),
(108, 13, '2020-12-11 05:42:29', '2020-12-11 00:04:19'),
(109, 3, '2020-12-11 06:04:28', '2020-12-11 00:11:43'),
(110, 13, '2020-12-11 06:12:07', '2020-12-11 00:37:49'),
(111, 1, '2020-12-11 06:37:56', '2020-12-11 00:40:07'),
(112, 13, '2020-12-11 06:40:19', '2020-12-11 10:40:27'),
(113, 12, '2020-12-11 16:19:18', '2020-12-11 10:40:27'),
(114, 13, '2020-12-11 16:50:42', '2020-12-11 12:07:44'),
(115, 13, '2020-12-11 17:12:44', '2020-12-11 12:07:44'),
(116, 15, '2020-12-11 18:07:51', '2020-12-11 12:27:24'),
(117, 12, '2020-12-11 18:27:26', '2020-12-11 12:27:36'),
(118, 13, '2020-12-11 18:27:43', '2020-12-11 12:28:04'),
(119, 15, '2020-12-11 18:28:09', '2020-12-11 12:54:58'),
(120, 13, '2020-12-11 18:55:20', '2020-12-12 00:18:00'),
(121, 12, '2020-12-12 06:17:27', '2020-12-12 00:18:00'),
(122, 1, '2020-12-12 06:18:05', '2020-12-12 13:24:38'),
(123, 13, '2020-12-12 19:23:24', '2020-12-12 13:24:38'),
(124, 1, '2020-12-12 19:24:46', '2020-12-12 13:27:09'),
(125, 13, '2020-12-12 19:27:19', '2020-12-12 13:27:25'),
(126, 1, '2020-12-12 19:27:29', '2020-12-12 17:50:28'),
(127, 13, '2020-12-12 19:47:53', '2020-12-12 17:50:28'),
(128, 13, '2020-12-13 00:12:02', '2020-12-12 18:17:40'),
(129, 13, '2020-12-13 00:20:23', '2020-12-12 18:24:17'),
(130, 14, '2020-12-13 00:24:25', '2020-12-12 18:28:39'),
(131, 14, '2020-12-13 00:24:59', '2020-12-12 18:28:39'),
(132, 14, '2020-12-13 00:25:12', '2020-12-12 18:28:39'),
(133, 14, '2020-12-13 00:28:04', '2020-12-12 18:28:39'),
(134, 14, '2020-12-13 00:28:45', '2020-12-12 18:48:23'),
(135, 13, '2020-12-13 00:48:51', '2020-12-12 18:49:30'),
(136, 15, '2020-12-13 00:49:35', '2020-12-12 18:51:07'),
(137, 16, '2020-12-13 00:51:27', '2020-12-12 18:53:02'),
(138, 15, '2020-12-13 00:53:08', '2020-12-12 18:57:03'),
(139, 13, '2020-12-13 00:57:12', '2020-12-12 18:57:31'),
(140, 15, '2020-12-13 00:57:36', '2020-12-12 18:59:02'),
(141, 13, '2020-12-13 00:59:21', '2020-12-12 18:59:50'),
(142, 15, '2020-12-13 00:59:57', '2020-12-12 19:03:58'),
(143, 13, '2020-12-13 01:04:10', '2020-12-12 19:04:50'),
(144, 15, '2020-12-13 01:04:57', '2020-12-12 19:06:50'),
(145, 13, '2020-12-13 01:06:59', '2020-12-12 19:07:11'),
(146, 14, '2020-12-13 01:07:16', '2020-12-12 19:08:32'),
(147, 13, '2020-12-13 01:08:50', '2020-12-12 19:09:09'),
(148, 14, '2020-12-13 01:09:14', '2020-12-12 19:26:02'),
(149, 13, '2020-12-13 01:26:12', '2020-12-12 19:37:54'),
(150, 14, '2020-12-13 01:37:59', '2020-12-12 19:45:26'),
(151, 3, '2020-12-13 01:45:31', '2020-12-12 19:47:12'),
(152, 13, '2020-12-13 01:47:38', '2020-12-12 19:51:21'),
(153, 14, '2020-12-13 01:51:25', '2020-12-12 20:12:24'),
(154, 13, '2020-12-13 02:12:42', '2020-12-12 20:23:27'),
(155, 14, '2020-12-13 02:23:31', '2020-12-12 20:37:21'),
(156, 13, '2020-12-13 02:37:31', '2020-12-12 20:38:10'),
(157, 14, '2020-12-13 02:38:32', '2020-12-12 20:56:04'),
(158, 13, '2020-12-13 02:56:13', '2020-12-12 20:56:32'),
(159, 14, '2020-12-13 02:56:45', '2021-02-05 06:33:32'),
(160, 1, '2021-02-05 13:05:56', '2021-02-05 06:33:32'),
(161, 1, '2021-02-05 13:33:40', '2021-02-12 15:19:59'),
(162, 1, '2021-02-12 20:34:32', '2021-02-12 15:19:59'),
(163, 3, '2021-02-12 22:20:04', '2021-02-12 15:41:57'),
(164, 15, '2021-02-12 22:42:02', '2021-02-12 15:47:15'),
(165, 14, '2021-02-12 22:47:19', '2021-02-12 15:49:29'),
(166, 1, '2021-02-12 22:49:33', '2021-02-12 15:51:19'),
(167, 3, '2021-02-12 22:51:25', '2021-02-12 15:51:59'),
(168, 15, '2021-02-12 22:52:05', '2021-02-12 15:53:06'),
(169, 14, '2021-02-12 22:53:11', '2021-02-12 15:57:09'),
(170, 15, '2021-02-12 22:57:15', '2021-02-12 16:06:57'),
(171, 14, '2021-02-12 23:07:02', '2021-02-13 21:31:02'),
(172, 3, '2021-02-14 03:49:31', '2021-02-13 21:31:02'),
(173, 14, '2021-02-14 04:31:09', '2021-02-13 21:42:12'),
(174, 1, '2021-02-14 04:42:17', '2021-02-13 23:32:49'),
(175, 14, '2021-02-14 06:28:48', '2021-02-13 23:32:49'),
(176, 3, '2021-02-14 06:33:01', '2021-02-13 23:33:19'),
(177, 14, '2021-02-14 06:33:35', '2021-02-13 23:35:00'),
(178, 1, '2021-02-14 06:35:04', '2021-02-13 23:53:56'),
(179, 14, '2021-02-14 06:54:01', '2021-02-14 00:15:11'),
(180, 1, '2021-02-14 07:15:15', '2021-02-14 00:22:55'),
(181, 14, '2021-02-14 07:22:59', '2021-02-14 00:29:19'),
(182, 14, '2021-02-14 07:29:24', '2021-02-14 00:29:56'),
(183, 14, '2021-02-14 07:30:00', '2021-02-14 00:39:09'),
(184, 1, '2021-02-14 07:39:12', '2021-02-14 01:06:03'),
(185, 14, '2021-02-14 08:06:08', '2021-02-14 01:06:13'),
(186, 3, '2021-02-14 08:06:21', '2021-02-14 01:06:29'),
(187, 15, '2021-02-14 08:06:35', '2021-02-14 04:32:30'),
(188, 3, '2021-02-14 09:44:44', '2021-02-14 04:32:30'),
(189, 3, '2021-02-14 11:31:57', '2021-02-14 04:32:30'),
(190, 3, '2021-02-14 11:33:00', '2021-02-14 04:33:07'),
(191, 1, '2021-02-14 11:34:19', '2021-02-14 04:35:07'),
(192, 3, '2021-02-14 11:35:15', '2021-02-14 04:36:28'),
(193, 3, '2021-02-14 11:36:36', '2021-02-14 04:37:25'),
(194, 3, '2021-02-18 12:13:56', '2021-02-18 09:17:14'),
(195, 1, '2021-02-18 12:15:16', '2021-02-18 09:17:14'),
(196, 3, '2021-02-18 16:15:28', '2021-02-18 09:17:14'),
(197, 3, '2021-02-18 16:16:19', '2021-02-18 09:17:14'),
(198, 3, '2021-02-20 16:30:39', '2021-02-20 09:30:57'),
(199, 14, '2021-02-20 16:31:04', '2021-02-20 09:31:23'),
(200, 1, '2021-02-21 08:14:52', '2021-02-21 01:17:53'),
(201, 3, '2021-02-21 08:17:58', '2021-02-21 01:18:27'),
(202, 1, '2021-02-21 08:18:31', '2021-02-21 01:18:57'),
(203, 3, '2021-02-21 08:19:01', '2021-02-21 01:21:14'),
(204, 1, '2021-02-21 08:21:19', '2021-02-21 01:27:45'),
(205, 3, '2021-02-21 08:24:28', '2021-02-21 01:27:45'),
(206, 14, '2021-02-21 08:27:50', '2021-02-21 01:29:21'),
(207, 14, '2021-02-21 08:29:32', '2021-02-21 01:30:36'),
(208, 1, '2021-02-21 08:30:40', '2021-02-21 01:31:16'),
(209, 14, '2021-02-21 08:31:21', '2021-02-21 01:31:33'),
(210, 15, '2021-02-21 08:31:41', '2021-02-21 01:32:30'),
(211, 1, '2021-02-21 08:32:36', '2021-02-21 01:33:04'),
(212, 14, '2021-02-21 08:33:10', '2021-02-21 01:41:29'),
(213, 1, '2021-02-21 08:41:38', '2021-02-21 01:46:34'),
(214, 14, '2021-02-21 08:46:38', '2021-02-21 01:49:24'),
(215, 15, '2021-02-21 08:49:31', '2021-02-21 02:00:13'),
(216, 1, '2021-02-21 09:00:18', '2021-02-22 02:45:34'),
(217, 3, '2021-02-21 14:05:25', '2021-02-22 02:45:34'),
(218, 3, '2021-02-21 14:05:56', '2021-02-22 02:45:34'),
(219, 3, '2021-02-21 14:06:36', '2021-02-22 02:45:34'),
(220, 3, '2021-02-22 09:45:20', '2021-02-22 02:45:34'),
(221, 1, '2021-02-22 09:45:42', '2021-03-04 09:14:20'),
(222, 3, '2021-02-22 09:46:28', '2021-03-04 09:14:20'),
(223, 1, '2021-02-22 09:48:27', '2021-03-04 09:14:20'),
(224, 1, '2021-02-24 15:21:22', '2021-03-04 09:14:20'),
(225, 3, '2021-02-24 15:27:09', '2021-03-04 09:14:20'),
(226, 14, '2021-02-27 10:00:33', '2021-03-04 09:14:20'),
(227, 1, '2021-03-01 07:18:34', '2021-03-04 09:14:20'),
(228, 1, '2021-03-04 08:39:00', '2021-03-04 09:14:20'),
(229, 1, '2021-03-04 16:11:06', '2021-03-04 09:14:20'),
(230, 1, '2021-03-04 16:11:17', '2021-03-04 09:14:20'),
(231, 1, '2021-03-04 16:11:58', '2021-03-04 09:14:20'),
(232, 1, '2021-03-04 16:14:16', '2021-03-04 09:14:20'),
(233, 1, '2021-03-04 16:14:32', '2021-03-05 03:17:27'),
(234, 1, '2021-03-04 16:16:09', '2021-03-05 03:17:27'),
(235, 1, '2021-03-04 16:16:28', '2021-03-05 03:17:27'),
(236, 1, '2021-03-04 20:02:34', '2021-03-05 03:17:27'),
(237, 14, '2021-03-05 09:46:49', '2021-03-05 03:17:27'),
(238, 1, '2021-03-05 10:17:33', '2021-03-05 03:19:00'),
(239, 15, '2021-03-05 10:19:05', '2021-03-05 03:19:56'),
(240, 14, '2021-03-05 10:20:04', '2021-03-05 03:22:13'),
(241, 15, '2021-03-05 10:22:18', '2021-03-05 03:52:37'),
(242, 14, '2021-03-05 10:52:42', '2021-03-05 03:53:50'),
(243, 1, '2021-03-05 10:53:54', '2021-03-06 00:23:59'),
(244, 1, '2021-03-05 11:08:35', '2021-03-06 00:23:59'),
(245, 1, '2021-03-05 11:09:52', '2021-03-06 00:23:59'),
(246, 1, '2021-03-05 11:13:58', '2021-03-06 00:23:59'),
(247, 1, '2021-03-05 18:29:41', '2021-03-06 00:23:59'),
(248, 3, '2021-03-06 07:23:31', '2021-03-06 00:23:59'),
(249, 15, '2021-03-06 07:24:18', '2021-03-06 00:28:18'),
(250, 1, '2021-03-06 07:25:51', '2021-03-06 00:28:18'),
(251, 3, '2021-03-06 07:28:26', '2021-03-06 00:28:44'),
(252, 14, '2021-03-06 07:28:55', '2021-03-06 00:29:48'),
(253, 1, '2021-03-06 07:29:54', '2021-03-06 00:47:29'),
(254, 1, '2021-03-06 07:54:59', '2021-03-06 00:55:27'),
(255, 15, '2021-03-06 07:55:31', '2021-03-06 00:56:14'),
(256, 3, '2021-03-06 07:56:18', '2021-03-06 08:55:12'),
(257, 14, '2021-03-06 15:53:41', '2021-03-06 08:55:12'),
(258, 1, '2021-03-06 15:55:21', '2021-03-06 08:56:58'),
(259, 15, '2021-03-06 15:57:03', '2021-03-06 08:57:30'),
(260, 14, '2021-03-06 16:13:20', '2021-03-06 09:14:46'),
(261, 1, '2021-03-06 16:14:52', '2021-03-06 09:15:56'),
(262, 15, '2021-03-06 16:16:01', '2021-03-06 09:16:56'),
(263, 1, '2021-03-06 16:17:44', '2021-03-06 09:19:13'),
(264, 1, '2021-03-06 18:37:17', '2021-03-06 12:43:53'),
(265, 3, '2021-03-06 19:44:07', '2021-03-06 12:54:05'),
(266, 1, '2021-03-06 19:48:17', '2021-03-06 12:54:05'),
(267, 1, '2021-03-06 19:54:18', '2021-03-06 12:54:27'),
(268, 3, '2021-03-06 19:54:34', '2021-03-06 13:22:51'),
(269, 15, '2021-03-07 09:20:29', '2021-03-07 02:38:34'),
(270, 14, '2021-03-07 09:38:43', '2021-03-12 05:34:40'),
(271, 1, '2021-03-08 09:47:40', '2021-03-12 05:34:40'),
(272, 1, '2021-03-08 09:58:19', '2021-03-12 05:34:40'),
(273, 3, '2021-03-08 10:09:15', '2021-03-12 05:34:40'),
(274, 1, '2021-03-08 15:32:12', '2021-03-12 05:34:40'),
(275, 1, '2021-03-10 18:21:13', '2021-03-12 05:34:40'),
(276, 1, '2021-03-10 18:32:27', '2021-03-12 05:34:40'),
(277, 3, '2021-03-12 12:33:23', '2021-03-12 05:34:40'),
(278, 1, '2021-03-12 12:34:47', '2021-03-12 08:38:34'),
(279, 1, '2021-03-12 15:35:15', '2021-03-12 08:38:34'),
(280, 1, '2021-03-12 15:41:37', '2021-03-12 08:43:55'),
(281, 3, '2021-03-12 15:44:03', '2021-03-12 09:54:01'),
(282, 1, '2021-03-12 16:54:07', '2021-03-14 02:23:11'),
(283, 1, '2021-03-12 20:03:17', '2021-03-14 02:23:11'),
(284, 1, '2021-03-13 08:21:02', '2021-03-14 02:23:11'),
(285, 1, '2021-03-13 09:52:41', '2021-03-14 02:23:11'),
(286, 3, '2021-03-13 10:13:05', '2021-03-14 02:23:11'),
(287, 3, '2021-03-13 12:15:36', '2021-03-14 02:23:11'),
(288, 1, '2021-03-13 12:39:20', '2021-03-14 02:23:11'),
(289, 3, '2021-03-13 12:44:59', '2021-03-14 02:23:11'),
(290, 1, '2021-03-14 09:20:10', '2021-03-14 02:23:11'),
(291, 15, '2021-03-14 09:23:26', '2021-03-14 02:48:22'),
(292, 3, '2021-03-14 09:48:26', '2021-03-14 02:55:51'),
(293, 1, '2021-03-14 09:50:50', '2021-03-14 02:55:51'),
(294, 3, '2021-03-14 09:55:55', '2021-03-14 03:00:14'),
(295, 1, '2021-03-14 10:00:21', '2021-03-14 03:01:00'),
(296, 3, '2021-03-14 10:01:03', '2021-03-14 03:11:31'),
(297, 14, '2021-03-14 10:11:39', '2021-03-14 03:13:44'),
(298, 1, '2021-03-14 10:13:47', '2021-03-15 04:59:06'),
(299, 1, '2021-03-15 11:57:30', '2021-03-15 04:59:06'),
(300, 14, '2021-03-15 11:59:21', '2021-03-15 04:59:40'),
(301, 3, '2021-03-15 11:59:59', '2021-03-15 05:00:14'),
(302, 15, '2021-03-15 12:00:47', '2021-03-15 05:01:18'),
(303, 1, '2021-03-22 10:11:24', '2021-03-22 03:19:34'),
(304, 13, '2021-03-22 10:13:24', '2021-03-22 03:19:34'),
(305, 12, '2021-03-22 10:14:29', '2021-03-22 03:19:34'),
(306, 12, '2021-03-22 10:19:24', '2021-03-22 03:19:34'),
(307, 13, '2021-03-22 10:19:39', '2021-03-22 03:20:11'),
(308, 15, '2021-03-22 10:20:25', '2021-03-22 03:21:22'),
(309, 3, '2021-03-22 10:21:26', '2021-03-22 03:22:14'),
(310, 13, '2021-03-22 10:22:18', '2021-04-01 18:06:18'),
(311, 3, '2021-03-24 16:52:43', '2021-04-01 18:06:18'),
(312, 1, '2021-03-28 23:57:29', '2021-04-01 18:06:18'),
(313, 1, '2021-03-30 10:19:49', '2021-04-01 18:06:18'),
(314, 1, '2021-03-30 11:06:49', '2021-04-01 18:06:18'),
(315, 1, '2021-03-30 15:03:00', '2021-04-01 18:06:18'),
(316, 1, '2021-03-30 15:03:01', '2021-04-01 18:06:18'),
(317, 3, '2021-03-30 15:07:26', '2021-04-01 18:06:18'),
(318, 1, '2021-03-30 17:40:34', '2021-04-01 18:06:18'),
(319, 1, '2021-03-30 18:35:52', '2021-04-01 18:06:18'),
(320, 3, '2021-03-31 19:35:16', '2021-04-01 18:06:18'),
(321, 1, '2021-03-31 22:49:19', '2021-04-01 18:06:18'),
(322, 15, '2021-04-02 01:04:51', '2021-04-01 18:06:18'),
(323, 14, '2021-04-02 01:06:29', '2021-04-01 18:08:01'),
(324, 3, '2021-04-02 01:08:14', '2021-04-18 15:20:10'),
(325, 1, '2021-04-03 07:34:34', '2021-04-18 15:20:10'),
(326, 3, '2021-04-03 07:39:42', '2021-04-18 15:20:10'),
(327, 3, '2021-04-03 07:40:00', '2021-04-18 15:20:10'),
(328, 1, '2021-04-03 07:57:00', '2021-04-18 15:20:10'),
(329, 3, '2021-04-03 08:45:20', '2021-04-18 15:20:10'),
(330, 1, '2021-04-06 18:56:58', '2021-04-18 15:20:10'),
(331, 3, '2021-04-06 18:57:09', '2021-04-18 15:20:10'),
(332, 1, '2021-04-07 13:43:52', '2021-04-18 15:20:10'),
(333, 1, '2021-04-18 22:19:05', '2021-04-18 15:20:10'),
(334, 1, '2021-04-18 22:20:23', '2021-04-18 16:44:17'),
(335, 1, '2021-04-18 23:44:11', '2021-04-18 16:44:17'),
(336, 3, '2021-04-18 23:44:21', '2021-04-18 17:27:14'),
(337, 1, '2021-04-19 00:26:36', '2021-04-18 17:27:14'),
(338, 3, '2021-04-19 00:27:25', '2021-04-18 17:28:32'),
(339, 1, '2021-04-19 02:34:18', '2021-04-18 19:35:48'),
(340, 21, '2021-04-19 02:35:57', '2021-04-18 19:36:17'),
(341, 3, '2021-04-19 02:36:24', '2021-04-19 00:16:27'),
(342, 3, '2021-04-19 07:12:58', '2021-04-19 00:16:27'),
(343, 1, '2021-04-19 07:16:31', '2021-04-19 00:16:47'),
(344, 3, '2021-04-19 07:16:52', '2021-04-19 00:17:03'),
(345, 1, '2021-04-19 07:17:07', '2021-04-19 00:17:38'),
(346, 14, '2021-04-19 07:17:45', '2021-04-19 01:01:25'),
(347, 1, '2021-04-19 08:01:30', '2021-04-19 13:06:53'),
(348, 1, '2021-04-19 20:06:10', '2021-04-19 13:06:53'),
(349, 14, '2021-04-19 20:07:35', '2021-04-19 13:10:11'),
(350, 1, '2021-04-19 20:08:37', '2021-04-19 13:10:11'),
(351, 3, '2021-04-19 20:10:15', '2021-04-19 13:14:24'),
(352, 14, '2021-04-19 20:15:41', '2021-04-19 13:20:12'),
(353, 1, '2021-04-19 20:17:29', '2021-04-19 13:20:12'),
(354, 3, '2021-04-19 20:20:17', '2021-04-19 13:26:53'),
(355, 1, '2021-04-19 20:26:59', '2021-04-20 06:24:09'),
(356, 1, '2021-04-19 20:39:06', '2021-04-20 06:24:09'),
(357, 1, '2021-04-20 13:23:12', '2021-04-20 06:24:09'),
(358, 3, '2021-04-20 13:24:14', '2021-04-20 06:25:07'),
(359, 1, '2021-04-20 13:25:13', '2021-04-20 06:26:10'),
(360, 3, '2021-04-20 13:26:15', '2021-04-20 07:50:00'),
(361, 3, '2021-04-20 14:45:52', '2021-04-20 07:50:00'),
(362, 1, '2021-04-20 14:50:05', '2021-04-20 08:10:03'),
(363, 14, '2021-04-20 15:10:08', '2021-04-20 08:11:51'),
(364, 1, '2021-04-20 15:12:05', '2021-04-20 08:12:47'),
(365, 3, '2021-04-20 15:12:51', '2021-04-20 08:13:20'),
(366, 1, '2021-04-20 15:13:26', '2021-04-20 10:06:36'),
(367, 14, '2021-04-20 17:00:29', '2021-04-20 10:06:36'),
(368, 14, '2021-04-20 17:05:21', '2021-04-20 10:06:36'),
(369, 1, '2021-04-20 17:06:43', '2021-04-20 10:07:17'),
(370, 3, '2021-04-20 17:07:23', '2021-04-20 14:40:23'),
(371, 3, '2021-04-20 21:39:02', '2021-04-20 14:40:23'),
(372, 1, '2021-04-20 21:40:27', '2021-04-20 14:41:55'),
(373, 14, '2021-04-20 21:43:13', '2021-04-20 14:47:34'),
(374, 1, '2021-04-20 21:44:32', '2021-04-20 14:47:34'),
(375, 3, '2021-04-20 21:47:39', '2021-04-20 14:50:14'),
(376, 22, '2021-04-20 21:52:10', '2021-04-20 14:52:18'),
(377, 1, '2021-04-23 06:36:20', '2021-04-25 05:51:09'),
(378, 3, '2021-04-25 12:49:07', '2021-04-25 05:51:09'),
(379, 15, '2021-04-25 12:51:16', '2021-04-25 05:52:31'),
(380, 1, '2021-04-25 22:22:59', '2021-04-25 15:23:45'),
(381, 3, '2021-04-25 22:23:55', '2021-04-28 06:50:00'),
(382, 1, '2021-04-27 21:17:21', '2021-04-28 06:50:00'),
(383, 1, '2021-04-27 21:49:46', '2021-04-28 06:50:00'),
(384, 1, '2021-04-27 21:50:48', '2021-04-28 06:50:00'),
(385, 1, '2021-04-28 13:46:51', '2021-04-28 06:50:00'),
(386, 3, '2021-04-28 13:50:08', '2021-04-28 06:51:10'),
(387, 15, '2021-04-28 13:51:34', '2021-04-28 06:52:17'),
(388, 14, '2021-04-28 13:52:26', '2021-04-28 06:53:05'),
(389, 1, '2021-04-28 21:05:49', '2021-04-30 06:42:55'),
(390, 1, '2021-04-28 21:08:58', '2021-04-30 06:42:55'),
(391, 1, '2021-04-28 22:24:26', '2021-04-30 06:42:55'),
(392, 1, '2021-04-30 13:33:01', '2021-04-30 06:42:55'),
(393, 15, '2021-04-30 13:44:10', '2021-04-30 06:46:31'),
(394, 14, '2021-04-30 13:44:40', '2021-04-30 06:46:31'),
(395, 3, '2021-04-30 13:46:42', '2021-04-30 06:47:00'),
(396, 1, '2021-04-30 13:47:07', '2021-04-30 06:51:09'),
(397, 1, '2021-05-01 11:14:46', '2021-05-01 04:17:28'),
(398, 3, '2021-05-01 11:19:25', '2021-05-01 04:22:52'),
(399, 15, '2021-05-01 11:23:27', '2021-05-01 04:24:11'),
(400, 14, '2021-05-01 11:24:15', '2021-05-01 04:25:24'),
(401, 1, '2021-05-01 11:25:30', '2021-05-04 04:12:26'),
(402, 1, '2021-05-04 11:08:57', '2021-05-04 04:12:26'),
(403, 14, '2021-05-04 11:12:32', '2021-05-04 04:13:08'),
(404, 3, '2021-05-04 11:13:13', '2021-05-04 04:16:32'),
(405, 1, '2021-05-04 11:16:37', '2021-05-04 12:21:44'),
(406, 3, '2021-05-04 19:19:30', '2021-05-04 12:21:44'),
(407, 15, '2021-05-04 19:21:58', '2021-05-04 12:24:25'),
(408, 14, '2021-05-04 19:24:37', '2021-05-04 12:25:38'),
(409, 1, '2021-05-04 19:25:44', '2021-05-05 09:00:45'),
(410, 1, '2021-05-05 13:06:52', '2021-05-05 09:00:45'),
(411, 1, '2021-05-05 15:58:41', '2021-05-05 09:00:45'),
(412, 1, '2021-05-05 15:59:20', '2021-05-05 09:00:45'),
(413, 3, '2021-05-05 16:00:54', '2021-05-06 00:57:18'),
(414, 1, '2021-05-06 07:52:26', '2021-05-06 00:57:18'),
(415, 1, '2021-05-06 07:55:24', '2021-05-06 00:57:18'),
(416, 3, '2021-05-06 07:57:25', '2021-05-14 10:14:34'),
(417, 1, '2021-05-09 09:15:26', '2021-05-14 10:14:34'),
(418, 1, '2021-05-14 17:13:34', '2021-05-14 10:14:34'),
(419, 14, '2021-05-14 17:14:46', '2021-05-14 10:16:41'),
(420, 3, '2021-05-14 17:16:54', '2021-05-14 10:20:25'),
(421, 15, '2021-05-14 17:20:42', '2021-05-14 12:52:06'),
(422, 14, '2021-05-14 19:46:24', '2021-05-14 12:52:06'),
(423, 3, '2021-05-14 19:47:29', '2021-05-14 12:52:06'),
(424, 1, '2021-05-14 19:52:18', '2021-05-14 12:56:10'),
(425, 15, '2021-05-14 19:56:18', '2021-05-14 14:18:09'),
(426, 1, '2021-05-21 08:20:05', '2021-05-21 01:20:38'),
(427, 3, '2021-05-21 08:20:27', '2021-05-21 01:20:38'),
(428, 1, '2021-05-21 08:20:45', '2021-05-21 02:52:18'),
(429, 1, '2021-05-21 08:22:03', '2021-05-21 02:52:18'),
(430, 1, '2021-05-21 09:38:03', '2021-05-21 02:52:18'),
(431, 1, '2021-05-21 09:46:59', '2021-05-21 02:52:18'),
(432, 3, '2021-05-21 09:52:22', '2021-05-21 02:54:41'),
(433, 1, '2021-05-21 09:54:44', '2021-05-22 12:26:04'),
(434, 3, '2021-05-21 10:35:53', '2021-05-22 12:26:04'),
(435, 1, '2021-05-22 18:37:20', '2021-05-22 12:26:04'),
(436, 3, '2021-05-22 19:26:11', '2021-05-26 19:44:57'),
(437, 3, '2021-05-22 23:03:29', '2021-05-26 19:44:57'),
(438, 21, '2021-05-23 14:42:37', '2021-05-26 19:44:57'),
(439, 1, '2021-05-27 02:40:17', '2021-05-26 19:44:57'),
(440, 15, '2021-05-27 02:45:00', '2021-06-02 00:05:33'),
(441, 1, '2021-06-02 07:01:45', '2021-06-02 00:05:33'),
(442, 1, '2021-06-02 07:05:57', '2021-06-02 10:01:43'),
(443, 1, '2021-06-02 07:16:44', '2021-06-02 10:01:43'),
(444, 1, '2021-06-02 07:16:49', '2021-06-02 10:01:43'),
(445, 1, '2021-06-02 07:17:07', '2021-06-02 10:01:43'),
(446, 1, '2021-06-02 07:17:20', '2021-06-02 10:01:43'),
(447, 3, '2021-06-02 07:17:35', '2021-06-02 10:01:43'),
(448, 3, '2021-06-02 07:17:53', '2021-06-02 10:01:43'),
(449, 1, '2021-06-02 16:55:13', '2021-06-02 10:01:43'),
(450, 3, '2021-06-02 17:01:50', '2021-06-02 10:02:58'),
(451, 1, '2021-06-02 17:03:04', '2021-06-02 10:28:44'),
(452, 15, '2021-06-02 17:28:51', '2021-06-02 10:30:48'),
(453, 14, '2021-06-02 17:30:55', '2021-06-02 10:32:17'),
(454, 3, '2021-06-02 17:32:25', '2021-06-02 10:34:43'),
(455, 1, '2021-06-02 17:34:49', '2021-06-02 10:44:35'),
(456, 3, '2021-06-02 17:44:45', '2021-06-02 10:45:32'),
(457, 1, '2021-06-02 17:45:37', '2021-06-02 10:46:22'),
(458, 3, '2021-06-02 17:46:27', '2021-06-02 10:48:58'),
(459, 1, '2021-06-02 17:49:04', '2021-06-13 13:49:32'),
(460, 1, '2021-06-04 08:39:41', '2021-06-13 13:49:32'),
(461, 1, '2021-06-13 20:44:16', '2021-06-13 13:49:32'),
(462, 3, '2021-06-13 20:49:40', '2021-06-13 13:54:52'),
(463, 15, '2021-06-13 20:54:59', '2021-06-13 13:55:35'),
(464, 14, '2021-06-13 20:55:41', '2021-06-13 13:56:56'),
(465, 1, '2021-06-13 20:57:03', '2021-06-13 13:59:38'),
(466, 14, '2021-06-13 20:59:46', '2021-06-13 14:00:32'),
(467, 3, '2021-06-13 21:00:38', '2021-06-16 03:27:45'),
(468, 1, '2021-06-16 10:16:32', '2021-06-16 03:27:45'),
(469, 1, '2021-06-16 10:16:37', '2021-06-16 03:27:45'),
(470, 1, '2021-06-16 10:16:48', '2021-06-16 03:27:45'),
(471, 3, '2021-06-16 10:17:06', '2021-06-16 03:27:45'),
(472, 1, '2021-06-16 10:17:55', '2021-06-16 03:27:45'),
(473, 3, '2021-06-16 10:28:02', '2021-06-16 03:35:20'),
(474, 15, '2021-06-16 10:35:39', '2021-06-16 03:36:20'),
(475, 14, '2021-06-16 10:36:32', '2021-06-16 03:36:41'),
(476, 1, '2021-06-16 10:38:45', '2021-06-16 03:41:52'),
(477, 3, '2021-06-16 10:41:55', '2021-06-16 03:51:04'),
(478, 15, '2021-06-16 10:51:14', '2021-06-16 03:51:34'),
(479, 1, '2021-06-16 10:51:43', '2021-06-28 00:04:12'),
(480, 1, '2021-06-28 07:03:51', '2021-06-28 00:04:12'),
(481, 1, '2021-06-28 07:04:21', '2021-06-28 00:04:29'),
(482, 3, '2021-06-28 07:04:34', '2021-06-28 00:05:06'),
(483, 1, '2021-06-29 06:10:43', '2021-06-28 23:24:14'),
(484, 1, '2021-06-29 14:49:11', '2021-06-29 08:11:20'),
(485, 14, '2021-06-29 15:12:12', '2021-06-29 08:12:37'),
(486, 15, '2021-06-29 15:12:48', '2021-06-29 08:13:32'),
(487, 3, '2021-06-29 15:13:40', '2021-06-29 08:14:31'),
(488, 21, '2021-06-29 15:14:02', '2021-06-29 08:14:31'),
(489, 1, '2021-06-29 15:14:40', '2021-06-29 10:41:57'),
(490, 1, '2021-06-29 15:14:41', '2021-06-29 10:41:57'),
(491, 1, '2021-06-29 15:35:06', '2021-06-29 10:41:57'),
(492, 1, '2021-06-29 15:35:44', '2021-06-29 10:41:57'),
(493, 1, '2021-06-29 17:38:50', '2021-06-29 10:41:57'),
(494, 1, '2021-06-29 17:38:55', '2021-06-29 10:41:57'),
(495, 14, '2021-06-29 17:42:02', '2021-06-29 10:56:46'),
(496, 21, '2021-06-29 17:55:34', '2021-06-29 10:56:46'),
(497, 21, '2021-06-29 17:57:22', '2021-06-29 10:59:32'),
(498, 21, '2021-06-30 07:06:48', '2021-06-30 00:08:19'),
(499, 21, '2021-06-30 21:10:44', '2021-06-30 14:11:20'),
(500, 21, '2021-06-30 23:27:47', '2021-06-30 16:30:44'),
(501, 1, '2021-07-01 18:34:04', '2021-07-01 11:42:12'),
(502, 1, '2021-07-01 18:39:03', '2021-07-01 11:42:12'),
(503, 3, '2021-07-01 18:42:16', '2021-07-09 13:58:33'),
(504, 1, '2021-07-04 23:21:44', '2021-07-09 13:58:33'),
(505, 1, '2021-07-04 23:22:20', '2021-07-09 13:58:33'),
(506, 1, '2021-07-09 13:31:57', '2021-07-09 13:58:33'),
(507, 21, '2021-07-09 20:57:58', '2021-07-09 13:58:33'),
(508, 21, '2021-07-10 23:18:37', '2021-07-10 16:20:19'),
(509, 21, '2021-07-10 23:20:28', '2022-01-05 14:21:49'),
(510, 1, '2022-01-05 19:56:02', '2022-01-05 14:21:49'),
(511, 1, '2022-01-05 20:02:00', '2022-01-05 14:21:49'),
(512, 1, '2022-01-05 20:02:40', '2022-01-05 14:21:49'),
(513, 1, '2022-01-05 20:03:24', '2022-01-05 14:21:49'),
(514, 1, '2022-01-05 20:06:18', '2022-01-05 14:21:49'),
(515, 1, '2022-01-05 20:21:11', '2022-01-05 14:21:49'),
(516, 1, '2022-01-05 21:16:03', '2022-01-05 14:21:49'),
(517, 1, '2022-01-05 21:21:56', '2022-01-06 04:01:39'),
(518, 1, '2022-01-06 09:41:07', '2022-01-06 04:01:39'),
(519, 1, '2022-01-06 10:57:23', '2022-01-06 04:01:39'),
(520, 1, '2022-01-06 11:00:58', '2022-01-06 04:01:39'),
(521, 21, '2022-01-06 11:01:43', '2022-01-19 16:40:12'),
(522, 1, '2022-01-19 23:39:15', '2022-01-19 16:40:12'),
(523, 1, '2022-01-23 13:11:28', '2022-01-23 06:11:59'),
(524, 1, '2022-01-23 13:12:04', '2022-01-23 06:12:29'),
(525, 1, '2022-01-23 13:12:34', '2022-01-23 06:16:16'),
(526, 1, '2022-02-20 21:43:05', '2022-02-20 14:46:31'),
(527, 3, '2022-02-20 21:46:36', '2022-02-20 14:46:54'),
(528, 1, '2022-04-12 21:29:45', '2022-04-14 05:54:23'),
(529, 21, '2022-04-14 12:27:15', '2022-04-14 05:54:23'),
(530, 3, '2022-04-14 12:54:32', '2022-04-25 02:02:46'),
(531, 1, '2022-04-20 11:56:49', '2022-04-25 02:02:46'),
(532, 1, '2022-04-25 09:02:32', '2022-04-25 02:02:46'),
(533, 3, '2022-04-25 09:02:51', '2022-04-25 02:03:29'),
(534, 15, '2022-04-25 09:03:34', '2022-04-25 02:03:39'),
(535, 14, '2022-04-25 09:03:45', '2022-04-25 02:04:13'),
(536, 1, '2022-04-25 09:04:52', '2022-04-25 02:09:24'),
(537, 1, '2022-04-25 09:14:45', '2022-04-25 02:15:55'),
(538, 1, '2022-04-25 09:14:53', '2022-04-25 02:15:55'),
(539, 1, '2022-04-25 09:15:15', '2022-04-25 02:15:55'),
(540, 1, '2022-04-25 09:15:35', '2022-04-25 02:15:55'),
(541, 1, '2022-04-25 09:16:05', '2022-04-25 02:21:00'),
(542, 1, '2022-04-25 09:16:33', '2022-04-25 02:21:00'),
(543, 3, '2022-04-25 09:21:06', '2022-04-25 02:22:55'),
(544, 15, '2022-04-25 09:23:05', '2022-04-25 02:24:08'),
(545, 1, '2022-04-25 09:24:21', '2022-04-25 02:27:46'),
(546, 3, '2022-04-25 09:27:50', '2022-04-25 02:29:48'),
(547, 1, '2022-04-25 09:29:52', '2022-04-25 02:34:01'),
(548, 3, '2022-04-25 09:34:05', '2022-04-25 02:45:49'),
(549, 1, '2022-04-25 09:44:23', '2022-04-25 02:45:49'),
(550, 3, '2022-04-25 09:45:58', '2022-04-25 02:46:12'),
(551, 1, '2022-04-25 09:46:23', '2022-04-25 02:47:42'),
(552, 1, '2022-04-25 09:47:47', '2022-04-25 03:07:07'),
(553, 3, '2022-04-25 10:07:12', '2022-05-07 11:06:58'),
(554, 1, '2022-04-25 10:45:03', '2022-05-07 11:06:58'),
(555, 1, '2022-04-30 13:58:55', '2022-05-07 11:06:58'),
(556, 1, '2022-05-05 06:11:14', '2022-05-07 11:06:58'),
(557, 1, '2022-05-05 06:18:30', '2022-05-07 11:06:58'),
(558, 1, '2022-05-07 18:02:02', '2022-05-07 11:06:58'),
(559, 21, '2022-05-07 18:02:14', '2022-05-07 11:06:58'),
(560, 1, '2022-05-07 18:03:16', '2022-05-07 11:06:58'),
(561, 1, '2022-05-07 18:05:39', '2022-05-07 11:06:58'),
(562, 1, '2022-05-07 18:21:39', '2022-05-07 11:27:50'),
(563, 3, '2022-05-07 18:27:54', '2022-05-07 11:31:13'),
(564, 1, '2022-05-07 18:31:17', '2022-05-07 11:31:38'),
(565, 3, '2022-05-07 18:31:43', '2022-05-07 11:48:45'),
(566, 1, '2022-05-07 18:34:20', '2022-05-07 11:48:45'),
(567, 1, '2022-05-07 18:34:28', '2022-05-07 11:48:45'),
(568, 1, '2022-05-07 18:34:57', '2022-05-07 11:48:45'),
(569, 1, '2022-05-07 18:48:53', '2022-05-07 12:27:48'),
(570, 3, '2022-05-07 19:27:55', '2022-05-07 12:28:20'),
(571, 1, '2022-05-07 19:28:26', '2022-05-07 12:29:29'),
(572, 15, '2022-05-07 19:29:33', '2022-05-07 12:29:46'),
(573, 3, '2022-05-07 19:29:49', '2022-05-07 12:31:06'),
(574, 15, '2022-05-07 19:31:15', '2022-05-07 12:31:20'),
(575, 1, '2022-05-07 19:31:23', '2022-05-07 12:33:31'),
(576, 15, '2022-05-07 19:33:35', '2022-05-07 12:33:45'),
(577, 3, '2022-05-07 19:33:49', '2022-05-07 12:40:00'),
(578, 1, '2022-05-08 09:46:33', '2022-05-08 02:48:59'),
(579, 3, '2022-05-08 09:47:07', '2022-05-08 02:48:59'),
(580, 1, '2022-05-08 09:47:13', '2022-05-08 02:48:59'),
(581, 1, '2022-05-08 09:47:22', '2022-05-08 02:48:59'),
(582, 1, '2022-05-08 09:47:29', '2022-05-08 02:48:59'),
(583, 1, '2022-05-08 09:48:02', '2022-05-08 02:48:59'),
(584, 1, '2022-05-08 09:48:55', '2022-05-08 02:48:59'),
(585, 3, '2022-05-08 09:49:02', '2022-05-08 02:57:29'),
(586, 1, '2022-05-08 09:57:33', '2022-05-08 03:00:08'),
(587, 1, '2022-05-08 10:00:25', '2022-05-08 06:47:49'),
(588, 1, '2022-05-08 11:43:34', '2022-05-08 06:47:49'),
(589, 15, '2022-05-08 11:47:53', '2022-05-08 06:52:03'),
(590, 1, '2022-05-08 11:52:13', '2022-05-08 07:08:01'),
(591, 3, '2022-05-08 12:08:05', '2022-05-08 09:49:06'),
(592, 1, '2022-05-08 12:29:31', '2022-05-08 09:49:06'),
(593, 1, '2022-05-08 14:50:17', '2022-05-08 08:17:10'),
(594, 1, '2022-05-08 15:17:00', '2022-05-08 08:17:10'),
(595, 1, '2022-05-08 15:19:33', '2022-05-08 08:20:35'),
(596, 3, '2022-05-08 15:20:39', '2022-05-09 01:10:50'),
(597, 3, '2022-05-08 15:23:17', '2022-05-09 01:10:50'),
(598, 3, '2022-05-08 15:24:37', '2022-05-09 01:10:50'),
(599, 3, '2022-05-09 05:54:01', '2022-05-09 01:10:50'),
(600, 1, '2022-05-09 08:10:03', '2022-05-09 01:10:50'),
(601, 3, '2022-05-09 08:10:55', '2022-05-09 13:07:54'),
(602, 3, '2022-05-09 18:05:28', '2022-05-09 13:07:54'),
(603, 1, '2022-05-09 18:08:00', '2022-05-09 13:17:38'),
(604, 1, '2022-05-09 18:12:01', '2022-05-09 13:17:38'),
(605, 3, '2022-05-09 18:17:43', '2022-05-09 21:12:10'),
(606, 3, '2022-05-09 18:20:48', '2022-05-09 21:12:10'),
(607, 3, '2022-05-09 22:56:33', '2022-05-09 21:12:10'),
(608, 3, '2022-05-10 01:05:08', '2022-05-09 21:12:10'),
(609, 1, '2022-05-10 02:12:17', '2022-05-09 21:12:35'),
(610, 3, '2022-05-10 02:12:39', '2022-05-09 22:12:56'),
(611, 1, '2022-05-10 03:13:02', '2022-05-09 22:21:17'),
(612, 3, '2022-05-10 03:21:25', '2022-05-09 22:22:49'),
(613, 1, '2022-05-10 03:23:00', '2022-05-09 22:24:09'),
(614, 3, '2022-05-10 03:24:13', '2022-05-10 02:25:20'),
(615, 1, '2022-05-10 03:24:53', '2022-05-10 02:25:20'),
(616, 1, '2022-05-10 07:24:18', '2022-05-10 02:25:20'),
(617, 3, '2022-05-10 07:25:26', '2022-05-10 02:26:21'),
(618, 1, '2022-05-10 07:28:50', '2022-05-10 02:31:04'),
(619, 3, '2022-05-10 07:31:18', '2022-05-10 02:31:31'),
(620, 1, '2022-05-10 07:32:56', '2022-05-10 02:37:28'),
(621, 15, '2022-05-10 07:37:37', '2022-05-10 02:37:43'),
(622, 3, '2022-05-10 09:26:49', '2022-05-10 04:31:38'),
(623, 1, '2022-05-10 09:31:42', '2022-05-10 04:32:24'),
(624, 3, '2022-05-10 09:32:31', '2022-05-10 04:38:57'),
(625, 1, '2022-05-10 09:39:02', '2022-05-10 04:39:48'),
(626, 3, '2022-05-10 09:39:54', '2022-05-10 04:45:24'),
(627, 15, '2022-05-10 09:46:24', '2022-05-10 04:52:20'),
(628, 1, '2022-05-10 09:52:26', '2022-05-10 06:42:33'),
(629, 3, '2022-05-10 11:42:38', '2022-05-12 17:55:45'),
(630, 3, '2022-05-12 08:35:53', '2022-05-12 17:55:45'),
(631, 3, '2022-05-12 08:51:09', '2022-05-12 17:55:45'),
(632, 3, '2022-05-12 08:54:28', '2022-05-12 17:55:45'),
(633, 3, '2022-05-12 08:57:32', '2022-05-12 17:55:45'),
(634, 3, '2022-05-12 09:01:54', '2022-05-12 17:55:45'),
(635, 3, '2022-05-12 22:50:26', '2022-05-12 17:55:45'),
(636, 1, '2022-05-12 22:57:06', '2022-05-12 18:52:32'),
(637, 3, '2022-05-12 23:52:36', '2022-05-12 18:55:31'),
(638, 1, '2022-05-12 23:53:51', '2022-05-12 18:55:31'),
(639, 15, '2022-05-12 23:55:36', '2022-05-12 19:04:55'),
(640, 3, '2022-05-13 00:05:00', '2022-05-12 19:52:36'),
(641, 3, '2022-05-13 00:52:53', '2022-05-12 19:53:12'),
(642, 1, '2022-05-13 00:53:22', '2022-05-12 19:55:50'),
(643, 15, '2022-05-13 00:56:05', '2022-05-12 19:56:17'),
(644, 1, '2022-05-13 00:56:37', '2022-05-12 19:58:01'),
(645, 3, '2022-05-13 00:58:12', '2022-05-13 02:40:19'),
(646, 1, '2022-05-13 07:40:04', '2022-05-13 02:40:19'),
(647, 1, '2022-05-13 07:40:24', '2022-05-13 02:43:53'),
(648, 24, '2022-05-13 07:44:01', '2022-05-13 02:44:09'),
(649, 1, '2022-05-13 08:13:31', '2022-05-13 03:15:10'),
(650, 24, '2022-05-13 08:15:55', '2022-05-13 03:20:50'),
(651, 3, '2022-05-13 08:20:56', '2022-05-13 03:27:19'),
(652, 24, '2022-05-13 08:27:34', '2022-05-13 03:27:40'),
(653, 24, '2022-05-13 08:28:31', '2022-05-13 03:33:39'),
(654, 3, '2022-05-13 08:33:55', '2022-05-13 03:35:54'),
(655, 24, '2022-05-13 08:36:23', '2022-05-13 03:39:12'),
(656, 3, '2022-05-13 08:39:25', '2022-05-13 03:55:44'),
(657, 24, '2022-05-13 08:55:59', '2022-05-13 04:00:51'),
(658, 3, '2022-05-13 09:00:59', '2022-05-13 04:02:14'),
(659, 24, '2022-05-13 09:02:28', '2022-05-13 04:06:49'),
(660, 3, '2022-05-13 09:07:16', '2022-05-13 04:10:23'),
(661, 24, '2022-05-13 09:12:06', '2022-05-13 04:13:52'),
(662, 3, '2022-05-13 09:14:03', '2022-05-13 04:15:23'),
(663, 24, '2022-05-13 09:15:51', '2022-05-13 04:18:31'),
(664, 3, '2022-05-13 09:18:54', '2022-05-13 04:20:52'),
(665, 3, '2022-05-13 09:23:14', '2022-05-13 04:25:04'),
(666, 3, '2022-05-13 09:25:17', '2022-05-13 04:26:24'),
(667, 24, '2022-05-13 09:27:05', '2022-05-13 04:29:05'),
(668, 3, '2022-05-13 09:29:14', '2022-05-13 04:30:23'),
(669, 24, '2022-05-13 09:30:38', '2022-05-13 04:31:59'),
(670, 3, '2022-05-13 09:32:17', '2022-05-13 04:33:25'),
(671, 24, '2022-05-13 09:33:40', '2022-05-13 04:36:01'),
(672, 3, '2022-05-13 09:36:14', '2022-05-13 04:37:24'),
(673, 24, '2022-05-13 09:37:39', '2022-05-13 04:40:39'),
(674, 3, '2022-05-13 09:40:52', '2022-05-13 04:46:38'),
(675, 24, '2022-05-13 09:46:57', '2022-05-13 04:49:38'),
(676, 24, '2022-05-13 09:50:53', '2022-05-13 04:56:05'),
(677, 3, '2022-05-13 09:56:18', '2022-05-13 05:03:49'),
(678, 24, '2022-05-13 10:04:00', '2022-05-13 05:04:18'),
(679, 24, '2022-05-13 10:05:19', '2022-05-13 05:10:57'),
(680, 3, '2022-05-13 10:11:08', '2022-05-13 05:11:54'),
(681, 24, '2022-05-13 10:12:33', '2022-05-13 05:12:46'),
(682, 3, '2022-05-13 10:12:53', '2022-05-13 05:16:21'),
(683, 1, '2022-05-13 10:17:06', '2022-05-13 05:17:33'),
(684, 24, '2022-05-13 10:18:03', '2022-05-13 05:22:46'),
(685, 3, '2022-05-13 10:23:02', '2022-05-13 05:24:52'),
(686, 24, '2022-05-13 10:25:15', '2022-05-13 05:27:48'),
(687, 3, '2022-05-13 10:28:04', '2022-05-13 05:29:20'),
(688, 24, '2022-05-13 10:29:24', '2022-05-13 05:31:31'),
(689, 3, '2022-05-13 10:31:47', '2022-05-13 05:32:54'),
(690, 24, '2022-05-13 10:33:16', '2022-05-13 05:35:39'),
(691, 3, '2022-05-13 10:35:55', '2022-05-13 05:37:17'),
(692, 24, '2022-05-13 10:37:26', '2022-05-13 05:39:33'),
(693, 3, '2022-05-13 10:39:42', '2022-05-13 05:40:37'),
(694, 24, '2022-05-13 10:40:44', '2022-05-13 05:42:44'),
(695, 3, '2022-05-13 10:42:51', '2022-05-13 05:44:07'),
(696, 24, '2022-05-13 10:45:11', '2022-05-13 05:47:35'),
(697, 3, '2022-05-13 10:47:56', '2022-05-13 05:50:54'),
(698, 24, '2022-05-13 10:51:12', '2022-05-13 05:53:02'),
(699, 3, '2022-05-13 10:53:17', '2022-05-13 05:54:52'),
(700, 24, '2022-05-13 10:56:11', '2022-05-13 05:58:42'),
(701, 3, '2022-05-13 10:59:01', '2022-05-13 05:59:49'),
(702, 24, '2022-05-13 11:00:08', '2022-05-13 06:02:36'),
(703, 3, '2022-05-13 11:02:47', '2022-05-13 06:03:31'),
(704, 24, '2022-05-13 11:03:35', '2022-05-13 06:05:31'),
(705, 3, '2022-05-13 11:05:51', '2022-05-13 06:10:05'),
(706, 24, '2022-05-13 11:10:17', '2022-05-13 06:14:01'),
(707, 3, '2022-05-13 11:14:08', '2022-05-13 06:18:59'),
(708, 24, '2022-05-13 11:19:06', '2022-05-13 06:21:48'),
(709, 3, '2022-05-13 11:21:54', '2022-05-13 06:29:10'),
(710, 24, '2022-05-13 11:29:28', '2022-05-13 06:32:38'),
(711, 3, '2022-05-13 11:32:50', '2022-05-13 06:34:50'),
(712, 24, '2022-05-13 11:36:01', '2022-05-13 06:38:13'),
(713, 3, '2022-05-13 11:38:19', '2022-05-13 06:40:00'),
(714, 24, '2022-05-13 11:40:26', '2022-05-13 06:40:46'),
(715, 24, '2022-05-13 11:40:55', '2022-05-13 06:42:39'),
(716, 3, '2022-05-13 11:42:45', '2022-05-13 06:44:10'),
(717, 24, '2022-05-13 11:44:28', '2022-05-13 06:50:04'),
(718, 3, '2022-05-13 11:50:09', '2022-05-13 06:52:10'),
(719, 1, '2022-05-16 10:26:51', '2022-05-16 05:52:37'),
(720, 1, '2022-05-16 12:40:57', NULL);

-- --------------------------------------------------------

--
-- Structure for view `coba`
--
DROP TABLE IF EXISTS `coba`;

CREATE ALGORITHM=UNDEFINED DEFINER= CURRENT_USER SQL SECURITY DEFINER VIEW `coba`  AS SELECT `barang`.`id_barang` AS `id_barang`, `barang`.`nama_barang` AS `nama_barang`, `barang`.`barcode` AS `barcode`, `barang`.`harga_pelanggan` AS `harga_pelanggan`, `barang`.`harga_sales` AS `harga_sales`, `satuan`.`satuan` AS `satuan`, `kategori`.`kategori` AS `kategori`, `barang`.`harga_beli` AS `harga_beli`, `barang`.`harga_jual` AS `harga_jual`, `barang`.`stok` AS `stok` FROM ((`barang` join `kategori` on(`kategori`.`id_kategori` = `barang`.`id_kategori`)) join `satuan` on(`satuan`.`id_satuan` = `barang`.`id_satuan`)) WHERE `barang`.`is_active` = 11  ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id_bank`) USING BTREE;

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`) USING BTREE,
  ADD KEY `ID_KATEGORI` (`id_kategori`) USING BTREE,
  ADD KEY `ID_SATUAN` (`id_satuan`) USING BTREE,
  ADD KEY `ID_SUPPLIER` (`id_supplier`) USING BTREE,
  ADD KEY `ID_USER` (`id_user`) USING BTREE;

--
-- Indexes for table `barang_per_member`
--
ALTER TABLE `barang_per_member`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_cs`) USING BTREE;

--
-- Indexes for table `customer_request_barang`
--
ALTER TABLE `customer_request_barang`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `detil_hutang`
--
ALTER TABLE `detil_hutang`
  ADD PRIMARY KEY (`id_detil_hutang`) USING BTREE;

--
-- Indexes for table `detil_pembelian`
--
ALTER TABLE `detil_pembelian`
  ADD PRIMARY KEY (`id_detil_beli`) USING BTREE,
  ADD KEY `FK_BARANG_DETIL_PEMBELIAN` (`id_barang`) USING BTREE,
  ADD KEY `FK_PEMBELIAN_DETIL` (`id_beli`) USING BTREE;

--
-- Indexes for table `detil_penjualan`
--
ALTER TABLE `detil_penjualan`
  ADD PRIMARY KEY (`id_detil_jual`) USING BTREE,
  ADD KEY `FK_BARANG_PENJUALAN_DETIL` (`id_barang`) USING BTREE,
  ADD KEY `FK_PENJUALAN_DETIL` (`id_jual`) USING BTREE;

--
-- Indexes for table `detil_piutang`
--
ALTER TABLE `detil_piutang`
  ADD PRIMARY KEY (`id_detil_piutang`) USING BTREE;

--
-- Indexes for table `hargabarang_request`
--
ALTER TABLE `hargabarang_request`
  ADD PRIMARY KEY (`id_hbr`) USING BTREE;

--
-- Indexes for table `historyupdatestok`
--
ALTER TABLE `historyupdatestok`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `hutang`
--
ALTER TABLE `hutang`
  ADD PRIMARY KEY (`id_hutang`) USING BTREE;

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`) USING BTREE;

--
-- Indexes for table `kas`
--
ALTER TABLE `kas`
  ADD PRIMARY KEY (`id_kas`) USING BTREE,
  ADD KEY `ID_USER` (`id_user`) USING BTREE;

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`) USING BTREE;

--
-- Indexes for table `pajak_ppn`
--
ALTER TABLE `pajak_ppn`
  ADD PRIMARY KEY (`id_pajak`) USING BTREE,
  ADD KEY `ID_USER` (`id_user`) USING BTREE;

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_beli`) USING BTREE,
  ADD KEY `FK_MENCATAT_PEMBELIAN` (`id_user`) USING BTREE,
  ADD KEY `FK_TRANSAKSI_PEMBELIAN` (`id_supplier`) USING BTREE;

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_jual`) USING BTREE,
  ADD KEY `FK_MELAYANI` (`id_user`) USING BTREE,
  ADD KEY `FK_TRANSAKSI` (`id_cs`) USING BTREE;

--
-- Indexes for table `piutang`
--
ALTER TABLE `piutang`
  ADD PRIMARY KEY (`id_piutang`) USING BTREE;

--
-- Indexes for table `profil_perusahaan`
--
ALTER TABLE `profil_perusahaan`
  ADD PRIMARY KEY (`id_toko`) USING BTREE;

--
-- Indexes for table `request_barang`
--
ALTER TABLE `request_barang`
  ADD PRIMARY KEY (`id_request`) USING BTREE;

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`) USING BTREE;

--
-- Indexes for table `servis`
--
ALTER TABLE `servis`
  ADD PRIMARY KEY (`id_servis`) USING BTREE;

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id_stok`) USING BTREE,
  ADD KEY `id_barang` (`id_barang`) USING BTREE;

--
-- Indexes for table `stok_opname`
--
ALTER TABLE `stok_opname`
  ADD PRIMARY KEY (`id_stok_opname`) USING BTREE,
  ADD KEY `ID_BARANG` (`id_barang`) USING BTREE;

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`) USING BTREE;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`) USING BTREE;

--
-- Indexes for table `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id_log`) USING BTREE,
  ADD KEY `ID_USER` (`id_user`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id_bank` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `barang_per_member`
--
ALTER TABLE `barang_per_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_cs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer_request_barang`
--
ALTER TABLE `customer_request_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `detil_hutang`
--
ALTER TABLE `detil_hutang`
  MODIFY `id_detil_hutang` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `detil_pembelian`
--
ALTER TABLE `detil_pembelian`
  MODIFY `id_detil_beli` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `detil_penjualan`
--
ALTER TABLE `detil_penjualan`
  MODIFY `id_detil_jual` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `detil_piutang`
--
ALTER TABLE `detil_piutang`
  MODIFY `id_detil_piutang` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `hargabarang_request`
--
ALTER TABLE `hargabarang_request`
  MODIFY `id_hbr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `historyupdatestok`
--
ALTER TABLE `historyupdatestok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `hutang`
--
ALTER TABLE `hutang`
  MODIFY `id_hutang` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `kas`
--
ALTER TABLE `kas`
  MODIFY `id_kas` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `pajak_ppn`
--
ALTER TABLE `pajak_ppn`
  MODIFY `id_pajak` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id_beli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id_jual` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=359;

--
-- AUTO_INCREMENT for table `piutang`
--
ALTER TABLE `piutang`
  MODIFY `id_piutang` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `profil_perusahaan`
--
ALTER TABLE `profil_perusahaan`
  MODIFY `id_toko` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `request_barang`
--
ALTER TABLE `request_barang`
  MODIFY `id_request` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `servis`
--
ALTER TABLE `servis`
  MODIFY `id_servis` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
  MODIFY `id_stok` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stok_opname`
--
ALTER TABLE `stok_opname`
  MODIFY `id_stok_opname` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id_log` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=721;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `ITEM_SUPPLIER` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id_supplier`) ON UPDATE CASCADE,
  ADD CONSTRAINT `KATEGORI_BARANG` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`) ON UPDATE CASCADE,
  ADD CONSTRAINT `SATUAN_BARANG` FOREIGN KEY (`id_satuan`) REFERENCES `satuan` (`id_satuan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `detil_pembelian`
--
ALTER TABLE `detil_pembelian`
  ADD CONSTRAINT `FK_BARANG_DETIL_PEMBELIAN` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`),
  ADD CONSTRAINT `FK_PEMBELIAN_DETIL` FOREIGN KEY (`id_beli`) REFERENCES `pembelian` (`id_beli`);

--
-- Constraints for table `detil_penjualan`
--
ALTER TABLE `detil_penjualan`
  ADD CONSTRAINT `FK_PENJUALAN_DETIL` FOREIGN KEY (`id_jual`) REFERENCES `penjualan` (`id_jual`);

--
-- Constraints for table `stok`
--
ALTER TABLE `stok`
  ADD CONSTRAINT `stok_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
